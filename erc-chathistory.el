;;; erc-chathistory.el -- Chathistory IRCv3 extension for ERC  -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file is currently just a stub.  Add `draft/chathistory' and
;; `draft/event-playback' to `erc-v3-extensions' to experiment.  In a
;; channel buffer, hit `scroll-down-command' (C-v) at the top of the
;; buffer to fetch more scrollback.

;; TODO
;;
;; - Use `labeled-response', when available, to track requests for
;;   debouncing or cancellation purposes.
;;
;; - Persist failed and prematurely terminated requests, and
;;   (optionally) recommence retrieval after reconnecting.
;;
;; - Implement failure handling. Remove requests and maybe retry.
;;   Example from Matrix2051:

;;; Code
(require 'erc-batch)
(require 'multisession nil t)

(defvar-keymap erc-v3--chathistory-mode-map
  :doc "Keymap for ERC's `chathistory' IRCv3 extension."
  "<remap> <scroll-down-command>" #'erc-chathistory--scroll-down-command)

;; XXX this exists to ensure the recorded time stamp for the last
;; received message won't survive system restarts (for now).  Note
;; that this has no effect when using "emacs -Q" because upstream
;; restricts such sessions to in-memory storage only.
(defvar erc-chathistory--dir (expand-file-name "erc-chathistory"
                                               (temporary-file-directory)))

;;;###autoload(put 'draft/chathistory 'erc--feature 'erc-chathistory)
(erc-v3--define-capability chathistory
  :aliases '(draft/chathistory)
  :supports '(message-tags labeled-response)
  :depends '(batch server-time)
  :slots ((max nil :type (or null integer))
          (msgreftypes '(timestamp msgid) :type (list-of symbol))
          (targets nil :type (list-of erc-chathistory--target))
          (reqs nil :type (list-of erc-chathistory--request)))
  :keymap erc-v3--chathistory-mode-map
  (when erc--target
    (if erc-v3--chathistory
        (progn
          (add-hook 'erc-chathistory--request-queued-functions
                    #'erc-chathistory--remove-placeholder-on-done 20 t)
          (add-hook 'erc-chathistory--request-queued-functions
                    #'erc-chathistory--clear-truncation-flag-on-done 30 t))
      (remove-hook 'erc-chathistory--request-queued-functions
                   #'erc-chathistory--remove-placeholder-on-done t)
      (remove-hook 'erc-chathistory--request-queued-functions
                   #'erc-chathistory--clear-truncation-flag-on-done t)))
  (unless erc--target
    (if erc-v3--chathistory
        (progn
          (erc-chathistory--add-kill-hook)
          (add-hook 'erc-server-JOIN-functions
                    #'erc-chathistory--ensure-buffer -75 t)
          (add-hook 'erc-server-NICK-functions
                    #'erc-chathistory--sandbox-nick-change -75 t)
          (add-hook 'erc-server-QUIT-functions
                    #'erc-chathistory--ensure-cmem -75 t)
          (add-hook 'erc-after-connect
                    #'erc-chathistory--get-targets-on-connect 0 t)
          (add-hook 'erc-disconnected-hook
                    #'erc-chathistory--check-failed-and-save-active 30 t))
      (remove-hook 'erc-server-QUIT-functions
                   #'erc-chathistory--ensure-cmem t)
      (remove-hook 'erc-server-NICK-functions
                   #'erc-chathistory--sandbox-nick-change t)
      (remove-hook 'erc-server-JOIN-functions
                   #'erc-chathistory--ensure-buffer t)
      (remove-hook 'erc-after-connect
                   #'erc-chathistory--get-targets-on-connect t)
      (remove-hook 'erc-disconnected-hook
                   #'erc-chathistory--check-failed-and-save-active t))))

;;;###autoload(put 'draft/event-playback 'erc--feature 'erc-chathistory)
;;;###autoload(put 'event-playback 'erc--feature 'erc-chathistory)
(erc-v3--define-capability event-playback
  :aliases '(draft/event-playback)
  :depends '(chathistory))

(declare-function make-multisession "multisession" (&rest rest))
(declare-function multisession-value "multisession" (object))

;; This exists to help simulate both environments in testing.
(defvar erc-chathistory--use-multisession-p (featurep 'multisession))

(defvar erc-chathistory--last-active
  (and erc-chathistory--use-multisession-p
       (make-multisession :key "erc-chathistory--last-active"
                          :initial-value nil
                          :package "erc"
                          :synchronized t
                          :storage 'files))
  "Alist of (NETWORK . UNIX-SECS) recording last message times.
When `erc-chathistory--use-multisession-p', value is boxed as a
multisession variable.")

(defun erc-chathistory--set-last-active (val)
  (if erc-chathistory--use-multisession-p
      ;; Just like in CL, we can't do (setf (cdr (assoc ...)) 42) with
      ;; places that have side effects, e.g.,
      ;;
      ;;  (setf (alist-get my-net
      ;;                  (multisession-value erc-chathistory--last-active))
      ;;        my-val)
      ;;
      ;; will fail because the inner setter won't be called on the
      ;; modified alist.
      (let* ((multisession-directory erc-chathistory--dir)
             (existing (multisession-value erc-chathistory--last-active)))
        (setf (alist-get (erc-network) existing) val
              (multisession-value erc-chathistory--last-active) existing))
    (setf (alist-get (erc-network) erc-chathistory--last-active) val)))

(defun erc-chathistory--get-last-active ()
  (alist-get (erc-network)
             (if erc-chathistory--use-multisession-p
                 (let ((multisession-directory erc-chathistory--dir))
                   (multisession-value erc-chathistory--last-active))
               erc-chathistory--last-active)))

(defvar erc-chathistory--fetch-limit 50)
(defvar erc-chathistory--sanity-limit 1000)

(cl-defmethod erc--server-005 :after
  ( _proc _parsed
    &context (erc-v3-mode (eql t)) (erc-v3--chathistory erc-v3--chathistory))
  "Populate CHATHISTORY parameters."
  (when-let* ((val (erc--get-isupport-entry 'CHATHISTORY 'single))
              (n (string-to-number val)))
    (setf (erc-v3--chathistory-max erc-v3--chathistory) n))
  (when-let* ((refs (erc--get-isupport-entry 'MSGREFTYPES)))
    (setf (erc-v3--chathistory-msgreftypes erc-v3--chathistory)
          (mapcar #'intern (cdr refs)))))


;;;; Targets API

(cl-defstruct erc-chathistory--target
  name buffer entry-stamp live-stamp request renicks)

(cl-defstruct erc-chathistory--renick
  old new stamp parsed)

(defun erc-chathistory--renick-equal-p (a b)
  "Compare `erc-chathistory--renick' A and B."
  (or (equal a b)
      (and (equal (erc-chathistory--renick-old a)
                  (erc-chathistory--renick-old b))
           (equal (erc-chathistory--renick-new a)
                  (erc-chathistory--renick-new b))
           (equal (erc-chathistory--renick-stamp a)
                  (erc-chathistory--renick-stamp b)))))

(defvar erc-message-english-chathistory-target "Last activity: %s"
  "English `erc-format-message' template for key `chathistory-target'.")

(cl-defstruct (erc-chathistory--targets-batch (:include erc-batch--info))
  "Data for processing a single `chathistory-targets' batch response."
  (parsed nil :type (or null erc-response)))

(defun erc-chathistory--create-targets-batch (ref type parsed)
  ;; Spoof timestamp for BATCH response because some servers don't
  ;; include one even though we've negotiated `server-time'.
  (unless (assq 'time (erc-response.tags parsed))
    (push (cons 'time (erc-span--ensure-time-str (erc--current-time-pair)))
          (erc-response.tags parsed)))
  (let ((info (make-erc-chathistory--targets-batch
               :type type :ref ref :parsed parsed :name "n/a")))
    (erc-batch--create-queue info)
    info))

(cl-defmethod erc-batch--create-info
  ((_ (eql chathistory-targets)) parsed ref _)
  (erc-chathistory--create-targets-batch ref 'chathistory-targets parsed))

(cl-defmethod erc-batch--create-info
  ((_ (eql draft/chathistory-targets)) parsed ref _)
  (erc-chathistory--create-targets-batch ref 'chathistory-targets parsed))

(cl-defmethod erc-batch--handle-enqueued
  ((info erc-chathistory--targets-batch))
  (erc-span--run-inserter (current-buffer) info))

(defvar erc-chathistory--new-query-request-functions nil)

;; FIXME catch up on open queries first, then channels, and, finally,
;; unknown queries.  But only do so after processing deferred quits
;; and nick changes seen in channels that can affect open queries.
(cl-defmethod erc-span--inserter-finalize
  ((info erc-chathistory--targets-batch))
  (erc-batch--common-teardown info)
  ;; We're merely destructuring here, not matching.
  (pcase-dolist ((and (cl-struct erc-chathistory--target name entry-stamp)
                      target)
                 (erc-v3--chathistory-targets erc-v3--chathistory))
    (when-let* ((parsed (erc-chathistory--targets-batch-parsed info))
                ((not (erc-channel-p name)))
                (time (erc-span--ensure-time entry-stamp)))
      (with-current-buffer (or (erc-get-buffer name erc-server-process)
                               (let ((erc-join-buffer 'bury))
                                 (erc--open-target name)))
        (setf (erc-chathistory--target-buffer target) (current-buffer))
        (if-let*
            ;; If ranges exist, fetch LATEST from last upper bound.
            ((req (or (erc-chathistory--backfill-from-newest-range)
                      ;; Otherwise, if reconnecting, fetch BETWEEN
                      ;; disconnect and now (currently no AFTER).
                      (erc-chathistory--backfill-from-fallback-on-reconnect
                       time))))

            (progn
              (setf (erc-chathistory--target-request target) req)
              (run-hook-with-args 'erc-chathistory--new-query-request-functions
                                  req))
          ;; Otherwise, inform the user they've got mail.
          (let ((erc--current-time-pair time))
            (erc-display-message parsed 'notice (current-buffer)
                                 'chathistory-target
                                 ?s (erc-span--local-time-string time))))))))

(cl-defmethod erc-span--inserter-insert-one
  ((info erc-chathistory--targets-batch))
  "Process one line of a queued `chathistory-targets' message."
  (pcase-let* ((`(,tags . ,latest) (erc-batch--remove info))
               (beg (string-search " " latest))
               (res (erc--parse-traditional-response
                     (substring latest (1+ beg))
                     (make-erc-response :unparsed latest :tags tags)))
               (`("TARGETS" ,target ,stamp) (erc-response.command-args res)))
    (push (make-erc-chathistory--target
           :name target
           :entry-stamp stamp
           :live-stamp (alist-get
                        'time (erc-response.tags
                               (erc-chathistory--targets-batch-parsed info))))
          (erc-v3--chathistory-targets erc-v3--chathistory)))
  (cl-assert (not (erc-batch--info-parent info))))

(defvar erc-chathistory--targets-initial-oldest (* 60 60 24 7)
  "Lower fallback bound for initial \"TARGETS\" query.")

(defun erc-chathistory--get-targets-on-connect (&rest _)
  "Ask the server for a list of targets with new messages."
  (let* ((newer (erc--current-time-pair))
         (older (if-let* ((existing (erc-chathistory--get-last-active)))
                    (erc-span--ensure-time existing)
                  (time-subtract newer
                                 erc-chathistory--targets-initial-oldest))))
    (erc-with-all-buffers-of-server erc-server-process #'erc-target
      (when-let* ((range erc-span--ranges)
                  (ts-hi (prog1 (erc-span--ensure-time
                                 (erc-span--range-ts-hi range)) ; possibly nil
                           (cl-assert (null (erc-span--range-next range)))))
                  ;; Update old/lo bound to a more recent last-seen stamp.
                  ((time-less-p older ts-hi)))
        (setq older ts-hi)))
    (erc-server-send (format "CHATHISTORY TARGETS timestamp=%s timestamp=%s %d"
                             (erc-span--ensure-time-str newer)
                             (erc-span--ensure-time-str older)
                             erc-chathistory--fetch-limit))))

(defun erc-chathistory--display-renicks-in-queries ()
  "Display nick changes in query buffers."
  (cl-assert (null (erc-v3--chathistory-reqs erc-v3--chathistory)))
  (dolist (target (erc-v3--chathistory-targets erc-v3--chathistory))
    (erc-with-buffer ((erc-chathistory--target-buffer target))
      (setf (erc-chathistory--target-renicks target)
            (nreverse (erc-chathistory--target-renicks target)))
      (while-let ((renick (pop (erc-chathistory--target-renicks target))))
        (cl-assert (string= (erc-chathistory--target-name target)
                            (erc-target)))
        (when-let*
            ((renick-stamp (erc-chathistory--renick-stamp renick))
             (request (erc-chathistory--target-request target))
             (older-stamp (erc-chathistory--request-older request))
             ((string-lessp older-stamp renick-stamp))
             (live-stamp (erc-chathistory--target-live-stamp target))
             ((string-lessp renick-stamp live-stamp))
             (parsed (erc-chathistory--renick-parsed renick))
             (nuh (erc--parse-nuh (erc-response.sender parsed)))
             (old (erc-chathistory--renick-old renick))
             (new (erc-chathistory--renick-new renick)))
          (cl-assert (member (erc-downcase (erc-target))
                             (list (erc-downcase old) (erc-downcase new))))
          (cl-assert (erc--zNICK-p parsed))
          (let ((new-buffer (erc-get-buffer new erc-server-process))
                ;; Spoof this local variable to force `erc--insert-line'
                ;; to insert the message at a historical position.
                (erc-v3--server-time-display-latest (current-time))
                (erc--parsed-response parsed))
            (erc-display-message parsed 'notice
                                 (cons (current-buffer)
                                       (and new-buffer (list new-buffer)))
                                 'NICK ?n old
                                 ?u (nth 1 nuh) ?h (nth 2 nuh) ?N new)))))))


;;;; Playback handling

(cl-defstruct erc-chathistory--request
  (target (error "Target required") :type string)
  (buffer (error "Buffer required") :type buffer)
  (holder nil :type (or null marker))
  (sent (error "Time sent required") :type list)
  (subcmd (error "Qualifying subcmd required") :type symbol)
  (limit nil :type (or null integer))
  (older nil :type (or null string) :documentation "Older goal bound.")
  (newer nil :type (or null string) :documentation "Newer goal bound.")
  (id-old nil :type (or null string) :documentation "Old msg-id bound.")
  (id-new nil :type (or null string) :documentation "New msg-id bound.")
  (count 0 :type integer)
  (queue nil :type (or null buffer)))

(defvar erc-chathistory--request-queued-functions nil
  "Called with final `erc-chathistory--request' object before insertion.
Runs in the target buffer when all batches in a logical history batch
have been consolidated and processed but not yet inserted.")

(defvar erc-chathistory--request-finalize-functions nil)

(cl-defmethod erc-span--inserter-finalize
  ((info erc-batch--chathistory)
   &context (erc-v3--chathistory erc-v3--chathistory))
  "Maybe rename query targets that have changed."
  (when-let* ((found (erc-chathistory--get-request
                      erc-v3--chathistory (erc-batch--info-name info))))
    (with-current-buffer (erc-chathistory--request-buffer found)
      (run-hook-with-args 'erc-chathistory--request-finalize-functions found)))
  (if (erc-v3--chathistory-reqs erc-v3--chathistory)
      (erc-chathistory--remove-request erc-v3--chathistory info)
    (erc-chathistory--display-renicks-in-queries))
  (cl-call-next-method))

(defun erc-chathistory--find-oldest-msg-time ()
  "Find lisp time of oldest message in the buffer."
  (or (and-let*
          ((ranges erc-span--ranges)
           ((erc-span--range-ts-lo (erc-span--range-get-oldest ranges)))))
      (and-let* ((bounds (erc-span--find-last-playback-msg (point-min)
                                                           erc-insert-marker)))
        (get-text-property (car bounds) 'erc--ts))
      (and-let* (((erc-query-buffer-p))
                 (prev (text-property-not-all (point-min) erc-insert-marker
                                              'erc--msg 'chathistory-target)))
        (get-text-property prev 'erc--ts))))

(defun erc-chathistory--ensure-cmem (_ parsed)
  "Ensure `erc-server-users' entry for current nick exists on span insertion."
  (when (and erc-batch--parsed-tags
             (erc--zresponse-p parsed))
    (pcase-let ((`(,nick ,login ,host) (erc--zresponse-nuh parsed))
                (channel (erc-batch--info-name erc-span--active-inserter)))
      (cl-assert channel)
      (erc-update-channel-member channel nick nick t nil nil nil nil nil
                                 host login)))
  nil)

(defun erc-chathistory--sandbox-nick-change (proc parsed)
  "Contain effects of nick changes for the sender of `erc-response' PARSED.
Inhibit further processing.  Expect PROC to be the `erc-server-process'."
  (erc-chathistory--ensure-cmem proc parsed)
  (when-let* ((erc-batch--parsed-tags)
              ((erc--zresponse-p parsed))
              (info erc-span--active-inserter)
              ((erc-batch--chathistory-p info))
              (buffer (erc-get-buffer (erc-batch--info-name info))))
    (pcase-let* ((`(,nick ,login ,host) (erc--zNICK-nuh parsed))
                 (new-nick (erc-response.contents parsed)))
      (erc-update-user-nick nick new-nick host login)
      (if (erc--zNICK-self-p parsed)
          (progn
            (setq erc-server-current-nick new-nick)
            (erc-display-message parsed 'notice buffer
                                 'NICK-you ?n nick ?N new-nick)
            (run-hook-with-args 'erc-nick-changed-functions new-nick nick))
        ;; Look for matching targets from this session.
        (when-let* ((found (cl-find nick (erc-v3--chathistory-targets
                                          erc-v3--chathistory)
                                    :key #'erc-chathistory--target-name
                                    :test #'erc-nick-equal-p)))
          (cl-assert (alist-get 'time (erc-response.tags parsed)))
          (cl-pushnew (make-erc-chathistory--renick
                       :old nick
                       :new new-nick
                       :stamp (erc--zresponse-time-str parsed)
                       :parsed parsed)
                      (erc-chathistory--target-renicks found)
                      :test #'erc-chathistory--renick-equal-p))
        (erc-display-message parsed 'notice buffer 'NICK
                             ?n nick ?u login ?h host ?N new-nick)))
    ;; Suppress further processing.
    t))

(defun erc-chathistory--ensure-buffer (_ parsed)
  "Prevent primary \"JOIN\" handler from opening a target during playback."
  (when (and erc-batch--parsed-tags
             (erc--zJOIN-p parsed)
             (erc--zJOIN-self-p parsed))
    (let* ((target (erc--ztargeted-target parsed))
           (buffer (erc-get-buffer target erc-server-process)))
      (cl-assert buffer)
      (setf (erc--zJOIN-buffer parsed) buffer)))
  nil)

(cl-defmethod erc-v3--standard-replies-handle
  ((_ (eql FAIL)) (_ (eql CHATHISTORY)) _ rest _)
  (cl-call-next-method)
  (pcase rest
    ;; This could use `labeled-response' to disambiguate.
    (`(,subcmd . ,_)
     (when-let* ((found (cl-find subcmd
                                 (erc-v3--chathistory-reqs erc-v3--chathistory)
                                 :key #'erc-chathistory--request-subcmd
                                 :test #'equal)))
       (setf (erc-v3--chathistory-reqs erc-v3--chathistory)
             (delq found (erc-v3--chathistory-reqs erc-v3--chathistory)))))))


;;; Utils

(defun erc-chathistory--has-msgid-p ()
  (and erc-v3--chathistory erc-v3--message-tags
       (memq 'msgid (erc-v3--chathistory-msgreftypes erc-v3--chathistory))))

(defun erc-chathistory--prep-time-bound (bound &optional newerp)
  ;; Ensure we include all messages sharing a stamp with a bound in so
  ;; as to prevent gaps when a logical batch is transmitted as
  ;; separate chunks due to count limits.
  (concat "timestamp="
          (erc-span--ensure-time-str
           (if newerp
               ;; Make newer bound even newer by 1 ms
               (time-add (erc-span--ensure-time bound) 0.001)
             ;; Make older bound even older by 100 us
             (time-subtract (erc-span--ensure-time bound) 0.0001)))))

(defun erc-chathistory--get-max (want)
  (min want (erc-v3--chathistory-max erc-v3--chathistory)))


;;;; Paging

(defun erc-chathistory--get-request (chathistory target)
  (cl-find target (erc-v3--chathistory-reqs chathistory)
           :key #'erc-chathistory--request-target
           :test #'erc-nick-equal-p))

(defun erc-chathistory--get-target-request ()
  (erc-chathistory--get-request erc-v3--chathistory (erc-target)))

(defun erc-chathistory--remove-request (chathistory info)
  (let ((found (erc-chathistory--get-request
                chathistory (erc-batch--info-name info))))
    (setf (erc-v3--chathistory-reqs chathistory)
          (delq found (erc-v3--chathistory-reqs chathistory)))
    found))

(defun erc-chathistory--sort-tags (tags)
  "Remove (batch . N) from a copy of TAGS before returning it."
  (let ((batch-elt (assq 'batch tags)))
    (cl-assert batch-elt)
    (sort (remove batch-elt tags) (lambda (a b) (string< (car a) (car b))))))

(defun erc-chathistory--dedupe-joint (info)
  ;; Pop the newest messages from the just-received batch that are
  ;; already present among the oldest in the work queue.
  (let (same)
    (erc-batch--visit-stamp-cohorts
     (lambda (tags beg end)
       (push (cons (erc-chathistory--sort-tags tags)
                   (save-excursion
                     (goto-char beg)
                     (search-forward " :" nil t)
                     (buffer-substring (point) end)))
             same)))
    (while-let ((same)
                (last (erc-batch--peek-last info))
                (tags (erc-chathistory--sort-tags (car last)))
                (beg (string-search " :" (cdr last)))
                (line (substring (cdr last) (+ 2 beg)))
                ((member (cons tags line) same)))
      (erc-batch--remove-last info))))

(defun erc-chathistory--append-batch-to-workspace (info prev next)
  "Clear batch queue after copying contents to workspace."
  ;; Append batch queue to workspace and increment count of total
  ;; lines received.
  (with-current-buffer
      (setf (erc-chathistory--request-queue next)
            (or (erc-chathistory--request-queue prev)
                (erc-batch--gen-queue
                 (concat "erc-chathistory-"
                         (erc-chathistory--request-target prev)))))
    (cl-assert (not (buffer-narrowed-p)))
    (cl-assert (with-current-buffer (erc-batch--info-queue info)
                 (not (buffer-narrowed-p))))
    ;; Dedupe when we don't support timestamps.
    (unless (erc-chathistory--has-msgid-p)
      (erc-chathistory--dedupe-joint info))
    ;; Transfer request-scope "context" state.
    (setf (erc-chathistory--request-count next)
          (+ (erc-chathistory--request-count prev)
             (erc-batch--info-length info)))
    (setf (erc-chathistory--request-holder next)
          (erc-chathistory--request-holder prev))
    (goto-char (point-min))
    (insert-before-markers (with-current-buffer (erc-batch--info-queue info)
                             (prog1 (buffer-string)
                               (erase-buffer))))
    (goto-char (point-max))
    (setf (erc-batch--info-length info) 0)))

(defun erc-chathistory--append-workspace-to-batch (info prev)
  "Destroy workspace after copying contents to batch."
  ;; Append the workspace stash to the current, yet-to-be processed
  ;; batch queue.
  (when-let* ((workspace (erc-chathistory--request-queue prev)))
    (unless (erc-chathistory--has-msgid-p)
      (with-current-buffer workspace
        (erc-chathistory--dedupe-joint info)))
    (with-current-buffer (erc-batch--info-queue info)
      (goto-char (point-max))
      (insert (with-current-buffer workspace (buffer-string)))
      (kill-buffer workspace))
    (cl-incf (erc-batch--info-length info)
             (erc-chathistory--request-count prev))))

(defun erc-chathistory--remove-placeholder-on-done (prev)
  ;; Insertion stage reached successfully, so delete placeholder.
  (when-let* ((marker (erc-chathistory--request-holder prev)))
    (erc-chathistory--remove-placeholder marker)
    (setf (erc-chathistory--request-holder prev) nil)))

(defun erc-chathistory--clear-truncation-flag-on-done (_)
  (setf (alist-get 'erc--inhibit-clear-p erc-span--destructive-target-locals)
        #'natnump))

(defun erc-chathistory--handle-before (info prev count)
  "Handle \"BEFORE\" sub-command with batch data INFO and PREV request.
Return new request object or nil."
  (cl-assert (null (erc-chathistory--request-older prev)))
  (let* ((otags (car (erc-batch--peek info)))
         (id-new (and (erc-chathistory--has-msgid-p) (alist-get 'msgid otags)))
         (limit (erc-chathistory--request-limit prev))
         (oldest (alist-get 'time otags)))
    (when-let* (((< count limit))
                ((or id-new (>= count (erc-chathistory--get-max limit))))
                ;; Offset the new count limit by the number of messages
                ;; sharing a time stamp.
                (diff (+ (- limit count) (or (and id-new 0)
                                             (erc-batch--count-oldest info)))))
      ;; Specify last `oldest' (lower) for next `newest' (upper) bound.
      (erc-chathistory--fetch 'BEFORE diff nil oldest nil id-new))))

(defun erc-chathistory--handle-between (info prev count)
  "Handle \"BETWEEN\" sub-command with batch data INFO and PREV request.
Return new request object or nil."
  (let* ((otags (car (erc-batch--peek info)))
         (oldest (alist-get 'time otags))
         (target-oldest (erc-chathistory--request-older prev))
         (limit (erc-chathistory--request-limit prev))
         (idp (erc-chathistory--has-msgid-p))
         (id-old (and idp (erc-chathistory--request-id-old prev)))
         (id-new (and idp (alist-get 'msgid otags))))
    (cl-assert target-oldest) ; `id-old' is likely nil
    ;; Absent support for IDs, stop if under the limit to ensure
    ;; termination even with padded bounds.
    (when (and (string< target-oldest oldest)
               (or idp (>= count (erc-chathistory--get-max limit))))
      (erc-chathistory--fetch 'BETWEEN limit target-oldest oldest
                              id-old id-new))))

(defun erc-chathistory--handle-latest (info prev count)
  "Handle \"LATEST\" sub-command with batch data INFO and PREV request.
Return new request object or nil."
  (if-let* ((otags (car (erc-batch--peek info)))
            (oldest (alist-get 'time otags)))
      ;; Previous fetch was stamp/id lower/"not before" bounded.
      (erc-chathistory--handle-between info prev count)
    ;; Previous fetch was line-count (*) bounded.
    (erc-chathistory--handle-before info prev count)))

(defconst erc-message-english-chathistory-request
  "Requesting history from %y %s"
  "English `erc-format-message' template for key `chathistory-request'.")

;; FIXME use `erc--clear-function' interface to ensure placeholder
;; remains after truncation.
(defun erc-chathistory--remove-placeholder (marker)
  "Remove placeholder message at MARKER.
Remove flag inhibiting truncation."
  (with-current-buffer (marker-buffer marker)
    (if (get-text-property marker 'erc-chathistory--request)
        (erc--delete-inserted-message-naively marker)
      ;; Placeholder has likely been deleted by the `truncate' module.
      (when-let* ((range (get-text-property marker 'erc-span--range)))
        (cl-assert (eq range (erc-span--range-get-oldest erc-span--ranges)))))
    (set-marker marker nil)))

(defvar-local erc-chathistory--save-active-on-kill-emacs-function nil)

(defun erc-chathistory--add-kill-hook ()
  "Arrange for saving last known message time on `kill-emacs-hook'."
  (cl-assert (erc--server-buffer-p))
  (cl-assert erc-v3--chathistory)
  (unless erc-chathistory--save-active-on-kill-emacs-function
    (add-hook
     'kill-emacs-hook
     (setq erc-chathistory--save-active-on-kill-emacs-function
           (let ((sym (gensym "erc-chathistory--save-active-on-kill-emacs-")))
             (fset sym (apply-partially #'erc-chathistory--save-last-active
                                        (current-buffer)))
             sym)))))

(defun erc-chathistory--remove-kill-hook ()
  (when-let* ((val erc-chathistory--save-active-on-kill-emacs-function))
    (remove-hook 'kill-emacs-hook val)
    (kill-local-variable
     'erc-chathistory--save-active-on-kill-emacs-function)))

(defun erc-chathistory--save-last-active (buffer)
  "Save last message time in BUFFER."
  (erc-with-buffer (buffer)
    (erc-chathistory--remove-kill-hook)
    (when (and erc-v3--chathistory erc-server-last-received-time)
      (erc-chathistory--set-last-active
       (erc-span--ensure-time-str erc-server-last-received-time)))))

(defun erc-chathistory--check-failed-and-save-active (&rest _)
  ;; Even if history playback has failed, save last received time in
  ;; persistent variable because "live" messages are inserted more or
  ;; less immediately.
  (erc-chathistory--remove-kill-hook)
  (erc-chathistory--set-last-active
   (erc-span--ensure-time-str erc-server-last-received-time))
  (dolist (request (erc-v3--chathistory-reqs erc-v3--chathistory))
    (when-let* ((marker (erc-chathistory--request-holder request)))
      (with-current-buffer (marker-buffer marker)
        (let* ((status (get-text-property marker
                                          'erc-chathistory--request-status))
               (faces (get-text-property marker 'font-lock-face))
               (pos (cl-position 'erc-pending faces)))
          (setf (car (cdr status)) "FAILED!" ; English only for now
                (car (car status)) "XXX "
                (nth pos faces) 'erc-error-face))))))

(cl-defmethod erc-batch--create-info ((_ (eql chathistory)) _ _ _
                                      &context (erc-v3--chathistory
                                                erc-v3--chathistory))
  (let ((info (cl-call-next-method)))
    (when-let* ((request (erc-chathistory--get-request
                          erc-v3--chathistory (erc-batch--info-name info)))
                ((null (erc-chathistory--request-holder request)))
                (time-str (or (erc-chathistory--request-older request)
                              (erc-chathistory--request-newer request)))
                (time (erc-span--ensure-time time-str)))
      (when (eq (erc-chathistory--request-subcmd request) 'LATEST)
        (setq time (time-add time 10)))
      (with-current-buffer (erc-chathistory--request-buffer request)
        (defvar erc-button-alist)
        ;; FIXME start searching from live span range's lower bound.
        (let* ((newer (erc-chathistory--request-newer request))
               (point (erc-span--find-insertion-point erc-insert-marker time
                                                      (null newer)))
               (marker (copy-marker point))
               (erc--insert-marker marker)
               (erc-button-alist (and (bound-and-true-p erc-button-alist)
                                      `((,(rx (group (+ nonl))) 0 t
                                         erc-chathistory--retry)
                                        ,@erc-button-alist)))
               (status (cons (list "... ") (list "")))
               (erc--msg-prop-overrides `((erc-chathistory--request . ,request)
                                          (erc--ephemeral . chathistory)
                                          (erc-chathistory--request-status
                                           . ,status)
                                          (erc--ts . ,time)
                                          ,@erc--msg-prop-overrides))
               (erc-notice-prefix (propertize erc-notice-prefix 'display
                                              (car status) 'erc--prefix
                                              'chathistory-pending)))
          (setf (erc-chathistory--request-holder request) marker)
          (erc-display-message nil '(t notice pending)
                               (current-buffer) 'chathistory-request ?y
                               (propertize time-str 'display
                                           (erc-span--local-time-string time))
                               ?s (propertize "..." 'display (cdr status))))))
    info))

(cl-defmethod erc-batch--massage-enqueued
  ((info erc-batch--chathistory)
   &context (erc-v3--chathistory erc-v3--chathistory))
  ;; If the buffer has an active request, stash the current queue,
  ;; request the next bunch, and inhibit insertion by setting the
  ;; count to zero.  Otherwise, replace the active batch's queue with
  ;; our request queue, delete the request object, and allow the span
  ;; inserter to run.
  (cl-assert (null (erc-batch--info-count info)))
  (cl-assert (null erc--target))
  (cl-assert (erc-v3--chathistory-reqs erc-v3--chathistory))
  (if-let*
      ((prev (erc-chathistory--remove-request erc-v3--chathistory info))
       (buffer (erc-chathistory--request-buffer prev))
       (count (erc-batch--info-length info))
       ((< (erc-chathistory--request-count prev)
           erc-chathistory--sanity-limit))
       ((not (zerop count)))
       (next (with-current-buffer buffer
               (cl-assert (alist-get 'time (car (erc-batch--peek info))))
               (cl-assert (erc-chathistory--request-limit prev))
               (pcase-exhaustive (erc-chathistory--request-subcmd prev)
                 ('LATEST (erc-chathistory--handle-latest info prev count))
                 ('BETWEEN (erc-chathistory--handle-between info prev count))
                 ('BEFORE (erc-chathistory--handle-before info prev count))))))
      (erc-chathistory--append-batch-to-workspace info prev next)
    (when (and count (not (zerop count))
               (>= (erc-chathistory--request-count prev)
                   erc-chathistory--sanity-limit))
      (erc-button--display-error-notice-with-keys
       buffer "Stopping history job early after hitting fetch limit of %d."
       erc-chathistory--sanity-limit))
    ;; Restore request obj and remove in `erc-span--inserter-finalize'
    ;; so it's clear there's still an outstanding job for this buffer.
    (push prev (erc-v3--chathistory-reqs erc-v3--chathistory))
    (with-current-buffer buffer
      (run-hook-with-args 'erc-chathistory--request-queued-functions prev))
    (erc-chathistory--append-workspace-to-batch info prev))
  (cl-call-next-method))


;;;; Entry points

(defun erc-chathistory--fetch (subcmd limit older newer
                                      &optional id-old id-new)
  "Fetch up to LIMIT lines via \"CHATHISTORY\" request.
Expect NEWER and OLDER to be timestamps or nil, as appropriate for
SUBCMD.  Expect message IDs, when present, to be strings present only
for continuing paginated fetches, as their ordering is unreliable for
initial fetches across connections."
  (cl-assert erc--target)
  (cl-assert limit)
  (when-let* ((existing (erc-chathistory--get-target-request)))
    (error "Existing request: %S" existing))
  (push (make-erc-chathistory--request
         :target (erc-target)
         :buffer (current-buffer)
         :sent (erc--current-time-pair)
         :subcmd subcmd
         :limit limit
         :older (and older (erc-span--ensure-time-str older))
         :newer (and newer (erc-span--ensure-time-str newer))
         :id-old id-old
         :id-new id-new)
        (erc-v3--chathistory-reqs erc-v3--chathistory))
  (when id-old (setq id-old (concat "msgid=" id-old)))
  (when id-new (setq id-new (concat "msgid=" id-new)))
  (erc-server-send
   (concat
    "CHATHISTORY " (symbol-name subcmd) " " (erc-target) " "
    (pcase-exhaustive subcmd
      ;; Latest with older page progress from newer to older.
      ('LATEST (if older
                   (or id-old (erc-chathistory--prep-time-bound older))
                 "*"))
      ('BEFORE (or id-new (erc-chathistory--prep-time-bound newer t)))
      ;; Always specify the newer bound first so that we progressively
      ;; fetch older pages.
      ('BETWEEN (concat
                 (or id-new (erc-chathistory--prep-time-bound newer t)) " "
                 (or id-old (erc-chathistory--prep-time-bound older)))))
    " " (number-to-string (erc-chathistory--get-max limit))))
  (car (erc-v3--chathistory-reqs erc-v3--chathistory)))

(defun erc-chathistory--retry ()
  (interactive)
  (when-let* (((null (erc-chathistory--get-target-request)))
              (request (get-text-property (pos-bol) 'erc-chathistory--request))
              ((and erc-server-connected (y-or-n-p "Retry? "))))
    (erc-chathistory--remove-placeholder
     (erc-chathistory--request-holder request))
    (erc-chathistory--fetch (erc-chathistory--request-subcmd request)
                            (erc-chathistory--request-limit request)
                            (erc-chathistory--request-older request)
                            (erc-chathistory--request-newer request)
                            (erc-chathistory--request-id-old request)
                            (erc-chathistory--request-id-new request))))

(defun erc-chathistory--scroll-down-command (&optional arg)
  ;; Intercept `scroll-down' and ask user to make a BEFORE or BETWEEN
  ;; request.
  (interactive "^P")
  (condition-case nil
      (scroll-down-command arg)
    (beginning-of-buffer
     (when (or (erc-chathistory--get-target-request)
               (not erc-server-connected)
               (not (y-or-n-p "Fetch more history? ")))
       (signal 'beginning-of-buffer nil))
     (let* ((default erc-chathistory--fetch-limit)
            (val (read-string
                  (format-prompt "How far back (e.g., Nd/h/m) or # lines?"
                                 default)
                  nil nil (number-to-string default)))
            (newest (erc-chathistory--find-oldest-msg-time))
            (limit (and (string-match (rx bot (+ (any "0-9")) eot) val)
                        (string-to-number val)))
            (oldest (and (null limit)
                         (time-subtract (current-time)
                                        (erc--decode-time-period val))))
            (command (if limit 'BEFORE 'BETWEEN)))
       (erc-chathistory--fetch command (or limit default) oldest newest)))))

(defun erc-chathistory--backfill-from-fallback-on-reconnect (newest)
  "Request between stamp recording close of last session and NEWEST.
Return request object or nil."
  (cl-assert erc--target)
  (cl-assert (not (erc--target-channel-p erc--target)))
  (when-let* ((last-received (erc-with-server-buffer
                               erc-span--last-received-time))
              ;; Decrement lower bound backward "fuzz interval" seconds.
              (oldest (time-subtract last-received 10)))
    (erc-chathistory--fetch 'BETWEEN erc-chathistory--fetch-limit
                            oldest newest)))

(defun erc-chathistory--backfill-from-newest-range ()
  "Request everything after the most recent span range.
If one doesn't exist but there's a persistent \"last active\" timestamp
recorded for this server, use that.  Return a new request object or nil."
  (erc-span--seal-last-live)
  ;; Unlike with the targets API, don't bother falling back on a fixed
  ;; window, e.g., `erc-chathistory--targets-initial-oldest', because
  ;; newly joined channels likely won't provide any history.
  (when-let*
      ((lower-bound
        (if-let* ((ranges erc-span--ranges)
                  (node (erc-span--range-get-newest ranges))
                  (ts-lo (erc-span--ensure-time (erc-span--range-ts-lo node)))
                  (ts-hi (erc-span--ensure-time (erc-span--range-ts-hi node)))
                  ;; Decrement lower bound backward "fuzz interval" seconds.
                  (adjusted (time-subtract ts-hi 10)))
            (if (time-less-p adjusted ts-lo) ts-lo adjusted)
          (erc-chathistory--get-last-active))))
    ;; Suppress truncation for duration of request.
    (setf (alist-get 'erc--inhibit-clear-p erc-span--destructive-target-locals)
          #'erc-chathistory--inhibit-clear-during-request)
    (when (erc--module-active-p 'truncate)
      (setq erc--inhibit-clear-p t))
    ;; Request messages after `lower-bound' until live stream.
    (erc-chathistory--fetch 'LATEST
                            ;; FIXME after paging is sorted, fetch
                            ;; full sanity limit instead.
                            erc-chathistory--fetch-limit lower-bound nil)))

(defun erc-chathistory--inhibit-clear-during-request (_state)
  (and (erc-chathistory--get-target-request) t))

(cl-defmethod erc-span--backfill-on-join
  (&context (erc-v3--chathistory erc-v3--chathistory))
  (erc-chathistory--backfill-from-newest-range))

(defun erc-chathistory-toggle-bookend-visibility (arg)
  "Toggle bookend visibility."
  (interactive "P")
  (erc--toggle-hidden 'span-range-bookend arg))


(provide 'erc-chathistory)
;;; erc-chathistory.el ends here
;;
;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
