---
stages:
  - build
  - prep
  - lint
  - test
  - compat
  - integrate
  - package
  - commit

include:
  # Can also specify branch with ref: item.
  - project: emacs-erc/bugs
    file: resources/gitlab-ci/common.yml

variables:
  # Override included common/variables:EMACS_IMAGE because it uses the
  # $CI_REGISTRY_IMAGE environment variable, which expands to a URL for this
  # project's registry, while we want the one from Bugs.
  EMACS_IMAGE: registry.gitlab.com/emacs-erc/bugs/emacs:master
  EMACS_IMAGE_NATIVE: registry.gitlab.com/emacs-erc/bugs/emacs:native-comp
  DOOM_IMAGE: $CI_REGISTRY_IMAGE/doom:latest
  DOOM_REVISION: 7a7503045850ea83f205de6e71e6d886187f4a22
  ERC_V3_TEST_SOCKS: tor:9050
  DOCKER_VERSION: 24.0.5
  ERC_PACKAGE_NAME: "erc-edge"
  BUG_URL: $CI_PROJECT_URL
  BUG_NUMBER: "999999"

.wait-for-tor: &wait-for-tor
  - |
    tries=20
    while ! (
      curl -s --socks5 $ERC_V3_TEST_SOCKS https://check.torproject.org |
      grep Congratulations
    ) && [ $tries -ge 0 ]; do
      sleep 1
      # Alpine ash knows how to do $(( arith ... ))
      echo $(( --tries )) tries remaining
    done
    [ $tries -ge 0 ]

.get-version: &get-version
  - >-
    version=$(
    readlink archive/erc-edge.tar | sed
    -e 's/\(erc-edge-\|[.]tar\)//g'
    )

build:docker-doom:
  stage: build
  image: docker:$DOCKER_VERSION
  services:
    - docker:$DOCKER_VERSION-dind
  variables:
    # Use TLS (for no reason)
    GIT_SUBMODULE_STRATEGY: normal
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: /certs
  before_script:
    # Sanity check for working dir
    - test $CI_PROJECT_DIR = $PWD
    # Ensure all variables are expanded (no bash, so no declare -p)
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  rules:
    - if: $ONEOFF =~ /^build/
    - when: never
  script:
    # currently unused
    - mkdir -v build
    - >
      docker build
      --pull
      --tag localhost/doom:testing
      --file resources/containerfiles/doom.dockerfile
      --build-arg revision=$DOOM_REVISION
      build
    # Push test image on success
    - docker tag localhost/doom:testing $DOOM_IMAGE
    - docker push $DOOM_IMAGE

# Ensure scripts subdir from Bugs exists in project dir.
prep:clone:
  stage: prep
  image: $EMACS_IMAGE
  script:
    - git -C /checkout log -n1
    - git clone https://gitlab.com/emacs-erc/bugs.git
    - mv bugs/scripts .
    - mv bugs/resources/elpa resources/
    - mv bugs/lisp .
    - cp bugs/resources/gitlab-ci/pages/delete_surplus_files.sh scripts/
  artifacts:
    when: always
    paths:
      - lisp
      - scripts
      - resources/elpa
    expire_in: 1 day
  rules:
    - if: $ONEOFF == "build"
      when: never
    - when: on_success

lint:trunk:wip:
  extends: .lint:common
  variables:
    PATCH_DIR: $CI_PROJECT_DIR/resources/patches
  # Override common/rules-bug, which requires the branch to match BUG_NUMBER.
  rules:
    - when: on_success

lint:trunk:misc:
  extends: .lint:common
  variables:
    PATCH_DIR: $CI_PROJECT_DIR/resources/patches
    SELECTOR: '$(SELECTOR_EXPENSIVE)'
  script:
    - bash scripts/apply_to_trunk.bash $PATCH_DIR
    - bash scripts/clean.bash
    - make -C /checkout
    - bash scripts/misc_custom_check.bash
  # Override common/rules-bug, which requires the branch to match BUG_NUMBER.
  rules:
    - when: on_success

test:trunk:check:
  extends:
    # This exports top-level logs, patches, archive, test
    - .test:trunk:common
  variables:
    PACKAGE: "1"
    BUG_README: $CI_PROJECT_DIR/README.org
    EMACS_TEST_JUNIT_REPORT: junit-test-report.xml
    SELECTOR:
      # Don't indent subsequent lines for readability because that forces the
      # parser to include newlines and indentation differences.
      value: >-
        (and $(SELECTOR_EXPENSIVE)
        (not (tag :tramp-asynchronous-processes))
        (not \"^file-notify-.*\") (not \"^ucs-normalize-.*\"))
      expand: false
    TEST_CMD: "make -C /checkout check"
    BUILD_CMD: make do-build-cmd
    PATCH_DIR: $CI_PROJECT_DIR/resources/patches
    STRICT: "0"
  dependencies:
    - prep:clone
  # Override common/rules-bug, which requires the branch to match BUG_NUMBER.
  rules:
    - when: on_success

test:trunk:wip:
  extends: test:trunk:check
  variables:
    STRICT: "1"
    PACKAGE: "0"
    SELECTOR: '$(SELECTOR_EXPENSIVE)'
    TEST_BACKTRACE_LINE_LENGTH: "1000"
    TEST_CMD: make -C /checkout/test check-lisp-erc

test:trunk:native:
  extends: test:trunk:check
  image: $EMACS_IMAGE_NATIVE
  variables:
    PACKAGE: "0"
  timeout: one hour

compat:27:
  extends:
    - .compat:common
  variables:
    BUG_NUMBER: "edge"
  image: docker.io/silex/emacs:27-ci
  dependencies:
    - prep:clone
    - test:trunk:check
  # Override common/rules-bug, which requires the branch to match BUG_NUMBER.
  rules:
    - when: on_success

compat:28:
  variables:
    SELECTOR: (quote (not erc-nicks-track-faces/prioritize))
  extends:
    - compat:27
  image: docker.io/silex/emacs:28-ci

compat:29:
  extends:
    - compat:27
  image: docker.io/silex/emacs:29-ci

integrate:doom:
  stage: integrate
  image: $DOOM_IMAGE
  script:
    - make prep-git
    - make clean-git
    - make update
    - make update-readme
    - make clean
    - make add
    - git commit -m "test"
    - testdir=/root/.emacs.d/.local/straight/repos/erc-edge
    - git clone $CI_PROJECT_DIR $testdir
    - make -C $testdir test-doom
  dependencies:
    - test:trunk:check

integrate:tor:
  stage: integrate
  image: $EMACS_IMAGE
  services:
    - name: osminogin/tor-simple:0.4.4.5
      alias: tor
  script:
    - *wait-for-tor
    - >-
      emacs -batch -eval '(package-refresh-contents)'
      -eval '(package-install-file "archive/erc-edge.tar")'
    - >-
      HEAD_SUMMARY=$(
      git -C /checkout log -n1
      --format='EDGE [%h] %an (%cr): %s'
      )
    - export HEAD_SUMMARY
    - make test-socks
  retry:
    max: 2
  dependencies:
    - test:trunk:check

package:lisp:
  stage: package
  image: alpine:latest
  variables:
    BASE_API_URL: $CI_API_V4_URL/projects/$CI_PROJECT_ID
    BASE_PKG_URL: $BASE_API_URL/packages/generic/master
  script:
    - test -d archive
    # Grab jq for the duplicate deletion below.
    - apk --no-cache add tar curl jq
    - test -f scripts/delete_surplus_files.sh
    - tar -czf archive.tar.gz archive
    # erc-edge-5.7alpha0.20250221.23906.tar ~~> 5.7alpha0.20250221.23906
    - *get-version
    # 5.7alpha0.20250221.23906 ~~> 5.7.0.20250221.23906
    - version=$(printf %s "$version" | sed 's/alpha/./')
    - package_registry_url=$BASE_PKG_URL/$version
    - >-
      printf '$package_registry_url: %s\n' $package_registry_url
    # Upload only archive.tar.gz as a package named "master."
    - >-
      curl --silent --show-error --fail --header "JOB-TOKEN: $CI_JOB_TOKEN"
      --upload-file archive.tar.gz $package_registry_url/archive.tar.gz
    # Delete older duplicates.
    - sh scripts/delete_surplus_files.sh delete master archive.tar.gz "$version"
  artifacts:
    paths:
      - archive.tar.gz
  rules:
    - if: $ONEOFF
      when: never
    - when: on_success
  dependencies:
    - prep:clone
    - test:trunk:check

# This updates the repo's contents from the ELPA tarball created in
# test:trunk:check. It then installs itself as a package directory and runs
# the tests in a slightly different order than on EMBA. If successful, it
# commits the updated files.
commit:update:
  stage: commit
  image: $EMACS_IMAGE
  rules:
    - if: $ONEOFF
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
      when: on_success
    - when: never
  script:
    - *get-version
    - test -n "$version"
    - make prep-git
    - make clean-git
    - make update
    - make update-readme
    - make clean
    - make add
    - >
      printf "%s\n\n%s" "$version" "$CI_COMMIT_MESSAGE" |
      git commit --author "$CI_COMMIT_AUTHOR" -F -
    - make install
    # For good measure, also run the standard test suite.
    - make test
    - >-
      git push
      -o ci.skip
      https://gitlab-ci-token:$PATTY_CAKE@$CI_SERVER_HOST/$CI_PROJECT_PATH.git
      HEAD:$CI_COMMIT_BRANCH
  dependencies:
    - test:trunk:check
