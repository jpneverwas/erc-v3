;;; erc-scenarios-v3-multiline.el --- IRCv3 multiline -*- lexical-binding: t -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO:
;; - Receiving a multiline message out of the blue as a direct message
;;   from someone you haven't conversed with yet in the current
;;   session.

;;; Code:
(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-scenarios-common)
(require 'erc-v3)

(declare-function erc-batch--es "erc-batch" nil)

(ert-deftest erc-scenarios-v3-multiline-chathistory ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/multiline")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'chathistory))
       (port (process-contact dumb-server :service))
       (erc-modules `(v3 fill-wrap ,@erc-modules))
       (erc-v3-extensions `(multiline sasl ,@erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connects")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :user "tester"
                                :password "changeme"
                                :full-name "tester")
        (funcall expect 10 "debug mode")))

    (ert-info ("Populates #chan with missed history")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
        (funcall expect 5 ">>> RANGE-BEG 2023-08-28T02:16:06.645Z")
        (funcall expect 5 "<alice> Ay, marry: now")
        (funcall expect 5 "<alice> bob: It was your pleasure")
        (funcall expect 5 "<weechat> #+BEGIN_MULTILINE")
        (funcall expect 5 ";; In order to hack on")
        (funcall expect 5 "      (erc--sid-dynamic-create))))")
        (funcall expect 5 "#+END_MULTILINE")
        (funcall expect 5 "<bob> That were the most,")
        (funcall expect 5 "<alice> bob: Reveal the damn'd")
        (funcall expect 5 "<weechat> #+BEGIN_MULTILINE")

        ;; Consecutive messages are successfully joined when tag
        ;; `draft/multiline-concat' is present.
        (funcall expect 5 "For server implementations")
        (funcall expect 5 "batch. In particular")
        (funcall expect 5 "disconnection (a denial")
        ;; Unicode characters (like U+2019 RIGHT SINGLE QUOTATION
        ;; MARK) in text body are correctly decoded.
        (funcall expect 5 "the sender’s full" )
        (funcall expect 5 "target. This")
        (funcall expect 5 "max-bytes. Configuring")
        (funcall expect 5 "Additionally, in the context")

        (funcall expect 5 "#+END_MULTILINE")
        (funcall expect 5 "<bob> alice: This is his second fit")
        (funcall expect 5 "<bob> alice: Flat treason")
        (funcall expect 5 "<<< RANGE-END 2023-08-28T02:16:06.645Z")
        (funcall expect 5 "You have joined channel #chan")
        (funcall expect 5 "Users on #chan (5): @fsbot")
        (funcall expect 5 "<bob> alice: Chi non te vede")
        (funcall expect 5 "<bob> Who ? not the duke ?")
        (funcall expect 5 erc-prompt)))

    (with-current-buffer "foonet"
      (should-not (erc-batch--es)))))

(defvar erc-scenarios-v3-multiline--long-line
  (concat
   "Et facere porro perferendis dolores qui. Neque sit illum dolores labore. "
   "Aut repudiandae dolor aliquam. Alias dolores neque magni in. Dolorem "
   "ullam labore reprehenderit voluptates. Tempore et nihil quia eos sed non. "
   "Possimus necessitatibus minima voluptatem autem voluptatem esse et."
   " Fugiat totam fugiat id et. Veniam aperiam sed ipsum a ut odit aperiam a. "
   "Omnis dolorum reprehenderit a sit doloremque veniam quidem. Culpa et "
   "deleniti quo. Deleniti eaque excepturi et omnis. Ut repudiandae voluptas "
   "qui et. Dolor dolores est minima voluptas hic. Dolorem vero eos et fuga. "
   "Praesentium non saepe ut. Et atque explicabo culpa quam voluptatem nihil. "
   "Rerum consectetur quae tempora optio et corrupti sed saepe. Dolores "
   "assumenda dolorem asperiores sint voluptatum. In doloribus ipsa nisi fugit"
   " ipsam blanditiis error. Cum hic rerum voluptas et ipsam. Tempore ut "
   "laborum delectus. Neque praesentium similique eligendi aut. Rerum "
   "consequatur omnis quam facere fugiat occaecati non debitis. "))

(declare-function erc-multiline--send-current-line "erc-multiline" nil)

;; This tests multiline message rendering on receipt only because
;; `echo-message' is enabled.  That is, there's no send-hook behavior
;; being triggered.
(ert-deftest erc-scenarios-v3-multiline-outgoing ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/multiline")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'outgoing))
       (port (process-contact dumb-server :service))
       (erc-modules `(v3 fill-wrap ,@erc-modules))
       (erc-autojoin-channels-alist '((foonet "#chan")))
       (erc-send-whitespace-lines t)
       (erc-warn-about-blank-lines nil)
       (erc-v3-extensions `(multiline ,@erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connects")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :user "tester"
                                :full-name "tester")
        (funcall expect 10 "debug mode")))

    (ert-info ("Autojoins #chan")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))

        (ert-info ("Correctly renders overlong singleton")
          (funcall expect 5 "<alice> bob: Taking the measure")
          (let (erc-accidental-paste-threshold-seconds inhibit-interaction)
            (goto-char erc-input-marker)
            (insert erc-scenarios-v3-multiline--long-line)
            (ert-simulate-keys "y\r" (erc-multiline--send-current-line)))
          (funcall expect 10 "<tester> #+BEGIN_MULTILINE")
          (funcall expect 10 erc-scenarios-v3-multiline--long-line)
          (funcall expect 10 "#+END_MULTILINE")
          (funcall expect 10 "<bob> Who ? not the duke ?"))

        ;; FIXME this likely suffers from a concurrency issue.  The
        ;; `batch-6' exchange "BATCH +2 draft/multiline #chan" can
        ;; arrive after `batch-7' "@batch=2 PRIVMSG #chan :one".
        (ert-info ("Correctly renders obvious multiline")
          (let (erc-accidental-paste-threshold-seconds inhibit-interaction)
            (goto-char erc-input-marker)
            (insert "one\n\ntwo\nthree")
            (ert-simulate-keys "y\r" (erc-multiline--send-current-line)))
          (funcall expect 10 "<bob> alice: And, to the advantage")
          (funcall expect 10 "<tester> #+BEGIN_MULTILINE")
          (funcall expect 10 "#+END_MULTILINE")
          (funcall expect 10 "<alice> Being of no power"))

        (funcall expect 10 erc-prompt)))

    (with-current-buffer "foonet"
      (should-not (erc-batch--es)))))

;; This asserts that messages are printed sequentially and order is
;; preserved between arrival and insertion.
(ert-deftest erc-scenarios-v3-multiline-back-to-back ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/multiline")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'back-to-back))
       (port (process-contact dumb-server :service))
       (erc-modules `(v3 fill-wrap ,@erc-modules))
       (erc-autojoin-channels-alist '((foonet "#chan")))
       (erc-v3-extensions `(multiline ,@erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connects")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "tester")
        (funcall expect 10 "debug mode")))

    (ert-info ("Autojoins #chan")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
        (funcall expect 10 "<alice> bob: We'll make you some sport")
        (funcall expect 10 ";; 0")
        (seq-doseq (c "123456789abcdefghijklmnopqrstuv")
          (funcall expect 5 (format ";; %c" c)))
        (funcall expect 10 "<bob> Dost thou infamonize me")
        (funcall expect 10 ";; w")
        (funcall expect 10 ";; x")
        (funcall expect 10 ";; y")
        (funcall expect 10 ";; z")
        (funcall expect 10 "<bob> alice: Thou shalt know her,")
        (funcall expect 10 "<alice> The citizens are up")))

    (with-current-buffer "foonet"
      (should-not (erc-batch--es)))))

;;; erc-scenarios-v3-multiline.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

