;;; erc-scenarios-v3-sasl.el --- SASL tests for ERC -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The point of this file (should be) to test the integration between
;; `erc-v3' and `erc-sasl' because the latter spoofs a naive form of
;; CAP negotiation, which obviously can't coexist with the real thing.

;;; Code:

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-scenarios-common)
(require 'erc-v3)

(defvar erc-sasl-authzid)
(defvar erc-sasl-mechanism)
(defvar erc-sasl-password)
(defvar erc-sasl-user)
(defvar sasl-unique-id-function)

(defun erc-scenarios-v3-sasl--versatile-init ()
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/sasl")
       (erc-server-flood-penalty 0.1)
       (erc-email-userid "tester@school")
       (dumb-server (erc-d-run "localhost" t 'plain))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-sasl-mechanism 'plain)
       (erc-sasl-user :nick)
       (erc-sasl-password "password123")
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

    (ert-info ("Notices received")
      (with-current-buffer "ExampleOrg"
        (funcall expect 10 "This server is in debug mode")
        ;; Regression "\0\0\0\0 ..." caused by (fillarray passphrase 0)
        (should (string= erc-sasl-password "password123"))))))

(ert-deftest erc-scenarios-v3-sasl--extension-only ()
  :tags '(:expensive-test)
  (should-not (memq 'sasl erc-v3-extensions))
  (should-not (memq 'sasl erc-modules))
  (let ((erc-v3-extensions '(sasl)))
    (erc-scenarios-v3-sasl--versatile-init)))

(ert-deftest erc-scenarios-v3-sasl--module-only ()
  :tags '(:expensive-test)
  (should-not (memq 'sasl erc-v3-extensions))
  (should-not (memq 'sasl erc-modules))
  (let ((erc-v3-extensions nil)
        (erc-modules (cons 'sasl erc-modules)))
    (erc-scenarios-v3-sasl--versatile-init)))

(ert-deftest erc-scenarios-v3-sasl--module-and-extension ()
  :tags '(:expensive-test)
  (should-not (memq 'sasl erc-v3-extensions))
  (should-not (memq 'sasl erc-modules))
  (let ((erc-v3-extensions '(sasl))
        (erc-modules (cons 'sasl erc-modules)))
    (erc-scenarios-v3-sasl--versatile-init)))

(defun erc-v3-scenarios--common--v3-sasl (mech)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/sasl")
       (erc-d-linger-secs 0.5)
       (erc-server-flood-penalty 0.1)
       (dumb-server (erc-d-run "localhost" t mech))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(sasl))
       (erc-sasl-user :nick)
       (erc-sasl-mechanism mech)
       (mock-rvs (list "c5RqLCZy0L4fGkKAZ0hujFBs" ""))
       (sasl-unique-id-function (lambda () (pop mock-rvs)))
       (inhibit-message noninteractive)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "jilles"
                                :password "sesame"
                                :full-name "jilles")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (ert-info ("Notices received")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "jaguar"))
        (funcall expect 10 "Found your hostname")
        (funcall expect 20 "marked as being away")))))

;; This one has a 3.2 extension value "sasl=mech".  It also simulates
;; a user having the `sasl' module loaded.

(ert-deftest erc-scenarios-v3-sasl--scram-sha-1 ()
  :tags '(:expensive-test)
  (let ((erc-modules (cons 'sasl erc-modules))
        (erc-sasl-authzid "jilles"))
    (erc-v3-scenarios--common--v3-sasl 'scram-sha-1)))

;; This one has no value, i.e., LS cap-notify sasl batch ...
;; Just to mix things up.

(ert-deftest erc-scenarios-v3-sasl--scram-sha-256 ()
  :tags '(:expensive-test)
  (unless (featurep 'sasl-scram-sha256)
    (ert-skip "Emacs lacks sasl-scram-sha256"))
  (erc-v3-scenarios--common--v3-sasl 'scram-sha-256))

(ert-deftest erc-scenarios-v3-sasl--authentication-failure ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/sasl")
       (erc-server-flood-penalty 0.1)
       (dumb-server (erc-d-run "localhost" t 'plain-failed))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(sasl))
       (erc-sasl-password "wrong")
       (erc--warnings-buffer-name  "*ERC test warnings*")
       (warnings-buffer (get-buffer-create erc--warnings-buffer-name))
       (inhibit-message noninteractive)
       (expect (erc-d-t-make-expecter)))

    (with-current-buffer (erc :server "127.0.0.1"
                              :port port
                              :nick "tester"
                              :user "tester"
                              :full-name "tester")
      (funcall expect 10 "Opening connection")
      (funcall expect 20 "SASL authentication failed")
      (funcall expect 20 "Connection failed!")
      (should-not (erc-server-process-alive)))

    (with-current-buffer warnings-buffer
      (funcall expect 10 "please review SASL settings")))

  (when noninteractive
    (should-not (get-buffer "*ERC test warnings*"))))

(ert-deftest erc-scenarios-v3-sasl--unsupported-mechanism ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/sasl")
       (erc-server-flood-penalty 0.1)
       (dumb-server (erc-d-run "localhost" t 'plain-unsupported))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(sasl))
       (erc-sasl-mechanism 'scram-sha-1)
       (erc--warnings-buffer-name  "*ERC test warnings*")
       (warnings-buffer (get-buffer-create erc--warnings-buffer-name))
       (inhibit-message noninteractive)
       (expect (erc-d-t-make-expecter))
       (buf nil))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :user "tester"
                                :full-name "tester")
        (funcall expect 10 "mechanism")
        (funcall expect 20 "aborting")
        (funcall expect 20 "Connection failed!")))

    (with-current-buffer warnings-buffer
      (funcall expect 10 "please review SASL settings")))

  (when noninteractive
    (should-not (get-buffer "*ERC test warnings*"))))

;;; erc-scenarios-v3-sasl.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

