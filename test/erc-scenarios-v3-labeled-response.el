;;; erc-scenarios-v3-labeled-response.el --- labeled response scenarios -*- lexical-binding: t -*-

;; Copyright (C) 2022 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:
;;; Code:

;; TODO
;;
;; Cover this (as of 05/2024, the echo placeholder is not erased until the last
;; NOTICE in the batch is printed):
;;
;;   @label=42 PRIVMSG NickServ :help identify
;;   @label=42 :irc.foonet.org BATCH +5 labeled-response
;;   @msgid=123;time=$TIME;batch=5 :dummy PRIVMSG NickServ :help identify...
;;   @time=$TIME;batch=5 :NickServ NOTICE dummy :*** ^BickServ HELP^B ***...
;;   @batch=5;time=$TIME :NickServ NOTICE dummy :Syntax: ^BIDENTIFY <user...
;;   @time=$TIME;batch=5 :NickServ NOTICE dummy :IDENTIFY lets you login ...
;;   @time=$TIME;batch=5 :NickServ NOTICE dummy :certfp (your client cert...
;;   @time=$TIME;batch=5 :NickServ NOTICE dummy :*** ^BEnd of NickServ He...
;;   :irc.foonet.org BATCH -5

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-scenarios-common)
(require 'erc-v3)
(eval-when-compile (require 'erc-labeled-response))

;; These two tests are more about general cap negotiation than
;; `labeled-respone' specifically.  It's just, at the moment, the
;; latter is our only extension with dependencies.

(ert-deftest erc-scenarios-v3-labeled-response--unoffered-dependency ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/labeled-response")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars '((ls-batch . "")))
       (dumb-server (erc-d-run "localhost" t 'missing-dependency))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(labeled-response batch echo-message))
       (erc-labeled-response--device-id "laptop")
       (erc-labeled-response--counter 0)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "FooNet"))
      (funcall expect 5 "Problem resolving dependencies"))))

(ert-deftest erc-scenarios-v3-labeled-response--unrequested-dependency ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/labeled-response")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars '((ls-batch . " batch")))
       (dumb-server (erc-d-run "localhost" t 'missing-dependency))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(labeled-response echo-message))
       (erc-labeled-response--device-id "laptop")
       (erc-labeled-response--counter 0)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "FooNet"))
      (funcall expect 5 "Problem resolving dependencies"))))

(defun erc-scenarios-v3-labeled-response--placeholder-p (&optional point)
  (let ((val (get-text-property (or point (1- (point))) 'font-lock-face)))
    (if (consp val) (memq 'erc-pending val) (eq 'erc-pending val))))

(defun erc-scenarios-v3-labeled-response--speaker-p (nick)
  (and-let* ((p (or (and (get-text-property (pos-bol) 'erc--speaker) (point))
                    (next-single-property-change (pos-bol) 'erc--speaker)))
             (found (get-text-property p 'erc--speaker))
             ((equal nick found)))
    p))

(defun erc-scenarios-v3-labeled-response--merged-p (nick)
  (and-let* ((p (erc-scenarios-v3-labeled-response--speaker-p nick)))
    (equal "" (get-text-property p 'display))))

(defun erc-scenarios-v3-labeled-response--replaced-p (needle)
  ;; The first occurrence of NEEDLE is the echoed reply and not a
  ;; placeholder.
  (erc-d-t-wait-for 10 (format "Placeholder %S replaced" needle)
    (goto-char (point-min))
    (when (search-forward needle nil t)
      (or (eq 'erc-input-face (get-text-property (1- (point))
                                                 'font-lock-face))
          (and-let* ((val (get-text-property (1+ (match-beginning 0))
                                             'font-lock-face))
                     ((consp val)))
            (memq 'erc-my-nick-face val))))))

(ert-deftest erc-scenarios-v3-labeled-response--echo-message ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/labeled-response")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'echo-message))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(labeled-response
                            server-time batch echo-message account-tag))
       (erc-labeled-response--device-id "laptop")
       (erc-labeled-response--counter 0)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :user "tester"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "foonet"))
      (funcall expect 10 "User modes for tester")
      (erc-cmd-JOIN "#chan"))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "#chan"))
      (funcall expect 5 "Go you, and tell")
      (erc-scenarios-common-say "bob: hi")
      (funcall expect 1 "<tester> bob: hi")
      (should (eq 'erc-pending
                  (get-text-property (1- (point)) 'font-lock-face)))
      (erc-d-t-wait-for 10 "Placeholder replaced"
        (goto-char (point-min))
        (when (search-forward "<tester> bob: " nil t)
          (eq 'erc-input-face (get-text-property (point) 'font-lock-face))))
      (funcall expect 10 "alice: With nought" (point))

      (ert-info ("Completely removes placeholder")
        (funcall expect 0.01 "<tester> bob: hi" (point-min))
        (funcall expect -0.01 "<tester> bob: hi"))

      (erc-cmd-QUIT ""))

    (with-current-buffer "foonet"
      (funcall expect 10 "Quit"))))

(declare-function erc-multiline--send-current-line "erc-multiline" nil)

;; This asserts that integrations, like fill-wrap and stamp, don't
;; perform state-keeping operations on inserted placeholders.  This is
;; currently implemented via an `erc--ephemeral' text property.
(ert-deftest erc-scenarios-v3-labeled-response--ephemeral ()
  :tags '(:expensive-test)
  (when (< emacs-major-version 28)
    (ert-skip "FIXME make reliable"))
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/labeled-response")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'ephemeral))
       (port (process-contact dumb-server :service))
       (erc-autojoin-channels-alist '((foonet "#chan")))
       (erc-modules `(v3 fill-wrap scrolltobottom ,@erc-modules))
       (erc-v3-extensions `(labeled-response multiline ,@erc-v3-extensions))
       (erc-labeled-response--device-id "laptop")
       (erc-labeled-response--counter 0)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "#chan"))
      (unless noninteractive (set-window-buffer nil (current-buffer)))
      (funcall expect 5 "<bob> This thou shalt answer")
      (erc-scenarios-common-say "hi")
      (funcall expect 10 "<tester> hi")
      (should-not (erc-scenarios-v3-labeled-response--merged-p "tester"))
      (should (erc-scenarios-v3-labeled-response--placeholder-p))

      (ert-info ("Consecutive placeholders aren't merged by `fill-wrap'")
        (erc-scenarios-common-say "there")
        (funcall expect 5 "<tester> there")
        (should (erc-scenarios-v3-labeled-response--placeholder-p))
        (should (bound-and-true-p erc-fill-wrap-merge))
        (should-not (erc-scenarios-v3-labeled-response--merged-p "tester"))
        (erc-scenarios-v3-labeled-response--replaced-p "<tester> there"))
      (ert-info ("Final echoed message is merged by `fill-wrap'")
        (should (looking-back "<tester> there"))
        (should (erc-scenarios-v3-labeled-response--merged-p "tester")))

      (funcall expect 10 "<bob> alice: For me most wretched,")
      (funcall expect 10 "<alice> bob: He should have worn the horns")

      (ert-info ("Non-multiline input correctly inserted")
        (let (erc-accidental-paste-threshold-seconds inhibit-interaction)
          (goto-char erc-input-marker)
          (insert "this\nis not a\nmultiline\nmessage")
          (ert-simulate-keys "n\r" (erc-multiline--send-current-line)))
        (funcall expect 10 "<tester> this")
        (dolist (m '("is not a" "multiline" "message"))
          (funcall expect 10 (concat "<tester> " m))
          (should (erc-scenarios-v3-labeled-response--placeholder-p))
          (should-not (erc-scenarios-v3-labeled-response--merged-p "tester")))
        (ert-info ("Replacements merged normally")
          (erc-scenarios-v3-labeled-response--replaced-p "<tester> this")
          (should-not (erc-scenarios-v3-labeled-response--merged-p "tester"))
          (erc-scenarios-v3-labeled-response--replaced-p "<tester> is not a")
          (should (erc-scenarios-v3-labeled-response--merged-p "tester"))
          (erc-scenarios-v3-labeled-response--replaced-p "<tester> multiline")
          (should (erc-scenarios-v3-labeled-response--merged-p "tester")))
        (funcall expect 10 "<bob> alice: For they shall")
        (erc-scenarios-v3-labeled-response--replaced-p "<tester> message")
        (should-not (erc-scenarios-v3-labeled-response--merged-p "tester")))

      (ert-info ("Actual multiline input triggers merge")
        (funcall expect 10 "<alice> bob: The valiant Paris")
        (let (erc-accidental-paste-threshold-seconds inhibit-interaction)
          (goto-char erc-input-marker)
          (insert "one\ntwo\nthree\nfour")
          (ert-simulate-keys "y\r" (erc-multiline--send-current-line)))
        (funcall expect 10 "<tester> #+BEGIN_MULTILINE")
        (should (erc-scenarios-v3-labeled-response--placeholder-p (pos-bol)))
        (funcall expect 10 "#+END_MULTILINE")
        (funcall expect 10 "<bob> alice: You have paid the heavens")

        (erc-scenarios-v3-labeled-response--replaced-p
         "<tester> #+BEGIN_MULTILINE")
        (erc-scenarios-common-say "okay")
        (funcall expect 10 "okay")
        (should (erc-scenarios-v3-labeled-response--placeholder-p))
        (should-not (erc-scenarios-v3-labeled-response--merged-p "tester"))
        (erc-scenarios-v3-labeled-response--replaced-p "<tester> okay")
        (should (erc-scenarios-v3-labeled-response--merged-p "tester")))

      (funcall expect 10 "<alice> bob: Commit my cause")
      (erc-scenarios-common-say "wow")
      (funcall expect 10 "wow")
      (should (erc-scenarios-v3-labeled-response--placeholder-p))
      (should-not (erc-scenarios-v3-labeled-response--merged-p "tester"))
      (erc-scenarios-v3-labeled-response--replaced-p "<tester> wow")
      (should-not (erc-scenarios-v3-labeled-response--merged-p "tester"))

      (funcall expect 10 "<alice> bob: Hold thee, there's my purse."))

    (ert-info ("Abort timer fires and removes placeholder")
      (with-current-buffer "foonet"
        (let ((erc-labeled-response--echo-abort-timeout 0.1))
          (erc-scenarios-common-say "erroneous command")
          (funcall expect 1 "<tester> erroneous command")
          (should (erc-scenarios-v3-labeled-response--placeholder-p)))
        (funcall expect 1 "No target")
        (erc-d-t-wait-for 1 "Placeholder disappears"
          (goto-char (point-min))
          (not (search-forward "<tester> erroneous command" nil t)))))))

(ert-deftest erc-scenarios-v3-labeled-response--self-query ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/labeled-response")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'self-query))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(labeled-response
                            server-time batch echo-message account-tag))
       (erc-labeled-response--device-id "laptop")
       (erc-labeled-response--counter 0)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :user "tester"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "foonet"))
      (funcall expect 10 "User modes for tester")
      (erc-cmd-QUERY "tester"))

    (with-current-buffer "tester"
      (ert-info ("Send a message to self")
        (erc-scenarios-common-say "note to self")
        (funcall expect 1 "note to self")
        (should (eq 'erc-pending
                    (get-text-property (1- (point)) 'font-lock-face))))

      (erc-d-t-wait-for 10 "Placeholder replaced"
        (goto-char (point-min))
        (when (search-forward "<tester> note to self" nil t)
          (eq 'erc-input-face
              (get-text-property (1- (point)) 'font-lock-face))))

      (ert-info ("No duplicates")
        (funcall expect 0.01 "note to self" (point-min))
        (funcall expect -0.01 "note to self"))
      (erc-cmd-QUIT ""))

    (with-current-buffer "foonet"
      (funcall expect 10 "Quit"))))

(ert-deftest erc-scenarios-v3-labeled-response--whois ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/labeled-response")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'whois))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(labeled-response server-time batch echo-message))
       (erc-labeled-response--device-id "laptop")
       (erc-labeled-response--counter 0)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "FooNet"))
      (funcall expect 5 "logged in as tester"))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "#chan"))
      (funcall expect 5 "These present wars")
      (erc-cmd-WHOIS "alice"))

    (with-current-buffer "FooNet"
      (erc-cmd-WHOIS "bob"))

    (with-current-buffer "#chan"
      (funcall expect 5 "alice is Irc bot")
      (funcall expect 5 "alice is/was on server irc.example.net")
      (funcall expect 5 "Z-lined: Your IP")
      (ert-info ("No foreign WHOIS present")
        (erc-d-t-absent-for 0.1 "alice is Fake Name")
        (erc-d-t-absent-for 0.1 "alice is/was on server irc.example.org")))

    (with-current-buffer "FooNet"
      (funcall expect 5 "No such nick"))))

;;; erc-scenarios-v3-labeled-response.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

