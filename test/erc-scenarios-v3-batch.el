;;; erc-scenarios-v3-batch.el --- v3 batch extension scenarios -*- lexical-binding: t -*-

;; Copyright (C) 2022 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Code:
(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-scenarios-common)
(require 'erc-v3)

(declare-function erc-batch--es "erc-batch" nil)
(defvar erc-span--use-lisp-threads-p)

;; Let-binding options is possible when batch is active because we're
;; no longer using lisp threads.  Should therefore set some realistic
;; or popular options, such as `erc-fill-function'.

(ert-deftest erc-scenarios-v3-batch-chathistory-chan ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/batch")
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p (not (getenv "CI")))
       (erc-stamp--tz (- (* 8 60 60)))
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'chathistory-chan))
       (port (process-contact dumb-server :service))
       ;; Maybe remove `nicks' or make presence random.
       (erc-modules (cons 'nicks (cons 'v3 erc-modules)))
       (erc-v3-extensions '(server-time batch))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "password123"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

    (ert-info ("Notices received")
      (with-current-buffer "ExampleOrg"
        (funcall expect 10 "Setting your virtual host")))

    (erc-d-t-wait-for 10 "buffer #foo ready" (get-buffer "#foo"))

    (ert-info ("Chan buffer #foo populated")
      (with-current-buffer "#foo"
        (funcall expect 30 "[Thu Dec 31 2020]")
        (funcall expect 10 ">>> RANGE-BEG 2021-01-01T04:35:31.906Z")
        (funcall expect 20 "<focused-ne> Then into")
        (funcall expect 20 "<gallant> Would be active") ; reordered
        (funcall expect 20 "<focused-ne> Worse if")
        (funcall expect 10 "[Fri Jan  1 2021]")
        (funcall expect 20 "<gallant> Tphsjiomjiheey") ; reordered
        (funcall expect 10 "[Sat Jan  2 2021]")
        (funcall expect 20 "<TRUST> Time. Makes sense.")
        (funcall expect 20 "<focused-ne> Their ahqwan of nwak")
        (funcall expect 20 "<gallant> that have sent") ; reordered
        (funcall expect 20 "<SHA> Talk to me")
        (funcall expect 20 "<SHA> So if I code.")
        (funcall expect 10 "[Sun Jan  3 2021]")
        (funcall expect 20 "<focused-ne> Sppleppodk")
        (funcall expect 20 "<focused-ne> #frllklw-klwjlsrs")
        (funcall expect 10 "<<< RANGE-END 2021-01-01T04:35:31.906Z")
        (funcall expect 10 '(: "[" (+ (any alnum " ")) "]"))
        (let ((stamp (match-string 0))) ; no redundant stamps
          (funcall expect -0.1 stamp))
        (funcall expect 10 "You have joined channel #foo")
        (funcall expect 5 "Users on #foo (13): @YkmnSvtv beyo dkm4")
        (funcall expect 5 "SHA tester TRUST")
        (funcall expect 5 "#foo modes:")
        (funcall expect 5 "#foo was")
        (funcall expect 5 "<gallant> According")
        (funcall expect 5 "<focused-ne> Or never")
        (funcall expect 5 erc-prompt)

        (ert-info ("Button integration succeeds")
          ;; Historical user no longer present in room.
          (should-not (erc-get-channel-user "someone"))
          ;; But their button data may remain (e.g., when `nicks' mode on).
          (erc-d-t-search-for 10 "someone> blah")
          (let ((dt (get-text-property (match-beginning 0) 'erc-data)))
            (if (bound-and-true-p erc-button--phantom-users-mode)
                ;; Timestamp reflects historical time rather than now.
                (should (equal (erc-channel-user-last-message-time (nth 2 dt))
                               (if (>= emacs-major-version 29)
                                   '(1609599649511 . 1000)
                                 '(24560 35489 511000 0))))
              (should-not dt))))))

    (erc-d-t-wait-for 10 "buffer #bar ready" (get-buffer "#bar"))

    ;; This no longer features a "nested" batch and is thus useless.
    (ert-info ("Chan buffer #bar populated")
      (with-current-buffer "#bar"
        (funcall expect 5 ">>> RANGE-BEG 2021-01-03T06:39:18.437Z")
        (funcall expect 1 "<Zealous_> anyone know what")
        (funcall expect 1 "<tester> anyway i'm really just")
        (funcall expect 5 "<Zealous_> who is it")
        (funcall expect 5 "<Zealous_> I won't either (likely")
        (funcall expect 1 "<<< RANGE-END 2021-01-03T06:39:18.437Z")
        (funcall expect 5 "You have joined channel #bar")
        (funcall expect 5 "Users on #bar (44): Blissf Boring_")
        (funcall expect 5 "Cxfixioiyzrcz Cyh")
        (funcall expect 5 "#bar modes:")
        (funcall expect 5 "#bar was")
        (funcall expect 5 "<Zealous_> If that may be")
        (funcall expect 5 "<fuofzhh> Here, at your")
        (funcall expect 5 erc-prompt)))

    (erc-d-t-wait-for 10 "buffer #baz ready" (get-buffer "#baz"))

    ;; From https://ircv3.net/specs/extensions/batch-3.2
    ;;
    ;; > An empty batch is allowed, and SHOULD be treated as a no-op, unless
    ;; > demanded otherwise by the specification for that batch type.
    ;;
    (ert-info ("Chan buffer #baz populated")
      (with-current-buffer "#baz"
        (funcall expect 5 "Users on #baz (30): aeyao agpph")
        (funcall expect 5 "yeuy ypezp")
        (funcall expect 5 "#baz modes:")
        (funcall expect 5 "#baz was")
        (funcall expect 5 erc-prompt)
        (erc-d-t-absent-for 0.5 "RANGE-BEG")))

    (erc-d-t-wait-for 5 "buffer #spam ready" (get-buffer "#spam"))

    (ert-info ("Chan buffer #spam populated")
      (with-current-buffer "#spam"
        (funcall expect 10 ">>> RANGE-BEG 2020-12-31T13:18:27.891Z")
        (funcall expect 9 "<ELATEDJONE> in yatyopdaot, i don't know")

        ;; Implementation wise, this asserts that advice for
        ;; `erc--cmem-from-nick-function' is layered properly.
        (ert-info ("Historical users prop data has correct last-message time")
          (pcase-let ((`(,n ,u ,c) (get-text-property (1+ (match-beginning 0))
                                                      'erc-data))
                      (`(,user . ,cusr) (erc-get-channel-member "ELATEDJONE")))
            (should (string= n "ELATEDJONE"))
            (should-not (eq u user))
            (should-not (eq c cusr))
            ;; ELATEDJONE only speaks in the backlog.  However...
            ;;
            ;; FIXME we should copy over last-message times if nick
            ;; and account of historical speaker matches current.
            (should-not (erc-channel-user-last-message-time cusr))
            (should (equal (erc-channel-user-last-message-time c)
                           (if (>= emacs-major-version 29)
                               '(1609677062258 . 1000)
                             '(24561 47366 258000 0))))))

        (funcall expect 5 "<<< RANGE-END 2020-12-31T13:18:27.891Z")
        (funcall expect 5 "You have joined channel #spam")
        (funcall expect 1 "Users on #spam (28): Blissf Boring_keller")
        (funcall expect 1 "tester Upb")
        (funcall expect 5 "#spam modes:")
        (funcall expect 5 "#spam was")
        (funcall expect 5 "<jol> oi")
        (funcall expect 5 "<focused-ne> Protest their first of manhood")

        (ert-info ("Live speaker times updated in `erc-channel-users'")
          ;; RELAX speaks in backlog and live, but only live matters.
          (should (time-less-p (time-subtract (current-time) 20)
                               (erc-channel-user-last-message-time
                                (cdr (erc-get-channel-user "RELAX"))))))
        (funcall expect 5 erc-prompt)))

    (with-current-buffer "ExampleOrg"
      (should-not (erc-batch--es)))))

(ert-deftest erc-scenarios-v3-batch-chathistory-query ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/batch")
       (erc-d-linger-secs 3)
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p (not (getenv "CI")))
       (erc-stamp--tz (- (* 8 60 60)))
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'chathistory-query))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "password123"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

    (ert-info ("Notices received")
      (with-current-buffer "ExampleOrg"
        (funcall expect 10 "marked as being away")))

    (erc-d-t-wait-for 10 "buffer #foo ready" (get-buffer "#foo"))

    (ert-info ("Chan buffer #foo populated")
      (with-current-buffer "#foo"
        (funcall expect 10 "[Thu Dec 31 2020]")
        (funcall expect 10 ">>> RANGE-BEG 2021-01-01T04:35:31.906Z")
        (funcall expect 10 "[Sun Jan  3 2021]")
        (funcall expect 5 "<focused-ne> #frllklw-klwjls")
        (funcall expect 5 "<<< RANGE-END 2021-01-01T04:35:31.906Z")
        (funcall expect 5 "You have joined channel #foo")
        (funcall expect 5 "Users on #foo (13): @YkmnSvtv beyo dkm4")
        (funcall expect 5 "SHA tester TRUST")
        (funcall expect 5 "#foo modes:")
        (funcall expect 5 "#foo was")
        (funcall expect 5 "<focused-ne> Hi")
        (funcall expect 5 erc-prompt)))

    (erc-d-t-wait-for 5 "buffer *keyword ready" (get-buffer "*keyword"))

    (ert-info ("Query buffer *keyword populated")
      (with-current-buffer "*keyword"
        (funcall expect 5 "[Wed Dec 30 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2020-12-30T17:21:31.099Z")
        (funcall expect 5 "Lorem ipsum dolor sit amet")
        (funcall expect 5 "voluptate velit esse cillum dolore eu fugiat")
        (funcall expect 5 "<<< RANGE-END 2020-12-30T17:21:31.099Z")
        (funcall expect 5 erc-prompt)))

    (erc-d-t-wait-for 5 "buffer bob ready" (get-buffer "bob"))

    (ert-info ("Query buffer bob populated")
      (with-current-buffer "bob"
        (funcall expect 5 "[Tue Dec 29 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2020-12-29T18:10:11.219Z")
        (funcall expect 5 "To Laced mon did my land extend.")
        (funcall expect 5 "Younger than she are happy mothers made")
        (funcall expect 5 "<<< RANGE-END 2020-12-29T18:10:11.219Z")
        (funcall expect 5 "<bob> Hey")
        (funcall expect 5 erc-prompt)))))

;; Check for regression in which playback for a *highlight buffer on a
;; second network causes issues
(ert-deftest erc-scenarios-v3-batch-chathistory-query-double ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/batch")
       (erc-d-linger-secs 8)
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p (not (getenv "CI")))
       (erc-stamp--tz (- (* 8 60 60)))
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t
                               'chathistory-query-foonet
                               'chathistory-query-barnet))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect to foonet")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "foonet:changeme"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "foonet"))

    (ert-info ("Connect to barnet")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "barnet:changeme"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (ert-info ("Playback for #chan@foonet received")
      (with-current-buffer
          (erc-d-t-wait-for 10 "buffer #chan@foonet ready"
            (get-buffer "#chan@foonet"))
        (funcall expect 5 "[Thu Dec 31 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2021-01-01T04:35:31.906Z")
        (funcall expect 5 "<focused-ne> Then into a single")
        (funcall expect 5 "<focused-ne> Worse if not")
        (funcall expect 5 "<<< RANGE-END 2021-01-01T04:35:31.906Z")
        (funcall expect 5 "You have joined channel #chan")
        (funcall expect 5 "Users on #chan (13): @YkmnSvtv beyo dkm4")
        (funcall expect 5 "SHA tester TRUST")
        (funcall expect 5 "#chan modes:")
        (funcall expect 5 "#chan was")
        (funcall expect 5 "<focused-ne> Hi")
        (funcall expect 5 erc-prompt)))

    (ert-info ("Playback for #chan@barnet received")
      (with-current-buffer
          (erc-d-t-wait-for 10 "buffer #chan@barnet ready"
            (get-buffer "#chan@barnet"))
        (funcall expect 5 "[Sat Jan  2 2021]")
        (funcall expect 5 ">>> RANGE-BEG 2021-01-03T07:36:20.310Z")
        (funcall expect 5 "<SHA> Emacs for me")
        (funcall expect 5 "[Sun Jan  3 2021]")
        (funcall expect 5 "<focused-ne> #frllklw-klwjlsrs")
        (funcall expect 5 "<<< RANGE-END 2021-01-03T07:36:20.310Z")
        (funcall expect 5 "You have joined channel #chan")
        (funcall expect 5 "Users on #chan (13): @YkmnSvtv beyo dkm4")
        (funcall expect 5 "SHA tester TRUST")
        (funcall expect 10 "#chan modes:")
        (funcall expect 5 "#chan was")
        (funcall expect 5 "<SHA> Oi")
        (funcall expect 5 erc-prompt)))

    (ert-info ("Query buffer *keyword@foonet populated")
      (with-current-buffer (erc-d-t-wait-for 10 "buffer *keyword@foonet ready"
                             (get-buffer "*keyword@foonet"))
        (funcall expect 5 "[Wed Dec 30 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2020-12-30T17:21:31.099Z")
        (funcall expect 5 "Lorem ipsum dolor sit amet")
        (funcall expect 5 "consectetur adipiscing elit")
        (funcall expect 5 "<<< RANGE-END 2020-12-30T17:21:31.099Z")
        (funcall expect 5 erc-prompt)))

    (ert-info ("Query buffer *keyword@barnet populated")
      (with-current-buffer (erc-d-t-wait-for 10 "buffer *keyword@barnet ready"
                             (get-buffer "*keyword@barnet"))
        (funcall expect 5 "[Wed Dec 30 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2020-12-30T17:24:55.099Z")
        (funcall expect 5 "Duis aute irure dolor")
        (funcall expect 5 "voluptate velit esse cillum")
        (funcall expect 5 "<<< RANGE-END 2020-12-30T17:24:55.099Z")
        (funcall expect 5 erc-prompt)))

    (ert-info ("Query buffer bob@foonet populated")
      (with-current-buffer (erc-d-t-wait-for 5 "buffer bob ready"
                             (get-buffer "bob@foonet"))
        (funcall expect 5 "[Tue Dec 29 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2020-12-29T18:10:11.219Z")
        (funcall expect 5 "To Laced mon did my land extend.")
        (funcall expect 5 "This is but a custom")
        (funcall expect 5 "<<< RANGE-END 2020-12-29T18:10:11.219Z")
        (funcall expect 5 "<bob> Hey")
        (funcall expect 5 erc-prompt)))

    (ert-info ("Query buffer bob@barnet populated")
      (with-current-buffer (erc-d-t-wait-for 5 "buffer bob ready"
                             (get-buffer "bob@barnet"))
        (funcall expect 5 "[Tue Dec 29 2020]")
        (funcall expect 5 ">>> RANGE-BEG 2020-12-29T18:14:41.259Z")
        (funcall expect 5 "But, in defence")
        (funcall expect 5 "Younger than")
        (funcall expect 5 "<<< RANGE-END 2020-12-29T18:14:41.259Z")
        (funcall expect 10 "<bob> Ey")
        (funcall expect 5 erc-prompt)))))

;; This happens to demonstrate duplicate timestamps printed on
;; reconnect.  See `chathistory' module for deduping facility.

(ert-deftest erc-scenarios-v3-batch-chathistory-reconnect ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/batch")
       (erc-d-linger-secs 0.5)
       (erc-server-flood-penalty 0.1)
       erc-autojoin-channels-alist
       last-ts
       (erc-d-tmpl-vars
        `(,@erc-d-tmpl-vars
          (now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))
          (recently . ,(lambda ()
                         (prog1
                             (format-time-string "%FT%T.%3NZ" last-ts t)
                           (setq last-ts (time-subtract (current-time) 1)))))))
       (dumb-server (erc-d-run "localhost" t
                               'chathistory-reconnect-a
                               'chathistory-reconnect-b))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch))
       (expect (erc-d-t-make-expecter))
       last)

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "changeme"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "foonet"))

    (ert-info ("Notices received")
      (with-current-buffer "foonet"
        (funcall expect 10 "marked as being away")))

    (erc-d-t-wait-for 10 "buffer #chan ready" (get-buffer "#chan"))

    (ert-info ("Chan buffer #chan populated")
      (with-current-buffer "#chan"
        (funcall expect 5 "[Sun Jul 18 2021]")
        (funcall expect 5 ">>> RANGE-BEG 2021-07-18T18:07:42.036Z")
        (funcall expect 5 "<alice> bob: To Laced mon did")
        (funcall expect 5 "<bob> alice: Younger than she")
        (funcall expect 5 "<<< RANGE-END 2021-07-18T18:07:42.036Z")
        (funcall expect 5 "You have joined channel #chan")
        (funcall expect 5 "Users on #chan (3):")
        (funcall expect 10 "#chan modes:")
        (funcall expect 5 "#chan was")
        (setq last (funcall expect 5 "<bob> alice: But purgatory"))
        (funcall expect 5 erc-prompt)
        (erc-cmd-QUIT "")))

    (sleep-for 2)
    (setq last-ts (time-subtract (current-time) 1))

    (should erc-autojoin-channels-alist)
    (setq erc-autojoin-channels-alist nil) ; kludge

    (ert-info ("Reconnect")
      (with-current-buffer "#chan"
        (erc-d-t-wait-for 4 "proc dies" (not (erc-server-process-alive)))
        (erc-cmd-RECONNECT)
        (funcall expect 10 "RANGE-END" last)
        (funcall expect 10 "RANGE-BEG")
        (funcall expect 5 "<alice> bob: Thou pout'st upon thy")
        (funcall expect 5 "<bob> alice: With these mortals")
        (funcall expect 5 "RANGE-END")
        (funcall expect 10 "You have joined channel #chan")
        (funcall expect 5 "#chan modes:")
        (funcall expect 5 "#chan was created on")
        (funcall expect 5 "RANGE-BEG")
        (funcall expect 5 "<alice> bob: Wilt thou rest damned")
        (funcall expect 5 erc-prompt)))

    (with-current-buffer "bob"
      (funcall expect 10 "[Sun Jul 18 2021]")
      (funcall expect 10 ">>> RANGE-BEG 2021-07-18T18:07:49.396Z")
      (funcall expect 1 "<bob> To Laced mon did")
      (funcall expect 1 "<bob> This is but a custom in")
      (funcall expect 1 "<<< RANGE-END 2021-07-18T18:07:49.396Z")
      (funcall expect 1 "<bob> Hey")
      (funcall expect 1 "RANGE-BEG")
      (funcall expect 1 "faults that are rich are")
      (funcall expect 1 "RANGE-END")
      (funcall expect 1 "<bob> Hi")
      (funcall expect 5 erc-prompt))))

;;; erc-scenarios-v3-batch.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

