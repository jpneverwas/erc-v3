;;; erc-scenarios-v3.el --- scenario-driven test cases for ERC v3 -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; FIXME many of these tests assert brittle implementation details.
;; These should be replaced with behavioral assertions.

;;; Code:

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(defvar erc-autojoin-channels-alist)

(require 'erc-scenarios-common)
(require 'erc-v3)


;; See also test for userhost-in-names, which verifies correct 353
;; behavior.  This only verifies 352 RPL_WHOREPLY parsing for
;; non-WHOX sessions (rare).
(ert-deftest erc-scenarios-v3-multi-prefix ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/multi-prefix")
       (erc-server-flood-penalty 0.1)
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (dumb-server-buffer (get-buffer "*erc-d-server*"))
       (erc-modules (cons 'v3 erc-modules))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

    (ert-info ("Capability enabled")
      (with-current-buffer "ExampleOrg"
        (erc-d-t-wait-for 5 "Extension enabled" erc-v3--multi-prefix)

        (funcall expect 5 "modes for tester")
        (ert-info ("Join chan #chan")
          (erc-cmd-JOIN "#chan"))))

    (erc-d-t-wait-for 5 "#chan buffer ready" (get-buffer "#chan"))

    (with-current-buffer "#chan"
      (should-not erc-v3--caps)
      (should erc-v3--multi-prefix)
      (should-not (local-variable-p 'erc-v3--caps))

      (erc-d-t-wait-for 5 "Own status updated"
        (erc-channel-user-op-p "tester"))

      (should-not (or (erc-channel-user-owner-p "tester")
                      (erc-channel-user-admin-p "tester")
                      (erc-channel-user-halfop-p "tester")
                      (erc-channel-user-voice-p "tester")))
      (should (erc-channel-user-op-p "tester"))

      (ert-info ("Add voice to self")
        (erc-scenarios-common-say "/MODE #chan +v tester")
        (erc-scenarios-common-say "/WHO #chan"))

      (erc-d-t-wait-for 5 "Voice added to self"
        (funcall expect 10 " ov "))

      (should-not (or (erc-channel-user-owner-p "tester")
                      (erc-channel-user-admin-p "tester")
                      (erc-channel-user-halfop-p "tester")))
      (should (erc-channel-user-op-p "tester"))
      (should (erc-channel-user-voice-p "tester")))))

(ert-deftest erc-scenarios-v3-userhost-in-names ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/userhost-in-names")
       (erc-server-flood-penalty 0.1)
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (dumb-server-buffer (get-buffer "*erc-d-server*"))
       (erc-modules (cons 'v3 erc-modules))
       (erc-autojoin-channels-alist '((ExampleOrg "#chan" "#spam")))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "password123"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (ert-info ("Capability enabled")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "ExampleOrg"))
        (erc-d-t-wait-for 5 "Extension enabled" erc-v3--userhost-in-names)
        (funcall expect 5 "modes for tester")))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))

      (ert-info ("Server-buffer-only vars absent from other buffers")
        (funcall expect 10 "alice")
        (goto-char (match-beginning 0))
        ;; Without this cap, we'd have a login (~user) and a host name
        ;; for Bob (because he speaks) but only a nick for Alice.
        (let ((user-data (erc-nick-at-point)))
          (should (eq user-data (erc-get-server-user "alice")))
          (should (erc-server-user-login user-data)) ; login means ~user
          (should (= 3 (hash-table-count erc-channel-users)))))

      (ert-info ("Prefixes correctly assigned")
        (should-not (erc-channel-user-owner-p "alice"))
        (should-not (erc-channel-user-admin-p "alice"))
        (should-not (erc-channel-user-op-p "alice"))
        (should-not (erc-channel-user-halfop-p "alice"))
        (should (erc-channel-user-voice-p "alice"))
        ;; bob
        (should-not (erc-channel-user-owner-p "bob"))
        (should-not (erc-channel-user-admin-p "bob"))
        (should (erc-channel-user-op-p "bob"))
        (should (erc-channel-user-halfop-p "bob"))
        (should (erc-channel-user-voice-p "bob")))

      (ert-info ("Buttonizing correctly applied")
        (should (equal (get-text-property (point) 'font-lock-face)
                       '(erc-button-nick-default-face erc-notice-face)))
        (should (search-forward "tester" nil t))
        ;; FIXME the face for the user's current-nick should appear in
        ;; front of `erc-default-face', but that's no longer the case.
        (should (memq 'erc-current-nick-face
                      (ensure-list
                       (get-text-property (1- (point)) 'font-lock-face)))))

      (ert-info ("No contamination in #chan")
        (erc-d-t-absent-for 0.001 '(or "joe" "mike" "ecstatic" "focused-ne"
                                       "gallant" "RELAX" "SHA" "TRUST"))))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#spam"))
      (ert-info ("No contamination in #spam")
        (should (= 9 (hash-table-count erc-channel-users)))
        (dolist (name '("joe" "mike" "ecstatic" "focused-ne" "gallant" "RELAX"
                        "SHA" "TRUST" "tester"))
          (erc-d-t-search-for 0.001 name))
        (erc-d-t-absent-for 0.001 '(or "bob" "alice"))))))

(ert-deftest erc-scenarios-v3-server-time ()
  :tags '(:expensive-test)
  (let ((erc-d-tmpl-vars
         `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t))))))
    (erc-scenarios-common-with-cleanup
        ((erc-scenarios-common-dialog "v3/server-time")
         (erc-server-flood-penalty 0.1)
         (erc-stamp--tz t)
         (dumb-server (erc-d-run "localhost" t 'basic))
         (port (process-contact dumb-server :service))
         (dumb-server-buffer (get-buffer "*erc-d-server*"))
         (erc-v3-extensions '(server-time))
         (erc-modules (cons 'v3 erc-modules))
         (expect (erc-d-t-make-expecter)))

      (ert-info ("Connect")
        (with-current-buffer (erc :server "127.0.0.1"
                                  :port port
                                  :nick "tester"
                                  :password "password123"
                                  :full-name "tester")
          (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

      (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

      (ert-info ("Capability enabled")
        (with-current-buffer "ExampleOrg"
          (erc-d-t-wait-for 5 "Extension enabled" erc-v3--server-time)
          (funcall expect 5 "modes for tester")))

      (erc-d-t-wait-for 2 "#foo buffer ready" (get-buffer "#foo"))
      (erc-d-t-wait-for 2 "#bar buffer ready" (get-buffer "#bar"))

      (with-current-buffer "#foo"
        ;; Wait for the the last message in replay because messages
        ;; are inserted out of order.
        (funcall expect 20 "<underpersona> \"Beware the")
        (funcall expect 2 "[Wed Sep 23 2020]" (point-min))
        (funcall expect 2 "<underpersona> 'Twas brillig")
        (funcall expect 2 "[23:50]")
        (funcall expect 2 "<underpersona> Did gyre and gimble")
        (funcall expect 2 "[23:51]")
        ;;
        (funcall expect 2 "[Thu Sep 24 2020]")
        (funcall expect 2 "<sneercat> All mimsy")
        (funcall expect 2 "[00:00]")
        (funcall expect 2 "<sneercat> And the mome")
        (funcall expect 2 "[00:01]")
        (funcall expect 2 "<underpersona> \"Beware the")
        (funcall expect 2 "[00:05]")
        (funcall expect 10 "\n\n[")
        (funcall expect 10 "*** You have joined channel #foo")
        (funcall expect 10 "*** Users on #foo")
        (funcall expect 10 "*** #foo modes:")
        (funcall expect 10 "*** #foo was created on"))

      (with-current-buffer "#bar"
        ;; Wait for the the last message in replay.
        (funcall expect 20 "<sneercat> And stood awhile")
        (funcall expect 10 "[Thu Sep 24 2020]" (point-min))
        (funcall expect 2 "<underpersona> The jaws that bite")
        (funcall expect 2 "[06:46]")
        (funcall expect 2 "<sneercat> Beware the Jubjub")
        (funcall expect 2 "[06:55]")
        (funcall expect 2 "<sneercat> The frumious")
        (funcall expect 2 "[06:56]")
        (funcall expect 2 "<underpersona> He took")
        (funcall expect 2 "[07:03]")
        (funcall expect 2 "<underpersona> Long time")
        (funcall expect 2 "<sneercat> So rested")
        (funcall expect 2 "[07:20]")
        (funcall expect 2 "<sneercat> And stood awhile")
        (funcall expect 10 "\n\n[")
        (funcall expect 10 "*** You have joined channel #bar")
        (funcall expect 10 "*** Users on #bar")
        (funcall expect 10 "*** #bar modes:")
        (funcall expect 10 "*** #bar was created on")))))

(ert-deftest erc-scenarios-v3-echo-message ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/echo-message")
       (erc-server-flood-penalty 0.1)
       erc-accidental-paste-threshold-seconds
       erc-autojoin-channels-alist
       (erc-d-tmpl-vars
        `(,@erc-d-tmpl-vars
          (now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time echo-message batch))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "changeme"
                                :full-name "tester")
        (funcall expect 10 "You have been marked as being away")))

    (ert-info ("Chan buffer #chan populated")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
        (funcall expect 5 "That lady is not now living")
        (erc-scenarios-common-say "hi")
        (funcall expect 5 "<tester> hi")
        (erc-d-t-absent-for 0.1 "<tester> hi" (point))
        (funcall expect 5 "<alice> bob: And reason says you")
        (erc-scenarios-common-say "/me sad")
        (funcall expect 5 "* tester sad")
        (erc-d-t-absent-for 0.1 "* tester sad" (point))
        (funcall expect 5 "<alice> bob: Than all yon fiery")
        ;; Sending a /msg opens a query on receipt of the echo.
        (erc-scenarios-common-say "/msg bob hi")))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "bob"))
      (funcall expect 1 "<tester> hi")
      (erc-d-t-absent-for 0.1 "<tester> hi" (point))
      (funcall expect 5 "<bob> Here lives a caitiff wretch")

      ;; Echoed non-ACTION CTCPs don't trigger an outgoing answer (NOTICE).
      (erc-scenarios-common-say "/ctcp bob VERSION")
      (funcall expect 10 "*** Version for bob is ERC")
      (erc-d-t-absent-for 0.1 "*** Version for tester" (point))

      (erc-scenarios-common-say "/me tired")
      (funcall expect 5 "* tester tired")
      (erc-d-t-absent-for 0.1 "* tester tired" (point)))

    (with-current-buffer "#chan"
      (funcall expect 5 "<bob> alice: Ha! now I see"))))

(ert-deftest erc-scenarios-v3-extended-join ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/extended-join")
       (erc-server-flood-penalty 0.1)
       erc-accidental-paste-threshold-seconds
       erc-autojoin-channels-alist
       (erc-d-tmpl-vars
        `(,@erc-d-tmpl-vars
          (now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time echo-message))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "tester:changeme"
                                :full-name "J. Random Hacker")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer
        (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))
      (funcall expect 5 "User modes for tester")
      (erc-cmd-JOIN "#chan"))
    (erc-d-t-wait-for 10 "buffer #chan ready" (get-buffer "#chan"))
    (should (equal erc-autojoin-channels-alist '((ExampleOrg "#chan"))))

    (ert-info ("Chan buffer #chan populated")
      (ert-with-message-capture messages
        (let ((inhibit-message t))
          (with-current-buffer "#chan"
            (funcall expect 5 "You have joined channel #chan")
            (funcall expect 5 "alice (aph: ~u@r7s6xb547r7hc.irc) has joined")
            (erc-scenarios-common--nick-at-point (match-beginning 0))
            (funcall expect 5 "alice (~u@r7s6xb547r7hc.irc) has left")
            (erc-scenarios-common--nick-at-point (match-beginning 0))
            (funcall expect 5 "alice (aph: ~u@r7s6xb547r7hc.irc) has joined")
            (erc-scenarios-common--nick-at-point (match-beginning 0))
            (funcall expect 5 "alice (~u@r7s6xb547r7hc.irc) has quit")
            (erc-scenarios-common--nick-at-point (match-beginning 0))))
        (with-temp-buffer
          (insert messages)
          (goto-char (point-min))
          (funcall expect 5 "aph: ~u@r7s6xb547r7hc.irc \"Alyssa P. Hacker\"")
          (funcall expect 5 "aph: ~u@r7s6xb547r7hc.irc \"Alyssa P. Hacker\"")
          (funcall expect 5 "aph: ~u@r7s6xb547r7hc.irc \"Alyssa P. Hacker\"")
          (funcall expect 5 "aph: ~u@r7s6xb547r7hc.irc \"Alyssa P. Hacker\"")
          nil)))))

;; For now, this also demos some account-tag stuff
;;
;; FIXME also include a scenario where names aren't received and
;; instead just a 315 sans token, after a 263 error:
;;
;;   :molybdenum.libera.chat 263 me WHO :This command could not be ...
;;   :molybdenum.libera.chat 315 me * :End of /WHO list.

(ert-deftest erc-scenarios-v3-whox--basic ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/whox")
       (erc-server-flood-penalty 0.1)
       erc-accidental-paste-threshold-seconds
       erc-autojoin-channels-alist
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch echo-message))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "tester:changeme"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer
        (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "foonet"))
      (funcall expect 10 "User modes for tester")
      (erc-cmd-JOIN "#chan")
      (erc-cmd-JOIN "#spam"))

    (ert-info ("Buffer #spam populated")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#spam"))
        (funcall expect 5 "You have joined channel #spam")
        (funcall expect 20 "<joe> mike: Excellent")))

    (ert-info ("Bob's account is known at join time")
      ;; FIXME accessing these library functions, though public, still
      ;; compromises the integrity of these UI-centric tests.
      (with-current-buffer "foonet"
        (should (equal (erc-server-user-account (erc-get-server-user "alice"))
                       "alice"))
        (should (equal (erc-server-user-account (erc-get-server-user "bob"))
                       "bob"))
        (should-not (erc-server-user-account (erc-get-server-user "fool")))))

    (with-current-buffer "#chan"
      (funcall expect 5 "<bob> alice: With him! why"))))

(ert-deftest erc-scenarios-v3-whox--rate-limited ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/whox")
       (erc-server-flood-penalty 0.1)
       erc-accidental-paste-threshold-seconds
       erc-autojoin-channels-alist
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'rate-limited))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch echo-message))
       (erc-v3--whox-mode-hook
        (list (lambda () (when erc-v3--whox
                           (setf (erc-v3--whox-penalty erc-v3--whox) 1)))))
       (expect (erc-d-t-make-expecter))
       (gecosp (lambda (name)
                 (let ((inhibit-message noninteractive))
                   (string-search name (erc-scenarios-common--nick-at-point
                                        (point)))))))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "changeme"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "foonet"))
      (funcall expect 10 "User modes for tester")
      (erc-cmd-JOIN "#chan")
      (erc-cmd-JOIN "#foo")
      (erc-cmd-JOIN "#bar")
      (erc-cmd-JOIN "#baz")
      (erc-cmd-JOIN "#spam"))

    (ert-info ("Full names not yet known")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
        (funcall expect 5 "chan_foo")
        (should-not (funcall gecosp "Chan Fool")))

      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#foo"))
        (funcall expect 5 "foo_foo")
        (should-not (funcall gecosp "Foo Fool")))

      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#bar"))
        (funcall expect 5 "bar_foo")
        (should-not (funcall gecosp "Bar Fool")))

      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#baz"))
        (funcall expect 5 "baz_foo")
        (should-not (funcall gecosp "Baz Fool"))))

    (ert-info ("Buffer #spam populated")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#spam"))
        (funcall expect 5 "<joe> mike: Excellent")))

    ;; Full names are now known.
    (dolist (chan '("chan" "foo" "bar" "baz" "spam"))
      (with-current-buffer (concat "#" chan)
        (goto-char (point-min))
        (search-forward (concat chan "_foo"))
        (should (funcall gecosp (concat (upcase-initials chan) " Fool")))))))

(ert-deftest erc-scenarios-v3-away-notify ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/away-notify")
       (erc-server-flood-penalty 0.1)
       erc-autojoin-channels-alist
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch echo-message))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "tester:changeme"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer
        (erc-d-t-wait-for 20 "server buffer ready" (get-buffer "foonet"))
      (funcall expect 5 "User modes for tester")
      (erc-cmd-JOIN "#chan"))
    (erc-d-t-wait-for 10 "buffer #chan ready" (get-buffer "#chan"))

    (ert-info ("Chan buffer #chan populated")
      (with-current-buffer "#chan"
        (funcall expect 5 "You have joined channel #chan")
        (funcall expect 5 "<alice> bob: That is, hot ice")))

    (ert-info ("Fool's account known at join time")
      ;; See comment in /v3/whox for why this id bad if not obvious
      (with-current-buffer "foonet"
        (let ((user-fool (erc-get-server-user "fool")))
          (should (erc-server-user-account user-fool))
          (should-not (erc--server-user-away-p user-fool))
          (erc-d-t-wait-for 5 "Fool sets AWAY msg"
            (equal (erc--server-user-away-p user-fool) "afk"))
          (erc-d-t-wait-for 5 "Fool clears AWAY msg"
            (not (erc--server-user-away-p user-fool))))))

    (with-current-buffer "#chan"
      (funcall expect 5 "<bob> alice: Which the false"))))

;; FIXME move this to batch-extension patch
(ert-deftest erc-scenarios-v3-cap-notify ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/cap-notify")
       (erc-server-flood-penalty 0.1)
       erc-autojoin-channels-alist
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions '(server-time batch echo-message))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "pretester"
                                :password "changeme"
                                :full-name "pretester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "Libera.Chat"))
    (erc-d-t-wait-for 10 "buffer #chan ready" (get-buffer "#chan"))

    (ert-info ("Chan buffer #chan populated")
      (with-current-buffer "#chan"
        (funcall expect 5 "You have joined channel #chan")
        (funcall expect 20 "ty for sharing")
        (should erc-v3--extended-join)
        (should erc-v3--away-notify)))
    (with-current-buffer "Libera.Chat"
      (should-not (erc-v3--caps-timer erc-v3--caps)))))

;;; erc-scenarios-v3.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

