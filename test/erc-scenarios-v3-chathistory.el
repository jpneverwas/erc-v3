;;; erc-scenarios-v3-chathistory.el --- `erc-chathistory' scenarios -*- lexical-binding: t -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

;; FIXME dedupe last/first around bookends

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-chathistory)

(defvar erc-scenarios-v3-chathistory--current-time-function nil)

(defun erc-scenarios-v3-chathistory--current-time-pair ()
  (funcall erc-scenarios-v3-chathistory--current-time-function))

(defun erc-scenarios-v3-chathistory--setup ()
  (add-function :override (local 'erc--current-time-pair-function)
                #'erc-scenarios-v3-chathistory--current-time-pair
                '((depth . 50))))

;; FIXME redo to match current expectations.
(ert-deftest erc-scenarios-v3-chathistory--baseline ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/chathistory")
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p nil)
       (erc-chathistory--last-active
        (and erc-chathistory--use-multisession-p (fboundp 'copy-multisession)
             (copy-multisession erc-chathistory--last-active)))
       (beg 0)
       (erc-stamp--tz t)
       (fake-beg (if (< emacs-major-version 29)
                     '(25910 768 880000 0) ; 2023-10-23T05:22:08.880Z
                   '(169803852888 . 100)))
       (fake-pre (if (< emacs-major-version 29)
                     '(25910 168 880000 0) ; 10 min before
                   '(169803792888 . 100)))
       (erc-scenarios-v3-chathistory--current-time-function
        (lambda () (time-add fake-beg beg)))
       (erc-mode-hook (cons #'erc-scenarios-v3-chathistory--setup
                            erc-mode-hook))
       (get-diff (lambda (fake)
                   (format-time-string "%FT%T.%3NZ"
                                       (time-add fake (cl-incf beg)) t)))
       (erc-d-tmpl-vars `((now . ,(lambda () (funcall get-diff fake-beg)))
                          (then . ,(lambda () (funcall get-diff fake-pre)))))
       (dumb-server (erc-d-run "localhost" t 'baseline))
       (port (process-contact dumb-server :service))
       (erc-modules `(v3 nicks fill-wrap scrolltobottom ,@erc-modules))
       (erc-v3-extensions (cons 'draft/chathistory erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (erc :server "127.0.0.1" :port port :nick "dummy" :full-name "dummy")
      (funcall expect 10 "User modes for dummy")
      (erc-cmd-JOIN "#chan"))

    (ert-info ("Chan buffer #chan populated")
      (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
        (funcall expect 5 "<bob> My sons' sweet blood")

        ;; Playback
        (if noninteractive
            (erc-chathistory--fetch 'BEFORE 19 nil
                                    (erc-chathistory--find-oldest-msg-time))
          (ert-simulate-keys "y19\r"
            (erc-chathistory--scroll-down-command 1)))
        (funcall expect 5 "[Mon Oct 23 2023]" (point-min))
        (funcall expect 5 ">>> RANGE-BEG 2023-10-23T05:12:46.880Z")
        (funcall expect 5 "<alice> You are a merciful general.")
        (funcall expect -0.01 "[Mon Oct 23 2023]") ; dedupe
        (funcall expect 10 "<bob> And thank you too; for society")
        (funcall expect 5 "<HistServ> dummy joined the channel")
        (funcall expect 5 "<<< RANGE-END 2023-10-23T05:12:46.880Z")
        (funcall expect -0.01 "[Mon Oct 23 2023]") ; dedupe

        ;; Live
        (funcall expect 5 ">>> RANGE-BEG 2023-10-23T05:22:08.880Z")
        (funcall expect 5 "<alice> dummy, welcome!")
        (funcall expect 5 "<bob> dummy, welcome!")
        (funcall expect 5 "<alice> Truly, Fortune's displeasure")
        (funcall expect 5 "<bob> My sons' sweet blood")
        (funcall expect 5 "<bob> alice: To grace us with")
        (funcall expect 5 "<alice> Herein I see thou lovest")
        (funcall expect 5 "<alice> bob: He hath abandoned")
        (funcall expect 5 "<bob> He'll strike, and")))))

;; FIXME redo to match current expectations.
(ert-deftest erc-scenarios-v3-chathistory--dateline ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/chathistory")
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p nil)
       (erc-chathistory--last-active
        (and erc-chathistory--use-multisession-p (fboundp 'copy-multisession)
             (copy-multisession erc-chathistory--last-active)))
       (beg 0)
       (erc-stamp--tz (- (* 7 60 60)))
       (fake-beg (if (< emacs-major-version 29)
                     '(25908 52315 445000 0)
                   '(1697959003445 . 1000))) ; 2023-10-22T07:16:43.445Z
       (get-time (lambda () (time-add fake-beg (cl-incf beg))))
       (erc-scenarios-v3-chathistory--current-time-function get-time)
       (erc-mode-hook (cons #'erc-scenarios-v3-chathistory--setup
                            erc-mode-hook))
       (erc-d-tmpl-vars
        `((now . ,(lambda ()
                    (format-time-string "%FT%T.%3NZ" (funcall get-time) t)))))
       (dumb-server (erc-d-run "localhost" t 'dateline))
       (port (process-contact dumb-server :service))
       ;; Maybe remove `nicks' or make presence random.
       (erc-modules `(v3 nicks fill-wrap scrolltobottom ,@erc-modules))
       (erc-v3-extensions (cons 'draft/chathistory erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (erc :server "127.0.0.1"
           :port port
           :nick "tester"
           :user "tester@vanilla/foonet"
           :password "changeme"
           :full-name "tester"))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))

      (ert-info ("Initial chatter")
        (funcall expect 5 "<bob> alice: All pride is willing")
        (funcall expect 10 "<alice> bob: Where should he have"))

      (ert-info ("First fetch appears above live")
        (erc-chathistory--fetch 'BEFORE 7 nil
                                (erc-chathistory--find-oldest-msg-time))
        (funcall expect 5 "[Sun Oct 22 2023]" (point-min))
        (funcall expect 5 ">>> RANGE-BEG 2023-10-22T07:12:35.000Z")
        (funcall expect 5 "<alice> bob: For I can nowhere find")
        (funcall expect 5 "<bob> Such as she says")
        (funcall expect 5 "<<< RANGE-END 2023-10-22T07:12:35.000Z"))

      (ert-info ("More live chatter")
        (funcall expect 5 "<bob> alice: All pride is willing")
        (funcall expect 5 "<bob> alice: And therefore do")
        (funcall expect 5 "<alice> bob: That I protest I simply am a maid"))

      (ert-info ("Second fetch appears atop first")
        (erc-chathistory--fetch 'BEFORE 8 nil
                                (erc-chathistory--find-oldest-msg-time))
        (funcall expect 5 "[Sat Oct 21 2023]" (point-min))
        (funcall expect 5 ">>> RANGE-BEG 2023-10-22T06:24:59.000Z")
        (funcall expect 5 "<alice> tester, welcome!")
        (funcall expect 5 "<alice> And I have been"))

      ;; FIXME remove obsolete logic and comments.  Basically, an
      ;; earlier version tracked stamps and repositioned them by
      ;; constantly deleting and inserting, which was obviously dumb.
      (ert-info ("Batch marker preserved when date stamp deleted")
        ;; We have to wait for this line to prevent skipping past the
        ;; first Oct 22 stamp.
        (funcall expect 5 "[Sun Oct 22 2023]")
        (funcall expect 5 "<bob> alice: Of Roman gentlemen")
        (funcall expect 5 ", by the senate sent. [00:12]\n<alice> bob: What")
        (funcall expect 5 "<alice> By my life, I do")
        (funcall expect 5 "<<< RANGE-END 2023-10-22T06:24:59.000Z"))

      (ert-info ("Last message of second batch trimmed away")
        (funcall expect 5 ">>> RANGE-BEG 2023-10-22T07:12:35.000Z")
        ;; Note that "bob:" is not buttonized here because batches are
        ;; inserted in a temporary environment with an empty
        ;; `erc-channel-members'.
        (funcall expect 5 "<alice> bob: For I can nowhere")
        (funcall expect 5 "find him like a man. [00:12]\n")
        ;; The final message of this section is a duplicate
        ;; of "<bob> alice: All pride...", but that's been suppressed.
        (funcall expect 5 "<bob> Such as she says")
        (funcall expect 5 "<<< RANGE-END 2023-10-22T07:12:35.000Z"))

      ;; Back to live
      (funcall expect 5 ">>> RANGE-BEG 2023-10-22T07:16:45.815Z")
      (funcall expect 5 "<bob> alice: All pride is willing")
      (funcall expect 5 "<alice> To die upon the hand")
      (funcall expect 5 "<bob> alice: And therefore do")
      (funcall expect 5 "<bob> alice: Very well! go to!")
      (funcall expect 5 "<alice> And bids thee christen"))))

;; TODO add catchup-msgid variant
(ert-deftest erc-scenarios-v3-chathistory--catchup-fallback ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/chathistory")
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p nil)
       (erc-chathistory--last-active
        (and erc-chathistory--use-multisession-p (fboundp 'copy-multisession)
             (copy-multisession erc-chathistory--last-active)))
       (erc-stamp--tz (- (* 7 60 60)))
       (dumb-server (erc-d-run "localhost" t 'catchup-fallback))
       (port (process-contact dumb-server :service))
       (erc-modules `(v3 nicks fill-wrap scrolltobottom ,@erc-modules))
       (erc-autojoin-channels-alist '((foonet "#chan")))
       (erc-v3-extensions `( draft/chathistory draft/event-playback
                             ,@erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (erc :server "127.0.0.1"
           :port port
           :nick "tester"
           :user "tester"
           :full-name "tester"))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))

      (ert-info ("Initial chatter")
        (funcall expect 5 "tester, welcome")
        (funcall expect 5 "<bob> Here comes Signior Claudio")
        (funcall expect 5 "<bob> alice: If it be")
        (funcall expect 5 "<alice> And must for aye")
        (funcall expect 5 "<alice> bob: That we may call")
        (erc-scenarios-common-say "/part"))

      (ert-info ("Rejoin and wait for last live message")
        (funcall expect 5 ">>> RANGE-BEG 2024-04-29T23:00:02.884Z" (point-min))
        (funcall expect 5 "you have left channel #chan")
        (funcall expect 5 "<<< RANGE-END 2024-04-29T23:00:02.884Z")
        (erc-scenarios-common-say "/join #chan")
        (funcall expect 5 "tester, welcome")
        (funcall expect 5 "<bob> alice: With dignities becoming")
        (funcall expect 5 "<bob> That all eyes saw"))

      (ert-info ("Playback")
        (funcall expect 10 ">>> RANGE-BEG 2024-04-29T23:00:26.795Z" (point-min))
        (funcall expect 10 "<bob> The ship is under sail")
        (funcall expect 10 "<bob> King Lear hath lost")
        (funcall expect 10 "<bob> alice: What needs this iterance")
        (funcall expect -0.1 "<bob> alice: What needs this iterance")
        (funcall expect 10 "<alice> bob: In their assign'd and native")
        (funcall expect 10 "<<< RANGE-END 2024-04-29T23:00:26.795Z")))))

(ert-deftest erc-scenarios-v3-chathistory--reconnect ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/chathistory")
       (erc-chathistory--last-active
        (and erc-chathistory--use-multisession-p (fboundp 'copy-multisession)
             (copy-multisession erc-chathistory--last-active)))
       (erc-server-flood-penalty 0.1)
       (erc-span--use-lisp-threads-p nil)
       (beg 0)
       (erc-stamp--tz (- (* 7 60 60)))
       (fake-beg (erc-compat--iso8601-to-time "2024-05-01T04:20:11.055Z" t))
       (erc-scenarios-v3-chathistory--current-time-function
        (lambda () (time-add fake-beg (cl-incf beg))))
       (erc-mode-hook (cons #'erc-scenarios-v3-chathistory--setup
                            erc-mode-hook))
       (dumb-server (erc-d-run "localhost" t 'reconnect-a 'reconnect-b))
       (port (process-contact dumb-server :service))
       (erc-modules `(v3 nicks fill-wrap scrolltobottom ,@erc-modules))
       (erc-autojoin-channels-alist '((foonet "#chan")))
       (erc-v3-extensions `( draft/chathistory draft/event-playback
                             ,@erc-v3-extensions))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect initial session")
      (erc :server "127.0.0.1"
           :port port
           :nick "tester"
           :user "tester@vanilla/foonet"
           :password "changeme"
           :full-name "tester"))

    ;; Buffer #chan contains normal chatter.
    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
      (funcall expect 10 "<alice> bob: Thou canst not come"))

    ;; Query buffer dummy contains playback message.
    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "dummy"))
      (funcall expect 10 "Last activity")
      ;; Simulate client scrolling up.
      (erc-chathistory--fetch 'BEFORE 50 nil
                              (erc-chathistory--find-oldest-msg-time))
      (funcall expect 10 "<dummy> f" (point-min)))

    ;; Reconnect.
    (with-current-buffer "foonet"
      (erc-scenarios-common-say "/quit")
      (funcall expect 5 "finished")
      ;; Kludge: spoof "last-active" entry to mock a historical
      ;; `last-received-time' (already copied).
      (erc-chathistory--set-last-active fake-beg)
      (erc-scenarios-common-say "/reconnect"))

    ;; Make mocked target= params more realistic.
    (setq fake-beg (erc-compat--iso8601-to-time "2024-05-01T04:21:52.107Z" t)
          beg 0)

    (with-current-buffer "#chan"
      (funcall expect 10 "<bob> alice: As I hate hell" (point-min))
      (funcall expect 10 "<alice> For that which thou")
      ;; No dupes.
      (funcall expect -0.05 "<alice> For that which thou")
      (funcall expect 10 "<alice> bob: No talk of Timon")
      (funcall expect -0.05 "<alice> bob: No talk of Timon")
      (funcall expect 10 "<bob> alice: Your nose smells")
      (funcall expect -0.05 "<bob> alice: Your nose smells"))

    (with-current-buffer "dummy"
      (funcall expect 5 "RANGE-BEG")
      (funcall expect 5 "<dummy> hola")
      (funcall expect 5 "RANGE-END"))

    ;; FIXME replace this with something less leaky and asserting the
    ;; effect.
    (with-current-buffer "foonet"
      (should erc-span--last-received-time))))

;;; erc-scenarios-v3-chathistory.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

