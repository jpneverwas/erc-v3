;;; erc-fugedit-tests.el --- Tests for erc-fugedit.  -*- lexical-binding:t -*-

;; Copyright (C) 2020-2022 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

(require 'erc-fugedit)

(ert-deftest erc-fugedit--find-edit-distance ()
  (should (equal (erc-fugedit--find-edit-distance "sunday" "saturday" t)
                 [[0 1 2 3 4 5 6 7 8]
                  [1 0 1 2 3 4 5 6 7]
                  [2 1 1 2 2 3 4 5 6]
                  [3 2 2 2 3 3 4 5 6]
                  [4 3 3 3 3 4 3 4 5]
                  [5 4 3 4 4 4 4 3 4]
                  [6 5 4 4 5 5 5 4 3]]))

  (should (equal (erc-fugedit--find-edit-distance "sitting" "kitten" t)
                 [[0 1 2 3 4 5 6]
                  [1 1 2 3 4 5 6]
                  [2 2 1 2 3 4 5]
                  [3 3 2 1 2 3 4]
                  [4 4 3 2 1 2 3]
                  [5 5 4 3 2 2 3]
                  [6 6 5 4 3 3 2]
                  [7 7 6 5 4 4 3]])))

(ert-deftest erc-fugedit--wd ()
  (should (equal (erc-fugedit--wd "abc" "abc")
                 '(#s(erc-fugedit--wd-comp "abc" :txt))))

  (should (equal (erc-fugedit--wd "abc" "123")
                 '(#s(erc-fugedit--wd-comp "123" :add)
                   #s(erc-fugedit--wd-comp "abc" :rem))))

  (should (equal (erc-fugedit--wd "abc" "abc 123")
                 '(#s(erc-fugedit--wd-comp "abc" :txt)
                   #s(erc-fugedit--wd-comp " 123" :add))))

  (should (equal (erc-fugedit--wd "abc 456" "abc 123")
                 '(#s(erc-fugedit--wd-comp "abc " :txt)
                   #s(erc-fugedit--wd-comp "123" :add)
                   #s(erc-fugedit--wd-comp "456" :rem))))

  (let ((o "the network shows up the page doesn't load.")
        (n "the network it doesn't load the page instead showing me a blank"))
    (should (equal (erc-fugedit--wd o n)
                   '(#s(erc-fugedit--wd-comp "the network " :txt)
                     #s(erc-fugedit--wd-comp "it" :add)
                     #s(erc-fugedit--wd-comp "shows" :rem)
                     #s(erc-fugedit--wd-comp " " :txt)
                     #s(erc-fugedit--wd-comp "doesn't" :add)
                     #s(erc-fugedit--wd-comp "up" :rem)
                     #s(erc-fugedit--wd-comp " " :txt)
                     #s(erc-fugedit--wd-comp "load " :add)
                     #s(erc-fugedit--wd-comp "the page " :txt)
                     #s(erc-fugedit--wd-comp "instead" :add)
                     #s(erc-fugedit--wd-comp "doesn't" :rem)
                     #s(erc-fugedit--wd-comp " " :txt)
                     #s(erc-fugedit--wd-comp "showing me a blank" :add)
                     #s(erc-fugedit--wd-comp "load." :rem))))))

(ert-deftest erc-fugedit--assemble-diff ()
  (let ((erc-fugedit-edit-style 'diff-color))

    (with-current-buffer (get-buffer-create "*assemble-diff*")
      (unless noninteractive
        (font-lock-mode +1))

      (insert (erc-fugedit--assemble-diff
               '(#s(erc-fugedit--wd-comp "Frisbee, " :txt)
                 #s(erc-fugedit--wd-comp "you are a " :rem)
                 #s(erc-fugedit--wd-comp "Legend!" :txt))))
      (should (equal-including-properties
               (buffer-string)
               #("Frisbee, you are a Legend!"
                 9 19 (erc--face-temp erc-fugedit-diff-removed-face
                                      rear-nonsticky t))))

      (delete-region (point-min) (point-max))

      (insert (erc-fugedit--assemble-diff
               '(#s(erc-fugedit--wd-comp "Frisbee, " :txt)
                 #s(erc-fugedit--wd-comp "yas " :add)
                 #s(erc-fugedit--wd-comp "you are a " :rem)
                 #s(erc-fugedit--wd-comp "Legend!" :txt))))
      (should (equal-including-properties
               (buffer-string)
               #("Frisbee, you are a yas Legend!"
                 9 19 (erc--face-temp erc-fugedit-diff-removed-face
                                      rear-nonsticky t)
                 19 23 (erc--face-temp erc-fugedit-diff-added-face
                                       rear-nonsticky t))))

      (when noninteractive
        (kill-buffer)))))

(ert-deftest erc-fugedit--check-edits--present-nomatch ()
  (let* ((now (current-time))
         (erc-fugedit--edits
          (list (list "alice"
                      (cons (time-add now 60) "avoid triggering high")
                      (cons (time-subtract now 1) "is non-normative"))
                (list "bob"
                      (cons (time-add now 60) "abc def 123 456 789")
                      (cons (time-add now 120) "xyz hello ok 123"))
                (list "someone"
                      (cons (time-add now 60) "bot message fallback")
                      (cons (time-subtract now 1) "I'm not sending a")))))
    (should-not
     (erc-fugedit--check-edits "bob" now "also not meant to be used"))
    (let ((want `(("alice"
                   (,(time-add now 60) . "avoid triggering high"))
                  ("bob"
                   (,(time-add now 300) . "also not meant to be used")
                   (,(time-add now 60) . "abc def 123 456 789"))
                  ("someone"
                   (,(time-add now 60) . "bot message fallback")))))

      (should (equal erc-fugedit--edits want)))))

;; Duplicates are currently accepted, which may not be ideal.
(ert-deftest erc-fugedit--check-edits--present-dupe ()
  (let* ((now (current-time))
         (erc-fugedit--edits
          (list (list "alice"
                      (cons (time-add now 60) "avoid triggering high")
                      (cons (time-subtract now 1) "is non-normative"))
                (list "bob"
                      (cons (time-add now 60) "abc def 123 456 789")
                      (cons (time-add now 120) "xyz hello ok 123"))
                (list "someone"
                      (cons (time-add now 60) "bot message fallback")
                      (cons (time-subtract now 1) "I'm not sending a")))))
    (should
     (erc-fugedit--check-edits "bob" now "abc def 123 456 789"))
    (let ((want `(("alice"
                   (,(time-add now 60) . "avoid triggering high"))
                  ("bob"
                   (,(time-add now 300) . "abc def 123 456 789")
                   (,(time-add now 60) . "abc def 123 456 789"))
                  ("someone"
                   (,(time-add now 60) . "bot message fallback")))))

      (should (equal erc-fugedit--edits want)))))

(ert-deftest erc-fugedit--check-edits--absent-nomatch ()
  (let* ((now (current-time))
         (erc-fugedit--edits
          `(("alice"
             (,(time-add now 60) . "avoid triggering high")
             (,(time-add now 60) . "is non-normative"))
            ("someone"
             (,(time-add now 60) . "bot message fallback")
             (,(time-add now 60) . "I'm not sending a")))))
    (should-not
     (erc-fugedit--check-edits "bob" now "also not meant to be used"))
    (let ((want `(("bob"
                   (,(time-add now 300) . "also not meant to be used"))
                  ("alice"
                   (,(time-add now 60) . "avoid triggering high")
                   (,(time-add now 60) . "is non-normative"))
                  ("someone"
                   (,(time-add now 60) . "bot message fallback")
                   (,(time-add now 60) . "I'm not sending a")))))
      (should (equal erc-fugedit--edits want)))))

(ert-deftest erc-fugedit--check-edits--present-match ()
  (let* ((now (current-time))
         (erc-fugedit--edits
          (list (list "alice"
                      (cons (time-add now 60) "avoid triggering high")
                      (cons (time-subtract now 1) "is non-normative"))
                (list "bob"
                      (cons (time-add now 60) "abc def 123 456 789")
                      (cons (time-add now 120) "also not meant to be")))))
    (should
     (erc-fugedit--check-edits "bob" now "also not meant to be used"))
    (let ((want `(("alice"
                   (,(time-add now 60) . "avoid triggering high"))
                  ("bob"
                   (,(time-add now 300) . "also not meant to be used")
                   (,(time-add now 60) . "abc def 123 456 789")))))

      (should (equal erc-fugedit--edits want)))))

(ert-deftest erc-fugedit--check-edits--skip ()
  (let* ((now (current-time))
         (erc-fugedit--edits
          (list (list "alice"
                      (cons (time-add now 60) "avoid triggering high")
                      (cons (time-subtract now 1) "is non-normative")))))
    (should-not
     (erc-fugedit--check-edits "bob" now "https://example.com/foo.txt"))
    (let ((want `(("alice"
                   (,(time-add now 60) . "avoid triggering high")))))

      (should (equal erc-fugedit--edits want)))))

;;; erc-fugedit-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

