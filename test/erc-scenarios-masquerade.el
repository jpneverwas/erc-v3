;;; erc-scenarios-masquerade.el --- masquerade scenarios -*- lexical-binding: t -*-

;; Copyright (C) 2022 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-scenarios-common)
(require 'erc-masquerade)
(require 'erc-fugedit)

(defvar erc-v3-extensions)
(declare-function erc-batch--es "erc-batch" nil)

(defun erc-scenarios-common--tps (timeout text)
  (save-restriction
    (widen)
    (let ((from (point)))
      (erc-d-t-wait-for timeout (format "string: %s" text)
        (goto-char from)
        (text-property-search-forward 'display text
                                      (lambda (v p)
                                        (string-prefix-p v p)))))))

(defun erc-scenarios-masqerade--assert-fill-button-ordering ()
  ;; Ensure that our custom fill function can see all face
  ;; modifications; otherwise its calculations will be off.
  (ert-info ("Button follows fill in modification hooks")
    (should (thread-last 'erc-insert-modify-hook
                         (default-value)
                         (memq 'erc-button-add-buttons)
                         (memq 'erc-fill)))))

(defun erc-scenarios-masquerade--batch (after-foo cleanup)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/masquerade")
       (erc-d-linger-secs 0.5)
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (dumb-server (erc-d-run "localhost" t 'batch))
       (port (process-contact dumb-server :service))
       (erc-modules `(nicks masquerade v3 ,@erc-modules))
       (erc-v3-extensions '(server-time batch))
       (expect (erc-d-t-make-expecter))
       (erc-scenarios-common-extra-teardown cleanup))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "password123"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))
    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

    (erc-scenarios-masqerade--assert-fill-button-ordering)

    (ert-info ("Notices received")
      (with-current-buffer "ExampleOrg"
        (funcall expect 10 "Setting your virtual host")))

    (erc-d-t-wait-for 10 "buffer #foo ready" (get-buffer "#foo"))

    (ert-info ("Chan buffer #foo populated")
      (with-current-buffer "#foo"
        (funcall expect 20 ">>> RANGE-BEG 2021-01-01T04:35:31.906Z")
        (funcall expect 5 "<focused-ne")
        (funcall expect 20 "#frllklw-klwjlsrs")
        (funcall expect 10 "<<< RANGE-END 2021-01-01T04:35:31.906Z")
        (funcall expect 10 "You have joined channel #foo")
        (funcall expect 5 "Users on #foo (9)")
        (funcall expect 5 "#foo modes:")
        (funcall expect 5 "#foo was")
        (funcall expect 5 "<gallant")
        (funcall expect 5 "According")
        (funcall expect 5 "<focused-ne")
        (funcall expect 5 "Or never")
        (funcall expect 5 erc-prompt)
        (funcall after-foo)))

    (with-current-buffer "ExampleOrg"
      (should-not (erc-batch--es)))))

(defvar erc-fill-function)
(declare-function erc-fill-static "erc-fill" nil)
(declare-function erc-fill-wrap "erc-fill" nil)

(defun erc-scenarios-masquerade--assert-completion ()
  (ert-info ("Completion at point works")
    (save-excursion
      (goto-char erc-input-marker)
      (insert "gall")
      (call-interactively #'erc-tab)
      (should (looking-back "gallant: "))
      (goto-char erc-input-marker)
      (delete-region erc-input-marker (point-max)))))

(ert-deftest erc-scenarios-masquerade-batch--normal ()
  :tags '(:expensive-test)
  (let ((orig (erc-scenarios-common--temporary-setq
               '((erc-masquerade-bots
                  ((ExampleOrg
                    ("#test" "spammer" "" "")
                    ("#foo" "botman" "<\\(.+?\\)> " ""))))
                 (erc-timestamp-use-align-to nil)))))
    (erc-scenarios-masquerade--batch
     (lambda ()
       (ert-info ("Completion at point works for local names")
         (save-excursion
           (goto-char erc-input-marker)
           (insert "some")
           (call-interactively #'erc-tab)
           (should (looking-back "someone: "))
           (goto-char erc-input-marker)
           (delete-region erc-input-marker (point-max))
           ;; SHA<tab> won't work because they were only present
           ;; during playback.
           (insert "TR")
           (call-interactively #'erc-tab)
           (should (looking-back "TRUST:dvdovt.org: "))
           (delete-region erc-input-marker (point-max)))))
     (lambda () (erc-scenarios-common--temporary-setq orig)))
    (should-not erc-masquerade-bots)
    (should (eq (display-graphic-p) erc-timestamp-use-align-to))))

(ert-deftest erc-scenarios-masquerade-batch--custom-parser ()
  :tags '(:expensive-test)
  (require 'erc-fill)
  (let ((orig (erc-scenarios-common--temporary-setq
               '((erc-masquerade-bots
                  ((ExampleOrg
                    ("fake" . ignore) ; legacy :type of (nick . func)
                    ("#foo" "botman"
                     erc-masquerade--preform-legacy-fallback))))
                 (erc-interpret-mirc-color t)
                 (erc-fill-wrap-merge-indicator
                  (pre #xb7 erc-fill-wrap-merge-indicator-face))
                 (erc-fill-function erc-fill-wrap)))))
    (erc-scenarios-masquerade--batch
     (lambda ()
       (erc-scenarios-masquerade--assert-completion)
       (goto-char (point-min))
       (should-not (search-forward "<botman>" nil t))
       (should (search-forward "<focused-ne>" nil t))
       (goto-char (1+ (match-beginning 0)))
       (should (string= (get-text-property (pos-bol) 'erc--masquerade)
                        "focused-ne:telegram"))
       (should (search-forward "<gallant>" nil t))
       (goto-char (1+ (match-beginning 0)))
       (should (string= (get-text-property (pos-bol) 'erc--masquerade)
                        "gallant:xsxpvtson.net"))
       (should (string= (get-text-property (pos-bol) 'erc--spkr)
                        "botman gallant:xsxpvtson.net")))
     (lambda () (erc-scenarios-common--temporary-setq orig)))))

(defun erc-scenarios-masquerade--edit (edit-style test)
  (require 'erc-fill)
  (should (equal erc-fugedit-edit-style "(Edit)"))
  (erc-scenarios-common-with-cleanup
      ((orig (erc-scenarios-common--temporary-setq
              `((erc-fugedit-edit-style ,edit-style)
                (erc-masquerade-bots
                 ((ExampleOrg
                   ("#foo" "botman"
                    ,(rx bot "<" (group (+ nonl)) ":" (group (+ nonl)) "> ")
                    "\\2 "))))
                (erc-timestamp-use-align-to nil)
                (erc-fill-function erc-fill-wrap))))
       (erc-scenarios-common-dialog "v3/masquerade")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        `((now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))))
       (erc-scenarios-common-extra-teardown
        (lambda () (erc-scenarios-common--temporary-setq orig)))
       (dumb-server (erc-d-run "localhost" t 'edit))
       (port (process-contact dumb-server :service))
       (erc-modules `(masquerade fugedit v3 ,@erc-modules))
       (erc-v3-extensions '(server-time batch))
       (expect (erc-d-t-make-expecter)))

    ;; Note: to test nicks integration, reverse this list
    (when (zerop (random 2))
      (setq erc-modules (reverse erc-modules)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "password123"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 10 "server buffer ready" (get-buffer "ExampleOrg"))

    (erc-scenarios-masqerade--assert-fill-button-ordering)

    (ert-info ("Notices received")
      (with-current-buffer "ExampleOrg"
        (funcall expect 10 "Setting your virtual host")))

    (erc-d-t-wait-for 10 "buffer #foo ready" (get-buffer "#foo"))

    (ert-info ("Chan buffer #foo populated")
      (with-current-buffer "#foo"
        (funcall expect 10 ">>> RANGE-BEG 2021-01-01T04:35:31.906Z")
        (funcall test expect)
        (funcall expect 10 "Or never after")
        (erc-scenarios-masquerade--assert-completion)))))

(ert-deftest erc-scenarios-masquerade-batch--edit-tag ()
  :tags '(:unstable :expensive-test)
  (erc-scenarios-masquerade--edit
   "(Edit)"
   (lambda (expect)
     (funcall expect 10 '(: "(Edit)" (+ anychar) "abcdef."))
     (funcall expect 10 "<SHA> kpvxmd.org https://example.org/diamonds")
     (funcall expect 10 '(: "(Edit)" (+ anychar) "hang")))))

(ert-deftest erc-scenarios-masquerade-batch--edit-diff-color ()
  :tags '(:unstable :expensive-test)
  (erc-scenarios-masquerade--edit
   'diff-color
   (lambda (expect)
     (funcall expect 10 '(: "SellSee" (+ anychar) "abcdeabcdef."))
     (funcall expect 10 "<SHA> kpvxmd.org https://example.org/diamonds")
     (funcall expect 10 " greeting.GREETING. Now!"))))

(ert-deftest erc-scenarios-masquerade-batch--edit-diff-plain ()
  :tags '(:unstable :expensive-test)
  (erc-scenarios-masquerade--edit
   'diff-plain
   (lambda (expect)
     (funcall expect 10 '(: "[-Sell-]{+See+}" (+ anychar)
                            "[-abcde-]{+abcdef+}."))
     (funcall expect 10 "<SHA> kpvxmd.org https://example.org/diamonds")
     (funcall expect 10 " [-greeting.-]{+GREETING. Now!+}"))))

;;; erc-scenarios-masquerade.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

