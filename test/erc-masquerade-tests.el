;;; erc-masquerade-tests.el --- Tests for erc-masquerade.  -*- lexical-binding:t -*-

;; Copyright (C) 2020-2022 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:
(require 'ert-x)
(require 'erc-masquerade)


(ert-deftest erc-masquerade--canonicalize-prefix-mattermost ()
  (should (string= (erc-masquerade--canonicalize-prefix-mattermost
                    "<\C-]b\u200bob\C-]> ")
                   "bob"))
  (should (string= (erc-masquerade--canonicalize-prefix-mattermost
                    "<b\u200bob> ")
                   "bob"))
  (should (string= (erc-masquerade--canonicalize-prefix-mattermost
                    "[t\u200belegram] <b\u200bob> ")
                   "bob:telegram"))
  (should (string= (erc-masquerade--canonicalize-prefix-mattermost
                    "[telegram-] <bob> ")
                   "bob:telegram")))

(ert-deftest erc-masquerade--match-nick-default ()
  ;; Baseline.
  (should (equal (erc-masquerade--match-nick-default
                  "<ABC> 123"
                  (rx bot "<" (group (+ nonl)) "> ")
                  "")
                 '("ABC" "ABC" "" "123" nil)))

  ;; Replacement.
  (should (equal (erc-masquerade--match-nick-default
                  "<ABC:gnu.org> 123"
                  (rx bot "<" (group (+ nonl)) ":" (group (+ nonl)) "> ")
                  "\\2 ")
                 '("ABC:gnu.org" "ABC" "gnu.org " "123" nil))))

(defun ert-masquerade-tests--perform-in-chan (setup test)
  (let* ((pat (rx bot "<" (group (+ nonl)) "> "))
         (erc-masquerade-bots `((foonet ("#tes." "spammer_?" ,pat "")
                                        ("#chan" "frisbeeosbridge" ,pat ""))))
         erc-kill-channel-hook erc-kill-server-hook erc-kill-buffer-hook)

    (with-current-buffer (get-buffer-create "foonet")
      (erc-mode)
      (setq erc-server-process (start-process "true"
                                              (current-buffer) "sleep" "1")
            erc-server-current-nick "tester"
            erc--isupport-params (make-hash-table)
            erc-server-announced-name "foo.gnu.chat"
            erc-network 'foonet
            erc-networks--id (erc-networks--id-create nil)
            erc-server-users (make-hash-table :test #'equal))
      (puthash 'CHANTYPES '("&#") erc--isupport-params)
      (set-process-query-on-exit-flag erc-server-process nil))

    (dolist (name '("#test" "#chan"))
      (with-current-buffer (get-buffer-create name)
        (erc-mode)
        (erc--initialize-markers (point) nil)
        (setq erc-server-process (buffer-local-value 'erc-server-process
                                                     (get-buffer "foonet"))
              erc-default-recipients (list name)
              erc--target (erc--target-from-string name)
              erc-network 'foonet
              erc-networks--id (buffer-local-value 'erc-networks--id
                                                   (get-buffer "foonet"))
              erc-channel-users (make-hash-table :test #'equal))
        (erc-fill-mode +1)
        (funcall setup)
        (erc-masquerade-mode +1)))

    (funcall test)

    (when noninteractive
      (kill-buffer "foonet")
      (kill-buffer "#test")
      (kill-buffer "#chan"))))

(ert-deftest erc-masquerade--adorn ()
  (ert-masquerade-tests--perform-in-chan
   #'ignore
   (lambda ()

     (with-current-buffer "foonet"
       (erc-parse-server-response
        erc-server-process
        (concat "@time=2022-07-18T09:19:34.604Z "
                ":spammer_!~spammer_@matrix.spammer.org "
                "PRIVMSG #test :<Bob:discord> "
                "Blah..."))
       (erc-parse-server-response
        erc-server-process
        (concat "@time=2022-07-18T09:19:34.604Z "
                ":frisbeeosbridge!~frisbeeo@matrix.frisbeeos.org "
                "PRIVMSG #chan :<Alice:telegram> "
                "Receiving clients SHOULD check for the presence of...")))

     (with-current-buffer "#test"
       (goto-char (point-min))
       (should (string= erc-masquerade--botnick-regexp "\\`spammer_?\\'"))
       (should (search-forward "<Bob:discord> " nil t))
       (should (string= (get-text-property (pos-bol) 'erc--masquerade)
                        "Bob:discord"))
       (should (looking-at-p "Blah")))

     (with-current-buffer "#chan"
       (goto-char (point-min))
       (should (search-forward "<Alice:telegram> " nil t))
       (should (string= (get-text-property (pos-bol) 'erc--masquerade)
                        "Alice:telegram"))
       (should (looking-at-p "Receiving"))))))

(defvar erc-fill-function)
(defvar erc-fill-static-center)
(declare-function erc-fill-static "erc-fill" nil)

(ert-deftest erc-masquerade--adorn/fill-static ()
  (ert-masquerade-tests--perform-in-chan

   (lambda ()
     (when (string= (buffer-name) "#chan")
       (setq-local erc-fill-function #'erc-fill-static)
       (setq-local erc-fill-static-center 22)))

   (lambda ()
     (with-current-buffer "foonet"
       (erc-parse-server-response
        erc-server-process
        (concat "@time=2022-07-18T09:19:34.604Z "
                ":frisbeeosbridge!~frisbeeo@matrix.frisbeeos.org "
                "PRIVMSG #chan :<Alice:telegram> "
                "Receiving clients SHOULD check for the presence of..."))
       (erc-parse-server-response
        erc-server-process
        (concat "@time=2022-07-18T09:19:35.604Z "
                ":frisbeeosbridge!~frisbeeo@matrix.frisbeeos.org "
                "PRIVMSG #chan :<BillyBob:matrix.org> "
                "Consider formatting display names differently to nicknames")))

     (with-current-buffer "#chan"
       (goto-char (point-min))
       (should (search-forward "<Alice:telegram> " nil t))
       (goto-char (match-beginning 0))
       (should (string= (get-text-property (pos-bol) 'erc--masquerade)
                        "Alice:telegram"))
       (should (= (- (point) (line-beginning-position)) 5))
       (should
        (string-prefix-p
         "     <Alice:telegram> Receiving clients SHOULD"
         (buffer-substring-no-properties (line-beginning-position)
                                         (line-end-position))))
       (goto-char (line-end-position))

       (should (search-forward "<BillyBob:matrix.org> " nil t))
       (goto-char (match-beginning 0))
       (should (string= (get-text-property (pos-bol) 'erc--masquerade)
                        "BillyBob:matrix.org"))
       (should (= (point) (line-beginning-position)))))))

;;; erc-masquerade-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

