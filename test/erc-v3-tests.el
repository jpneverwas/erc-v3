;;; erc-v3-tests.el --- Tests for erc-v3.  -*- lexical-binding:t -*-

;; Copyright (C) 2020-2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-tests-common)))

(require 'erc-v3)

;; These use vendor prefixes to avoid polluting the symbol properties
;; of plain `a', `b', etc.

(erc-v3--define-capability test/a)
(erc-v3--define-capability test/b)
(erc-v3--define-capability test/c)
(erc-v3--define-capability test/d)
(erc-v3--define-capability test/e)
(erc-v3--define-capability test/f)

(defun erc-v3-tests--slots (slots &optional objects)
  (mapcar (lambda (o)
            (mapcar (lambda (slot)
                      (let ((v (cl-struct-slot-value (type-of o) slot o)))
                        (intern (symbol-name (if (eq slot 'step) (cdr v) v)))))
                    slots))
          (or objects (erc-v3--caps-objects erc-v3--caps))))

(defvar erc-v3-tests--parser-tests
  (expand-file-name "erc-d/resources/irc-parser-tests.eld"
                    (ert-resource-directory)))

(ert-deftest erc-v3--step ()
  (should (cl-typep erc-v3--step-SEEN 'erc-v3--step))
  (should (cl-typep erc-v3--step-OFFERED 'erc-v3--step))
  (should (cl-typep erc-v3--step-CONSIDERED 'erc-v3--step))
  (should (cl-typep erc-v3--step-REQUESTED 'erc-v3--step))
  (should (cl-typep erc-v3--step-ANSWERED 'erc-v3--step))
  (should (cl-typep erc-v3--step-RESOLVED 'erc-v3--step))
  (should-not (cl-typep 'seen 'erc-v3--step))
  (should-not (cl-typep -1 'erc-v3--step))
  (should-not (cl-typep 6 'erc-v3--step))
  (should-not (cl-typep 42 'erc-v3--step)))

(ert-deftest erc-v3--deps-plumb ()
  (let* ((a (erc-v3--test/a))
         (b (erc-v3--test/b))
         (c (erc-v3--test/c :depends '(test/b)))
         (d (erc-v3--test/d :depends '(test/b test/c)))
         (objects (list a b c d)))
    (should (equal (erc-v3--deps-plumb objects a) nil))
    (should (equal (erc-v3--deps-plumb objects b) nil))
    (should (equal (erc-v3--deps-plumb objects c) (list b)))
    (should (equal (erc-v3--deps-plumb objects d) (list c b))))

  (let* ((a (erc-v3--test/a :depends '(test/e test/f)))
         (b (erc-v3--test/b :depends '(test/e)))
         (c (erc-v3--test/c :depends '(test/f)))
         (d (erc-v3--test/d :depends '(test/c)))
         (e (erc-v3--test/e))
         (f (erc-v3--test/f))
         (objects (list a b c d e f)))
    (should (equal (erc-v3--deps-plumb objects a) (list f e)))
    (should (equal (erc-v3--deps-plumb objects b) (list e)))
    (should (equal (erc-v3--deps-plumb objects c) (list f)))
    (should (equal (erc-v3--deps-plumb objects d) (list f c)))
    (should (equal (erc-v3--deps-plumb objects e) nil))
    (should (equal (erc-v3--deps-plumb objects f) nil))))

(ert-deftest erc-v3--deps-gather-dependents ()
  (let* ((a (erc-v3--test/a))
         (b (erc-v3--test/b))
         (c (erc-v3--test/c :depends '(test/b)))
         (d (erc-v3--test/d :depends '(test/b test/c)))
         (objects (list a b c d)))
    (should (equal (erc-v3--deps-gather-dependents objects a) nil))
    (should (equal (erc-v3--deps-gather-dependents objects b) (list d c)))
    (should (equal (erc-v3--deps-gather-dependents objects c) (list d)))
    (should (equal (erc-v3--deps-gather-dependents objects d) nil)))

  (let* ((a (erc-v3--test/a :depends '(test/e test/f)))
         (b (erc-v3--test/b :depends '(test/e)))
         (c (erc-v3--test/c :depends '(test/f)))
         (d (erc-v3--test/d :depends '(test/c)))
         (e (erc-v3--test/e))
         (f (erc-v3--test/f))
         (objects (list a b c d e f)))
    (should (equal (erc-v3--deps-gather-dependents objects a) nil))
    (should (equal (erc-v3--deps-gather-dependents objects b) nil))
    (should (equal (erc-v3--deps-gather-dependents objects c) (list d)))
    (should (equal (erc-v3--deps-gather-dependents objects d) nil))
    (should (equal (erc-v3--deps-gather-dependents objects e) (list b a)))
    (should (equal (erc-v3--deps-gather-dependents objects f) (list d c a)))))

(ert-deftest erc-v3--deps-build-dag ()
  (let ((objects (list (erc-v3--test/a)
                       (erc-v3--test/b)
                       (erc-v3--test/c :depends '(test/b))
                       (erc-v3--test/d :depends '(test/b test/c)))))
    (should (equal (erc-v3--deps-build-dag objects)
                   '((test/c test/d)
                     (test/b test/d test/c))))))

(ert-deftest erc-v3--deps-tsort ()
  (let* ((a (erc-v3--test/a))
         (b (erc-v3--test/b))
         (c (erc-v3--test/c :depends '(test/b)))
         (d (erc-v3--test/d :depends '(test/b test/c)))
         (objects (list a b c d))
         (graph (erc-v3--deps-build-dag objects)))
    (should (equal (erc-v3--deps-tsort objects graph) (list b c d a))))

  (let* ((a (erc-v3--test/a :depends '(test/e test/f)))
         (b (erc-v3--test/b :depends '(test/e)))
         (c (erc-v3--test/c :depends '(test/f)))
         (d (erc-v3--test/d :depends '(test/c)))
         (e (erc-v3--test/e))
         (f (erc-v3--test/f))
         (objects (list a b c d e f))
         (graph (erc-v3--deps-build-dag objects)))
    (should (equal (erc-v3--deps-tsort objects graph) (list f e c d b a)))))

;; Ask for soft dependencies, which may include unknown extensions.
(ert-deftest erc-v3--deps-build-dag/soft ()
  (let* ((a (erc-v3--test/a))
         (b (erc-v3--test/b))
         (c (erc-v3--test/c :supports '(test/? test/b)))
         (d (erc-v3--test/d :supports '(test/b) :depends '(test/c))))
    (dotimes (_ 8)
      (let* ((objects (sort (list a b c d) (lambda (_ _) (zerop (random 2)))))
             (graph (erc-v3--deps-build-dag objects 'softp)))
        (should (member (erc-v3--deps-tsort objects graph)
                        (list (list a b c d) ; item `a' can go anywhere
                              (list b a c d)
                              (list b c a d)
                              (list b c d a)))))))

  (let* ((a (erc-v3--test/a :supports '(test/?? test/?)))
         (b (erc-v3--test/b :supports '(test/? test/a)))
         (c (erc-v3--test/c :supports '(test/b tests/?? test/?)))
         (d (erc-v3--test/d :supports '(test/? test/c) :depends '(test/a)))
         (e (erc-v3--test/e :supports '(test/a test/??) :depends '(test/c))))
    (dotimes (_ 10)
      (let* ((objects (sort (list a b c d e)
                            (lambda (_ _) (zerop (random 2)))))
             (graph (erc-v3--deps-build-dag objects 'softp))
             (result (erc-v3--deps-tsort objects graph)))
        (should (member result (list (list a b c d e)
                                     (list a b c e d))))))))

(ert-deftest erc-v3--ensure-disabled ()
  (erc-mode)
  (setq erc-server-process (start-process "t" (current-buffer) "true"))
  (set-process-query-on-exit-flag erc-server-process nil)
  (letrec ((calls nil)
           ;;
           (a (erc-v3--test/a))
           (b (erc-v3--test/b :step erc-v3--step-ANSWERED
                              :disablep (lambda (o)
                                          (should (eq b o))
                                          (push 'b calls))))
           (c (erc-v3--test/c :depends '(test/b)
                              :step erc-v3--step-ANSWERED
                              :disablep (lambda (o)
                                          (should (eq c o))
                                          (push 'c calls))))
           (d (erc-v3--test/d :depends '(test/b test/c)
                              :step erc-v3--step-ANSWERED
                              :disablep (lambda (o)
                                          (should (eq d o))
                                          (push 'd calls))))
           (erc-v3--test/d t)
           (erc-v3--test/b t)
           (erc-v3--test/c t)
           (erc-v3--caps (make-erc-v3--caps :objects (list a b c d))))
    (erc-v3--ensure-disabled (list b) erc-v3--step-SEEN)
    (should-not erc-v3--test/b)
    (should-not erc-v3--test/c)
    (should-not erc-v3--test/d)
    (should (eq (erc-v3--extension-step b) erc-v3--step-SEEN))
    (should (eq (erc-v3--extension-step c) erc-v3--step-SEEN))
    (should (eq (erc-v3--extension-step d) erc-v3--step-SEEN))
    (should (equal (nreverse calls) '(d c b)))))

(ert-deftest erc-v3--ensure-enabled ()
  (erc-mode)
  (setq erc-server-process (start-process "t" (current-buffer) "true"))
  (set-process-query-on-exit-flag erc-server-process nil)
  (letrec ((calls nil)
           ;;
           (a (erc-v3--test/a))
           (b (erc-v3--test/b :step erc-v3--step-ANSWERED
                              :enablep (lambda (o)
                                         (should (eq b o))
                                         (push 'b calls))))
           (c (erc-v3--test/c :depends '(test/b)
                              :step erc-v3--step-ANSWERED
                              :enablep (lambda (o)
                                         (should (eq c o))
                                         (push 'c calls))))
           (d (erc-v3--test/d :depends '(test/b test/c)
                              :step erc-v3--step-ANSWERED
                              :enablep (lambda (o)
                                         (should (eq d o))
                                         (push 'd calls))))
           (erc-v3--test/d nil)
           (erc-v3--test/b nil)
           (erc-v3--test/c nil)
           (erc-v3--caps (make-erc-v3--caps :objects (list a b c d)))
           (reset (lambda ()
                    (setf calls nil
                          erc-v3--test/b nil
                          erc-v3--test/c nil
                          erc-v3--test/d nil
                          (erc-v3--extension-step b) erc-v3--step-ANSWERED
                          (erc-v3--extension-step c) erc-v3--step-ANSWERED
                          (erc-v3--extension-step d) erc-v3--step-ANSWERED)))
           (assert-common
            (lambda ()
              (should (eq (erc-v3--extension-step b) erc-v3--step-RESOLVED))
              (should (eq (erc-v3--extension-step c) erc-v3--step-RESOLVED))
              (should (eq (erc-v3--extension-step d) erc-v3--step-RESOLVED))
              (should erc-v3--test/b)
              (should erc-v3--test/c)
              (should erc-v3--test/d))))

    (ert-info ("Basic")
      (erc-v3--ensure-enabled (list d))
      (funcall assert-common)
      (should (equal (nreverse calls) '(b c d))))

    (funcall reset)
    (ert-info ("Overlap")
      (erc-v3--ensure-enabled (list c d))
      (funcall assert-common)
      (should (equal (nreverse calls) '(b c d))))

    (funcall reset)
    (ert-info ("B called even though already satisfied")
      (setf erc-v3--test/b t
            (erc-v3--extension-step b) erc-v3--step-RESOLVED)
      (erc-v3--ensure-enabled (list d))
      (funcall assert-common)
      (should (equal (nreverse calls) '(b c d))))

    (funcall reset)
    (ert-info ("B skipped when already satisfied with default `enablep'")
      (setf erc-v3--test/b t
            (erc-v3--extension-step b) erc-v3--step-RESOLVED
            (erc-v3--capability-enablep b) #'erc-v3--inactive-p)
      (erc-v3--ensure-enabled (list d))
      (funcall assert-common)
      (should (equal (nreverse calls) '(c d))))))

(ert-deftest erc-v3--define-extension ()
  (should
   (equal
    (macroexpand '(erc-v3--define-extension foo))
    '(progn

       (cl-defstruct (erc-v3--foo (:include erc-v3--extension
                                            (canon 'foo))
                                  (:constructor erc-v3--foo))
         "IRCv3 `foo' extension.")

       (put 'foo 'erc-v3--extension-class 'erc-v3--foo)
       (put 'foo 'erc-v3--extension-minor-mode 'erc-v3--foo-mode)

       (defvar-local erc-v3--foo nil
         "Local instance of `erc-v3--foo' when enabled.")

       (define-minor-mode erc-v3--foo-mode
         "Internal minor mode for `erc-v3--foo'."
         :interactive nil
         :variable
         ((buffer-local-value 'erc-v3--foo (erc-server-buffer))
          . (lambda (v) (erc-v3--sync-mode-value 'erc-v3--foo v))))))))

(ert-deftest erc-v3--define-capability ()
  (should
   (equal (macroexpand '(erc-v3--define-capability foo :depends '(bar)))
          '(progn
             (cl-defstruct (erc-v3--foo (:include erc-v3--capability
                                                  (canon 'foo)
                                                  (depends '(bar)))
                                        (:constructor erc-v3--foo))
               "IRCv3 `foo' capability.")
             (put 'foo 'erc-v3--extension-class 'erc-v3--foo)
             (put 'foo 'erc-v3--extension-minor-mode 'erc-v3--foo-mode)
             (defvar-local erc-v3--foo nil
               "Local instance of `erc-v3--foo' when enabled.")
             (define-minor-mode erc-v3--foo-mode
               "Internal minor mode for `erc-v3--foo'."
               :interactive nil
               :variable
               ((buffer-local-value 'erc-v3--foo (erc-server-buffer))
                . (lambda (v) (erc-v3--sync-mode-value 'erc-v3--foo v)))))))

  (should
   (equal (macroexpand '(erc-v3--define-capability foo
                          "Hello World."
                          :slots ((somefield 1))
                          :aliases '(bar baz)))
          '(progn
             (cl-defstruct (erc-v3--foo (:include erc-v3--capability
                                                  (canon 'foo)
                                                  (aliases '(bar baz)))
                                        (:constructor erc-v3--foo))
               "Hello World."
               (somefield 1))
             (put 'foo 'erc-v3--extension-class 'erc-v3--foo)
             (put 'bar 'erc-v3--extension-class 'erc-v3--foo)
             (put 'baz 'erc-v3--extension-class 'erc-v3--foo)
             (put 'foo 'erc-v3--extension-minor-mode 'erc-v3--foo-mode)
             (defvar-local erc-v3--foo nil
               "Local instance of `erc-v3--foo' when enabled.")
             (define-minor-mode erc-v3--foo-mode
               "Internal minor mode for `erc-v3--foo'."
               :interactive nil
               :variable
               ((buffer-local-value 'erc-v3--foo (erc-server-buffer))
                . (lambda (v) (erc-v3--sync-mode-value 'erc-v3--foo v)))))))

  (should
   (equal (macroexpand '(erc-v3--define-capability
                          (foo (:include erc-v3--cap-notify)
                               (:constructor erc-v3--my-foo))
                          "Hello World."
                          :enablep #'my-func
                          :early-init (#'ignore :read-only t)
                          :keymap my-keymap
                          "My mode."
                          (always) (ignore)))
          '(progn
             (cl-defstruct (erc-v3--foo ( :include erc-v3--cap-notify
                                          (canon 'foo)
                                          (enablep #'my-func)
                                          (early-init #'ignore :read-only t))
                                        (:constructor erc-v3--my-foo))
               "Hello World.")
             (put 'foo 'erc-v3--extension-class 'erc-v3--foo)
             (put 'foo 'erc-v3--extension-minor-mode 'erc-v3--foo-mode)
             (defvar-local erc-v3--foo nil
               "Local instance of `erc-v3--foo' when enabled.")
             (define-minor-mode erc-v3--foo-mode
               "My mode."
               :interactive nil
               :variable
               ((buffer-local-value 'erc-v3--foo (erc-server-buffer))
                . (lambda (v) (erc-v3--sync-mode-value 'erc-v3--foo v)))
               (always) (ignore))
             (setf (alist-get 'erc-v3--foo minor-mode-map-alist) my-keymap)))))

(ert-deftest erc-v3--capability ()
  (should '(fboundp make'erc-v3--cap-notify))
  (should-not erc-v3--cap-notify)

  (let ((o (erc-v3--cap-notify)))
    (should (eq 'cap-notify (erc-v3--cap-notify-canon o)))
    (should (eq erc-v3--step-SEEN (erc-v3--cap-notify-step o)))

    ;; High-level interrogating
    (should (erc-v3--account-tag-p (cl-find 'account-tag
                                            (list (erc-v3--cap-notify)
                                                  (erc-v3--account-tag)
                                                  (erc-v3--account-notify))
                                            :key #'erc-v3--capability-canon)))
    (setf (erc-v3--cap-notify-step o) erc-v3--step-OFFERED)
    (should (pcase o
              ((cl-struct erc-v3--capability
                          (canon 'cap-notify)
                          (step (pred (eq erc-v3--step-OFFERED))))
               t)))))

(ert-deftest erc-v3--non-cap-ext-p ()
  (should-not (erc-v3--non-cap-ext-p 'cap-notify))
  (should (erc-v3--non-cap-ext-p 'whox)))

(ert-deftest erc-v3--canon-from-key ()
  (should (eq (erc-v3--canon-from-key 'cap-notify) 'cap-notify))
  (should (eq (erc-v3--canon-from-key '-cap-notify) 'cap-notify))
  (should-not (erc-v3--canon-from-key 'whox)))

;; The purpose of this test is to declare a dependency on
;; `cl-struct-slot-info' and fail if the shape of its return value
;; ever changes in incompatible ways.
(ert-deftest erc-v3--get-slot-initform ()

  ;; Shadow `erc-v3--capability-cached-initforms' for this test only.
  (let ((erc-v3--capability-cached-initforms (make-hash-table :test #'equal)))
    ;; Does not work for non-cap `erc-v3--extensions'.
    (should-not (erc-v3--get-slot-initform 'erc-v3--whox 'canon))

    ;; Works for early-init forms.
    (should (equal (erc-v3--get-slot-initform 'erc-v3--sasl 'early-init)
                   #'erc-v3--sasl-on-early-init))

    ;; This (seems?) to reliably reflect the original insertion order.
    (should (equal (map-pairs erc-v3--capability-cached-initforms)
                   '(((erc-v3--whox . canon) nil)
                     ((erc-v3--sasl . early-init)
                      erc-v3--sasl-on-early-init))))))

(ert-deftest erc-v3--define-extension-subclass--minor-mode ()
  (ert-info ("Callable but not bound as a variable")
    (should (fboundp 'erc-v3--test/a-mode))
    (should (boundp 'erc-v3--test/a-mode-hook))
    (should-not (boundp 'erc-v3--test/a-mode))
    (should (eq (get 'test/a 'erc-v3--extension-minor-mode)
                'erc-v3--test/a-mode)))

  (ert-info ("Value in target buffers")
    (with-current-buffer (get-buffer-create "FooNet")
      (erc-tests-common-prep-for-insertion)
      (setq erc-server-process (start-process "t" (current-buffer) "true"))
      (set-process-query-on-exit-flag erc-server-process nil)
      (with-current-buffer (get-buffer-create "#chan")
        (erc-tests-common-prep-for-insertion)
        (setq erc-server-process
              (with-current-buffer "FooNet" erc-server-process)))

      (ert-info ("Copies server buffer's nil value to targets")
        (should-not (local-variable-p 'erc-v3--test/a))
        (should-not erc-v3--test/a)
        (erc-with-all-buffers-of-server erc-server-process nil
          (erc-v3--test/a-mode +1))
        (with-current-buffer "#chan"
          (should (local-variable-p 'erc-v3--test/a))
          (should-not (eq erc-v3--test/a t)))
        ;; Server-buffer's copy not set because redundant
        (should-not (local-variable-p 'erc-v3--test/a))
        (should-not erc-v3--test/a))

      (ert-info ("Copies server buffer's non-nil value to targets")
        (let ((v (setq erc-v3--test/a (erc-v3--test/a))))
          (should (erc-v3--test/a-p v))
          ;; Target's copy not updated automatically
          (with-current-buffer "#chan"
            (should-not erc-v3--test/a))
          ;; Manually call mode activation function
          (erc-with-all-buffers-of-server erc-server-process nil
            (erc-v3--test/a-mode +1))
          (ert-info ("Server's value intact")
            (should (eq v erc-v3--test/a)))
          (with-current-buffer "#chan"
            (should (erc-v3--test/a-p erc-v3--test/a))
            (should (eq v erc-v3--test/a)))))

      (ert-info ("Sets all instances to nil when mode disabled.")
        (erc-with-all-buffers-of-server erc-server-process nil
          (erc-v3--test/a-mode -1))
        (should-not erc-v3--test/a)
        (with-current-buffer "#chan"
          (should-not erc-v3--test/a)))

      (when noninteractive
        (kill-buffer "#chan")
        (kill-buffer)))))

(ert-deftest erc-parse-server-response ()
  (let* ((data (with-temp-buffer
                 (insert-file-contents erc-v3-tests--parser-tests)
                 (read (current-buffer))))
         (tests (assoc-default 'tests (assoc-default 'msg-split data)))
         (erc-v3-mode t)
         input atoms m ours)

    (cl-letf (((symbol-function 'erc-handle-parsed-server-response) #'ignore))
      (dolist (test tests)
        (setq input (assoc-default 'input test)
              atoms (assoc-default 'atoms test)
              m (erc-parse-server-response nil input))

        (ert-info ("Parses tags correctly")
          (setq ours (erc-response.tags m))
          (if-let* ((tags (assoc-default 'tags atoms)))
              (pcase-dolist (`(,key . ,value) ours)
                (should (string= (cdr (assq key tags)) (or value ""))))
            (should-not ours)))

        (ert-info ("Parses verbs correctly")
          (setq ours (erc-response.command m))
          (if-let* ((verbs (assoc-default 'verb atoms)))
              (should (string= (downcase verbs) (downcase ours)))
            (should (string-empty-p ours))))

        (ert-info ("Parses sources correctly")
          (setq ours (erc-response.sender m))
          (if-let* ((source (assoc-default 'source atoms)))
              (should (string= source ours))
            (should (string-empty-p ours))))

        (ert-info ("Parses params correctly")
          (setq ours (erc-response.command-args m))
          (if-let* ((params (assoc-default 'params atoms)))
              (should (equal ours params))
            (should-not ours)))))))

(cl-defun erc-v3-tests--build-message (&key tags source verb params)
  "Build an outgoing message string from parts."
  (pcase-let* ((`(,n . ,p) (reverse params))
               (rest `(,@(and source (list (concat ":" source)))
                       ,verb
                       ,@(nreverse p)
                       ,@(and n (list (concat ":" n))))))
    (concat (and tags (erc-v3--merge-client-tags tags))
            (string-join rest " "))))

;; Unfortunately, we can't really do the same for ERC's
;; message-building functions because they're currently scattered
;; everywhere.

(ert-deftest erc-v3--merge-client-tags ()
  (let* ((data (with-temp-buffer
                 (insert-file-contents erc-v3-tests--parser-tests)
                 (read (current-buffer))))
         (tests (assoc-default 'tests (assoc-default 'msg-join data))))

    (dolist (test tests)
      (let ((atoms (alist-get 'atoms test))
            (desc (alist-get 'desc test))
            (want (alist-get 'matches test)))
        (ert-info (desc)
          (should (member (erc-v3-tests--build-message
                           :tags (alist-get 'tags atoms)
                           :source (alist-get 'source atoms)
                           :verb (alist-get 'verb atoms)
                           :params (alist-get 'params atoms))
                          want)))))))

(ert-deftest erc-v3--partition-cap-tokens ()
  (let ((parts '("a" "b=x" "-c" "d" "e" "f=y")))
    (should (equal (erc-v3--partition-cap-tokens parts)
                   '((a) (b . "x") (-c) (d) (e) (f . "y"))))))

;; FIXME explain why a LIST would arrive during registration.
;;
;; XXX this is also unrealistic because current erc-v3--caps-objects
;; should match the set reported by the server as enabled.
(ert-deftest erc-v3--subcmd--list-during-registration ()
  (let ((erc-v3-mode t)
        (erc-v3--caps (make-erc-v3--caps))
        calls)
    (cl-letf (((symbol-function 'erc-display-message)
               (lambda (_ _ _ m) (push m calls)))
              ((symbol-function 'erc-server-send)
               (lambda (msg &rest _) (push msg calls))))

      (ert-info ("CAP END sent if registration unfinished and nothing pending")
        (should-not (erc-v3--caps-negotiated-p erc-v3--caps))
        (erc-call-hooks nil (make-erc-response
                             :command "CAP"
                             :command-args '("*" "LIST" "")
                             :contents ""))
        (should (erc-v3--caps-negotiated-p erc-v3--caps))
        (should (equal (pop calls) "CAP END"))
        (should-not (erc-v3--caps-continued erc-v3--caps)))

      (setf (erc-v3--caps-negotiated-p erc-v3--caps) nil)
      (ert-info ("Continuation line ignored")
        (erc-call-hooks nil (make-erc-response
                             :command "CAP"
                             :sender "irc.example.org"
                             :command-args '("*" "LIST" "*")
                             :contents "test/a test/b"))
        (should-not calls)
        (should (erc-v3--caps-continued erc-v3--caps)))

      (ert-info ("Prints message when non-empty")
        (erc-call-hooks nil (make-erc-response
                             :command "CAP"
                             :command-args '("*" "LIST" "test/c")
                             :contents "test/c"))
        (should (equal calls
                       '("CAP END" "Enabled CAPs: test/a, test/b, test/c")))
        (should-not (erc-v3--caps-continued erc-v3--caps))
        ;; No objects initialized
        (should-not (erc-v3--caps-objects erc-v3--caps))))))

;; This tests 302 continuation lines for LIST (but not LS) subcommands.

(ert-deftest erc-v3--subcmd--multiline-replies ()
  (should-not erc-v3--caps)
  (let ((erc-v3-mode t)
        (erc-v3--caps (make-erc-v3--caps))
        calls)

    (cl-letf (((symbol-function 'erc-display-message)
               (lambda (&rest r) (push r calls)))
              ((symbol-function 'erc-server-send)
               (lambda (msg &rest _) (push msg calls))))

      (ert-info ("Multiline continuation line stashed")
        (let ((msg (make-erc-response
                    :command "CAP"
                    :command-args '("*" "LIST" "*" "test/a test/b test/c")
                    :contents "test/a test/b test/c")))
          (erc-call-hooks nil msg)
          (should (equal (erc-v3--caps-continued erc-v3--caps)
                         '((test/a) (test/b) (test/c)))))
        (should-not calls))

      ;; Prevent final handler from running
      (setf (erc-v3--caps-negotiated-p erc-v3--caps) t)

      (ert-info ("Validates when terminating multiline")
        (let ((msg (make-erc-response
                    :command "CAP"
                    :command-args '("*" "LIST" "test/d test/e test/f")
                    :contents "test/d test/e test/f")))
          (erc-call-hooks nil msg))

        (should-not (erc-v3--caps-continued erc-v3--caps))
        (should-not (erc-v3--caps-advertised erc-v3--caps))
        (let ((msg (car (last (pop calls)))))
          (should (string-search "test/a" msg))
          (should (string-search "test/f" msg))))))

  (should-not erc-v3--caps))

;; This test asserts that when no common caps exist between those
;; wanted and those advertised, we still send a CAP END.

(ert-deftest erc-server-CAP--no-request ()
  (should-not erc-v3--caps)
  (setq erc-v3-mode t
        erc-v3--caps (make-erc-v3--caps :extensions '(fake)))
  (let (erc-timer-hook calls)
    (cl-letf (((symbol-function #'erc-server-send)
               (lambda (&rest r) (push r calls))))

      (ert-info ("Not called when done flag set")
        (cl-letf (((erc-v3--caps-negotiated-p erc-v3--caps) t))
          (erc-parse-server-response
           nil ":irc.example.org CAP * LS :test/a test/b")
          ;; But advertised caps recorded
          (should (equal (erc-v3-tests--slots '(canon step))
                         '((test/b offered) (test/a offered))))
          ;; REQ Queue emtpy because no common extensions (we only want 'c)
          (should-not calls)))

      (ert-info ("End signaled when queue empty and no common caps")
        (cl-letf (((erc-v3--caps-negotiated-p erc-v3--caps) nil))
          (erc-parse-server-response
           nil ":irc.example.org CAP * LS :test/a test/b")
          (should (equal (erc-v3-tests--slots '(canon step))
                         '((test/b offered) (test/a offered))))
          (should (equal (pop calls) '("CAP END")))
          (should-not calls)
          (should (erc-v3--caps-negotiated-p erc-v3--caps))))))

  (with-current-buffer (messages-buffer) (should-not erc-v3--caps)))

(ert-deftest erc-server-CAP-functions ()
  (should-not erc-v3--caps)
  (erc-mode)
  (setq erc-v3-mode t
        erc-v3--caps (make-erc-v3--caps :extensions '(test/b test/d test/e)))
  (let (erc-timer-hook calls)
    (cl-letf (((symbol-function #'erc-server-send)
               (lambda (&rest r) (push r calls)))
              ((symbol-function 'erc-server-buffer) #'current-buffer))

      (ert-info ("No handlers run when multiline response arrives")
        (erc-parse-server-response
         nil ":irc.example.org CAP * LS * :test/a test/b")
        (should-not (erc-v3-tests--slots '(canon step)))
        (should-not (erc-v3--caps-advertised erc-v3--caps)))

      (ert-info ("Terminating LS response arrives")
        (erc-parse-server-response
         nil ":irc.example.org CAP * LS :test/c test/d")
        (should-not (erc-v3--caps-continued erc-v3--caps))
        (should (equal (pop calls) '("CAP REQ :test/b test/d")))
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((test/d requested) (test/c offered)
                         (test/b requested) (test/a offered))))
        (should-not (erc-v3--caps-negotiated-p erc-v3--caps))
        (should (equal (erc-v3--caps-advertised erc-v3--caps)
                       '((test/a) (test/b) (test/c) (test/d)))))

      (ert-info ("All ACKs for connection registration arrive")
        (let ((o (should (cl-find 'test/b (erc-v3--caps-objects erc-v3--caps)
                                  :key #'erc-v3--extension-canon))))
          (setf (erc-v3--capability-enablep o)
                (lambda (o) (push (erc-v3--extension-canon o) calls))))
        (erc-parse-server-response
         nil ":irc.example.org CAP * ACK :test/b test/d")
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((test/d resolved) (test/c offered)
                         (test/b resolved) (test/a offered))))
        (should (and erc-v3--test/b erc-v3--test/d))
        (should (equal (list (pop calls) (pop calls)) '(("CAP END") test/b)))
        (should (erc-v3--caps-negotiated-p erc-v3--caps))
        (should-not calls))

      (ert-info ("NEW offer arrives after connection registration")
        (erc-parse-server-response
         nil ":irc.example.org CAP * NEW :test/e test/b=foo")
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((test/e requested) ; b already enabled
                         (test/d resolved) (test/c offered)
                         (test/b resolved) (test/a offered))))
        (should (equal (pop calls) '("CAP REQ :test/e")))
        (erc-parse-server-response nil ":irc.example.org CAP * ACK :test/e")
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((test/e resolved)
                         (test/d resolved) (test/c offered)
                         (test/b resolved) (test/a offered))))
        (should (equal (erc-v3--caps-advertised erc-v3--caps)
                       '((test/a) (test/b . "foo") (test/c)
                         (test/d) (test/e))))
        (should erc-v3--test/e)
        (should (equal (cons (pop calls) calls) '(test/b))))

      (ert-info ("DEL arrives indicating cap b no longer supported")
        (should erc-v3--test/b)
        (erc-parse-server-response
         nil ":irc.example.org CAP * DEL :test/b test/a")
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((test/e resolved) (test/d resolved)
                         (test/c offered) (test/b seen) (test/a seen))))
        (should (equal (erc-v3--caps-advertised erc-v3--caps)
                       '((test/c) (test/d) (test/e))))
        (should-not calls)
        (should-not erc-v3--test/b))))

  (with-current-buffer (messages-buffer)
    (should-not erc-v3--caps)))

;; FIXME delete any tests that rely on this fixture once convinced of
;; how the precedence model works WRT method qualifiers.  It's not
;; really reliable or maintainable because it mocks internal
;; cl--generic* structures, which we don't control or understand.

(defvar erc-v3-tests--calls nil)

(defun erc-v3-tests--with-mocked-generics (test-fn)
  (let ((erc-v3-mode t)
        erc-active-buffer erc-timer-hook erc-v3-tests--calls)
    (cl-letf (((symbol-function #'erc-server-send)
               (lambda (&rest r) (push r erc-v3-tests--calls)))
              ((symbol-function 'erc-server-buffer) #'current-buffer))
      (funcall test-fn))))

(cl-defmethod erc-v3--subcmd-LS
  ((_ null) &context ((ert-test-name (ert-running-test))
                      (eql erc-v3--subcmd--meta-ls)))
  (error "I don't run")) ; lower than (eql nil) ^

(cl-defmethod erc-v3--subcmd-LS :before
  (o &context ((ert-test-name (ert-running-test))
               (eql erc-v3--subcmd--meta-ls)))
  (push (intern (format "bef-%s" (erc-v3--extension-canon o)))
        erc-v3-tests--calls))

(cl-defmethod erc-v3--subcmd-LS :before
  ((_ erc-v3--test/a) &context ((ert-test-name (ert-running-test))
                                (eql erc-v3--subcmd--meta-ls)) )
  (push 'bef-a erc-v3-tests--calls))

(cl-defmethod erc-v3--subcmd-LS ((o erc-v3--test/a)
                                 &context ((ert-test-name (ert-running-test))
                                           (eql erc-v3--subcmd--meta-ls)))
  (should (eq erc-v3--step-OFFERED (erc-v3--extension-step o)))
  (push 'a erc-v3-tests--calls)
  (cl-call-next-method))

;; Methods can inhibit a request by altering the `wantedp' slot.
(cl-defmethod erc-v3--subcmd-LS ((o erc-v3--test/c)
                                 &context ((ert-test-name (ert-running-test))
                                           (eql erc-v3--subcmd--meta-ls)))
  (should (eq erc-v3--step-OFFERED (erc-v3--extension-step o)))
  (setf (erc-v3--extension-wantedp o) nil)
  (push 'c erc-v3-tests--calls)
  (cl-call-next-method))

(ert-deftest erc-v3--subcmd--meta-ls ()
  :tags '(:unstable)
  (let ((erc-v3--caps (make-erc-v3--caps :extensions '(test/a test/c))))

    (erc-v3-tests--with-mocked-generics
     (lambda ()

       (ert-info ("Handlers deferred")
         (erc-parse-server-response
          nil ":irc.example.org CAP * LS * :test/a test/c")
         (should-not erc-v3-tests--calls)
         (should-not (erc-v3-tests--slots '(canon step))))

       (ert-info ("Handlers run and requests sent")
         (with-temp-buffer
           (erc-mode)
           (erc-parse-server-response nil ":irc.example.org CAP * LS :test/b"))
         (should (equal
                  (erc-v3-tests--slots '(canon step))
                  '((test/b offered) (test/c offered) (test/a requested))))
         (should (equal erc-v3-tests--calls
                        '(("CAP REQ :test/a")
                          bef-test/b c bef-test/c a bef-test/a bef-a))))))))


(cl-defmethod erc-v3--subcmd-ACK
  ((o erc-v3--test/a) &context ((ert-test-name (ert-running-test))
                                (eql erc-v3--act-on-answered)))
  (should (eq erc-v3--step-ANSWERED (erc-v3--extension-step o)))
  (should (erc-v3--extension-wantedp o))
  (push (erc-v3--extension-canon o) erc-v3-tests--calls))

(cl-defmethod erc-v3--subcmd-ACK ((o erc-v3--test/c)
                                  &context ((ert-test-name (ert-running-test))
                                            (eql erc-v3--act-on-answered)))
  (should (eq erc-v3--step-ANSWERED (erc-v3--extension-step o)))
  (should (erc-v3--extension-wantedp o))
  (push (erc-v3--extension-canon o) erc-v3-tests--calls))

(cl-defmethod erc-v3--subcmd-NAK ((o erc-v3--test/b)
                                  &context ((ert-test-name (ert-running-test))
                                            (eql erc-v3--act-on-answered)))
  (should (eq erc-v3--step-ANSWERED (erc-v3--extension-step o)))
  (should-not (erc-v3--extension-wantedp o))
  (push (erc-v3--extension-canon o) erc-v3-tests--calls))


;; The function `erc-v3--act-on-answered' does not actually appear in this
;; test (but it definitely runs).
(ert-deftest erc-v3--act-on-answered ()
  :tags '(:unstable)
  (let ((erc-v3--caps (make-erc-v3--caps :extensions '(test/a test/b test/c))))

    (erc-v3-tests--with-mocked-generics
     (lambda ()

       (erc-parse-server-response
        nil ":irc.example.org CAP * LS :test/a test/b test/c")

       (should (equal
                (erc-v3-tests--slots '(canon step))
                '((test/c requested) (test/b requested) (test/a requested))))
       (should (equal (erc-v3--caps-advertised erc-v3--caps)
                      '((test/a) (test/b) (test/c))))

       (ert-info ("ACK 1 arrives")
         (erc-parse-server-response nil ":irc.example.org CAP * ACK :test/c")
         (should (equal
                  (erc-v3-tests--slots '(canon step))
                  '((test/c answered) (test/b requested) (test/a requested))))
         (should (equal (pop erc-v3-tests--calls)
                        '("CAP REQ :test/a test/b test/c"))))
       (should-not erc-v3-tests--calls)

       (ert-info ("ACK 2 arrives")
         (erc-parse-server-response nil ":irc.example.org CAP * ACK :test/a")
         (should (equal
                  (erc-v3-tests--slots '(canon step))
                  '((test/c answered) (test/b requested) (test/a answered))))
         (should-not erc-v3-tests--calls))

       (ert-info ("NAK arrives")
         (erc-parse-server-response nil ":irc.example.org CAP * NAK :test/b")
         (should (equal
                  (erc-v3-tests--slots '(canon step))
                  '((test/c resolved) (test/b offered) (test/a resolved))))
         (should (equal erc-v3-tests--calls
                        '(("CAP END") test/b test/a test/c))))))))

;; This test asserts that activation functions don't run until all the
;; ACKs for an REQ have arrived.
(ert-deftest erc-v3--capability-enablep--deferred-activation ()
  (let* ((erc-v3-mode t)
         (erc-v3--caps (make-erc-v3--caps
                        :extensions '(test/a test/b test/c)))
         calls)

    (cl-letf (((symbol-function #'erc-server-send)
               (lambda (&rest r) (push r calls))))

      ;; Pretend REQ already sent
      (setf (erc-v3--caps-objects erc-v3--caps)
            (list (erc-v3--test/a :wantedp t
                                  :depends '(test/c)
                                  :step erc-v3--step-REQUESTED
                                  :enablep (lambda (_) (push 'a calls)))
                  (erc-v3--test/b :wantedp t
                                  :depends '(test/a)
                                  :step erc-v3--step-REQUESTED
                                  :enablep (lambda (_) (push 'b calls)))
                  (erc-v3--test/c :wantedp t
                                  :step erc-v3--step-REQUESTED
                                  :enablep (lambda (_) (push 'c calls)))))

      (erc-parse-server-response
       nil ":irc.example.org CAP * ACK :test/b test/a")
      (should (equal (erc-v3-tests--slots '(canon step))
                     '((test/a answered)
                       (test/b answered)
                       (test/c requested))))

      (erc-parse-server-response
       nil ":irc.example.org CAP * ACK :test/c")
      (should (equal (erc-v3-tests--slots '(canon step))
                     '((test/a resolved)
                       (test/b resolved)
                       (test/c resolved))))

      (should (equal calls '(("CAP END") b a c)))
      (should (erc-v3--caps-negotiated-p erc-v3--caps)))))

(ert-deftest erc-v3--extension-step--post-registration ()
  (should-not erc-v3--caps)
  (erc-mode)
  (setq erc-v3-mode t
        erc-v3--caps (make-erc-v3--caps :extensions '(cap-notify)))
  (let (erc-timer-hook
        erc-v3--cap-notify
        calls)
    (cl-letf (((symbol-function #'erc-server-send)
               (lambda (&rest r) (push r calls)))
              ((symbol-function 'erc-server-buffer) #'current-buffer))

      (ert-info ("LS arrives, REQ sent")
        (erc-parse-server-response
         nil ":irc.example.org CAP * LS :cap-notify")
        (should-not (erc-v3--caps-continued erc-v3--caps))
        (should (equal (pop calls) '("CAP REQ :cap-notify")))
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((cap-notify requested))))
        (should-not (erc-v3--caps-negotiated-p erc-v3--caps)))

      (ert-info ("ACK arrives, END sent")
        (should-not erc-v3--cap-notify)
        (erc-parse-server-response
         nil ":irc.example.org CAP * ACK :cap-notify")
        (should erc-v3--cap-notify)
        (should (equal (pop calls) '("CAP END")))
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((cap-notify resolved))))
        (should (erc-v3--caps-negotiated-p erc-v3--caps)))

      (ert-info ("REQ to drop caps")
        ;; Pretend some other v3 extension sends REQ -cap-notify
        (setf (erc-v3--extension-wantedp
               (should (cl-find 'cap-notify
                                (erc-v3--caps-objects erc-v3--caps)
                                :key #'erc-v3--capability-canon)))
              nil)
        (erc-parse-server-response
         nil ":irc.example.org CAP * ACK :-cap-notify")
        (should-not erc-v3--cap-notify)
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((cap-notify offered))))
        (should (erc-v3--caps-negotiated-p erc-v3--caps))
        (should-not calls))

      (ert-info ("Cap offer rescinded")
        (erc-parse-server-response
         nil ":irc.example.org CAP * DEL :cap-notify")
        (should-not erc-v3--cap-notify)
        (should (equal (erc-v3-tests--slots '(canon step))
                       '((cap-notify seen))))
        (should (erc-v3--caps-negotiated-p erc-v3--caps))
        (should-not calls))))

  (with-temp-buffer
    (should-not erc-v3--cap-notify)
    (should-not erc-v3--caps)))

(ert-deftest erc-server-410 ()
  (let ((erc-insert-modify-hook)
        (erc-server-current-nick "tester"))
    (erc-tests-common-prep-for-insertion)
    (erc-server-410
     nil (make-erc-response :unparsed ":irc.gnu.org 410 * FOO :Invalid"
                            :sender "irc.gnu.org"
                            :command "410"
                            :command-args '("*" "FOO")
                            :contents "Invalid"))
    (goto-char (point-min))
    (should (search-forward "*** Invalid CAP subcommand: FOO" nil t))))


;;;; Extension: multi-prefix

(ert-deftest erc-v3--multi-prefix-split-flags ()
  (let ((erc-server-process (start-process "nil" (current-buffer) "true"))
        (erc--isupport-params (make-hash-table))
        (erc-server-parameters '(("PREFIX" . "(Yqaohv)!~&@%+"))))
    (set-process-query-on-exit-flag erc-server-process nil)
    (should (equal (erc-v3--multi-prefix-split-flags "H~&@%+")
                   '(?H nil (31 . "qaohv") nil)))
    (should (equal (erc-v3--multi-prefix-split-flags "H+")
                   '(?H nil (1 . "v") nil)))
    (should (equal (erc-v3--multi-prefix-split-flags "H*!r")
                   '(?H t (32 . "Y") (?r))))
    (should (equal (erc-v3--multi-prefix-split-flags "H+Bs")
                   '(?H nil (1 . "v") (?B ?s))))
    (should (equal (erc-v3--multi-prefix-split-flags "H")
                   '(?H nil (0 . "") nil)))
    (should-not (erc-v3--multi-prefix-split-flags ""))))

(ert-deftest erc-server-352 () ; WHO
  (erc-tests-common-make-server-buf (buffer-name))
  (setq erc-server-current-nick "tester"
        erc-v3--caps (make-erc-v3--caps
                      :extensions '(multi-prefix)
                      :objects (list (erc-v3--multi-prefix
                                      :step erc-v3--step-RESOLVED
                                      :wantedp t)))
        erc-server-users (make-hash-table :test 'equal)
        erc--isupport-params (make-hash-table)
        erc-server-parameters '(("PREFIX" . "(Yqaohv)!~&@%+"))
        erc-active-buffer nil
        erc-v3--multi-prefix (erc-v3--multi-prefix)
        erc-channel-new-member-names nil)

  (let* (erc-timer-hook
         erc-channel-members-changed-hook
         ;;
         (req (concat ":irc.example.org 352 * #chan"
                      " hugle fsf/person/hugle *.fsf.org hugle H%+"
                      " :0 F. S. Hugle"))
         (expect `([erc-server-user "hugle" "fsf/person/hugle" "hugle"
                                    "F. S. Hugle" nil ,(current-buffer)]
                   . [erc-channel-user 3 nil])))
    (with-current-buffer (erc--open-target "#chan")
      (setq erc-v3--multi-prefix t)
      (erc-update-current-channel-member "hugle" "hugle" t))

    (should (equal '((?Y . ?!) (?q . ?~) (?a . ?&)
                     (?o . ?@) (?h . ?%) (?v . ?+))
                   (erc--parsed-prefix-alist (erc--parsed-prefix))))

    ;; Force display in #chan to simulate /WHO
    (setq erc-active-buffer (get-buffer "#chan"))
    (erc-parse-server-response erc-server-process req)

    (with-current-buffer "#chan"
      (let ((actual (gethash "hugle" erc-channel-users)))
        (should (equal (seq-subseq (car actual) 0 5)
                       (seq-subseq (car expect) 0 5)))
        (erc-with-server-buffer
          (should (equal (seq-subseq (gethash "hugle" erc-server-users) 0 5)
                         (seq-subseq (car expect) 0 5))))
        (should (equal (cdr actual) (cdr expect))))
      (when noninteractive (kill-buffer)))))


;;;; Extension: userhost-in-names

(ert-deftest erc-v3--multi-prefix--partition ()
  (let ((s "+%@&~"))
    (should (equal '(31 "~&@%+" "bob")
                   (erc-v3--multi-prefix--partition "~&@%+bob" s)))
    (should (equal '(15 "&@%+" "bob")
                   (erc-v3--multi-prefix--partition "&@%+bob" s)))
    (should (equal '(7 "@%+" "bob")
                   (erc-v3--multi-prefix--partition "@%+bob" s)))
    (should (equal '(3 "%+" "bob")
                   (erc-v3--multi-prefix--partition "%+bob" s)))
    (should (equal '(1 "+" "bob")
                   (erc-v3--multi-prefix--partition "+bob" s)))
    (should (equal '(0 "" "bob")
                   (erc-v3--multi-prefix--partition "bob" s)))
    (should (equal '(1 "+" "")
                   (erc-v3--multi-prefix--partition "+" s)))))

(defun erc-v3-tests--channel-receive-names (input result-fn)
  (erc-tests-common-make-server-buf)
  (setq erc-server-parameters '(("PREFIX" . "(Yqaohv)!~&@%+")))
  (should (equal '((?Y . ?!) (?q . ?~) (?a . ?&) (?o . ?@) (?h . ?%) (?v . ?+))
                 (erc--parsed-prefix-alist (erc--parsed-prefix))))
  (with-current-buffer (erc--open-target "#chan")
    (let (erc-channel-members-changed-hook)
      ;; Skips lone status-prefix char "+" (from orignial impl.).
      (erc-channel-receive-names input)
      (erc-channel-end-receiving-names)
      (let ((result (funcall result-fn)))
        (dolist (actual (sort (map-pairs erc-channel-users)
                              (lambda (a b) (string< (car a) (car b)))))
          (let ((expected (pop result)))
            (should (equal (car actual) (car expected)))
            (should (equal actual expected))
            (should (equal (gethash (car actual) (erc-with-server-buffer
                                                   erc-server-users))
                           (cadr expected)))))))
    (kill-buffer)))

(ert-deftest erc-channel-receive-names--baseline ()
  (erc-v3-tests--channel-receive-names
   "@ChanServ +susnight + m hugle znebot agnesi"
   ;; host login full-name info account buffers
   (lambda ()
     (let ((rest `(nil nil nil nil nil (,(current-buffer)))))
       `(("agnesi" [erc-server-user "agnesi" ,@rest]
          . [erc-channel-user 0 nil])
         ("chanserv" [erc-server-user "ChanServ" ,@rest]
          . [erc-channel-user 4 nil])
         ("hugle" [erc-server-user "hugle" ,@rest]
          . [erc-channel-user 0 nil])
         ("m" [erc-server-user "m" ,@rest]
          . [erc-channel-user 0 nil])
         ("susnight" [erc-server-user "susnight" ,@rest]
          . [erc-channel-user 1 nil])
         ("znebot" [erc-server-user "znebot" ,@rest]
          . [erc-channel-user 0 nil]))))))

(ert-deftest erc-channel-receive-names--enabled ()
  (should-not erc-v3--userhost-in-names)
  (let ((erc-v3--userhost-in-names (erc-v3--userhost-in-names))
        (erc-v3--353-work (make-erc-v3--353-work))
        ;; full-name info account buffers
        (rest `(nil nil nil (,(current-buffer)))))
    (lambda ()
      (erc-v3-tests--channel-receive-names
       (concat " @+susnight!~susnight@fsf/member/susnight"
               " %+hugle!hugle@fsf/person/hugle"
               " +" ; pathological (impossible?) case from orig
               " ~u@fake.com" ; ignored: can't index w/o nick
               " ride@foo" ; ignored: same as ^ (no nick)
               " lovelace!bar" ; interpret "bar" as host
               " hamilton!" ; see comment re bouncers in `erc-parse-user'
               " franklin@" ; allow, discarding @
               " m!~u@example.org"
               " +znebot!~znebot@user/pike/bot/znebot"
               " +agnesi")
       `(("agnesi"
          [erc-server-user "agnesi" nil nil ,@rest]
          . [erc-channel-user 1 nil])
         ("franklin"
          [erc-server-user "franklin" nil nil ,@rest]
          . [erc-channel-user 0 nil])
         ("hamilton"
          [erc-server-user "hamilton" nil nil ,@rest]
          . [erc-channel-user 0 nil])
         ("hugle"
          [erc-server-user "hugle" "fsf/person/hugle" "hugle" ,@rest]
          . [erc-channel-user 3 nil])
         ("lovelace"
          [erc-server-user "lovelace" "bar" nil ,@rest]
          . [erc-channel-user 0 nil])
         ("m"
          [erc-server-user "m" "example.org" "~u" ,@rest]
          . [erc-channel-user 0 nil])
         ("susnight"
          [erc-server-user "susnight" "fsf/member/susnight" "~susnight" ,@rest]
          . [erc-channel-user 5 nil])
         ("znebot"
          [erc-server-user "znebot" "user/pike/bot/znebot" "~znebot" ,@rest]
          . [erc-channel-user 1 nil]))))))


;;;; Extension WHOX

;; This also (somewhat) tests `erc-v3--whox-enqueue' and also
;; `erc-v3--whox-make-request'.
(ert-deftest erc-v3--whox-delete ()
  (setq erc-v3--whox (erc-v3--whox))
  (let (calls)
    (cl-letf (((symbol-function 'erc-server-send)
               (lambda (&rest r) (push r calls))))
      (erc-v3--whox-enqueue "#foo" "cuhnflar" #'ignore)
      (should (equal calls '(("WHO #foo %tcuhnflar,1" nil "#foo"))))
      (should (equal (erc-v3--whox-requests erc-v3--whox)
                     '(#s(erc-v3--whox-request 1 "#foo" "cuhnflar" ignore))))
      (erc-v3--whox-enqueue "#bar" "cuhnflar" #'ignore)
      (should (equal (car (erc-v3--whox-requests erc-v3--whox))
                     #s(erc-v3--whox-request 2 "#bar" "cuhnflar" ignore)))
      (erc-v3--whox-delete 1)
      (should (equal (car (erc-v3--whox-requests erc-v3--whox))
                     #s(erc-v3--whox-request 2 "#bar" "cuhnflar" ignore))))))

;; An earlier version recorded "idle time" as an item in
;; `erc-server-user-info'.  If re-adding, use 1580601600, as in
;;
;;   $ date --date @1580601600 --utc Sun Feb 2 12:00:00 AM UTC 2020
;;
(ert-deftest erc-server-354--whox ()
  (erc-tests-common-make-server-buf (buffer-name))
  (setq erc-v3--caps (make-erc-v3--caps
                      :extensions '(multi-prefix whox)
                      :objects (list (erc-v3--multi-prefix
                                      :step erc-v3--step-RESOLVED
                                      :wantedp t)
                                     (erc-v3--whox :wantedp t)))
        erc-server-users (make-hash-table :test 'equal)
        erc--isupport-params (make-hash-table)
        erc-v3-mode t
        erc-v3--multi-prefix (erc-v3--multi-prefix))
  (set-process-query-on-exit-flag erc-server-process nil)

  (let* ((erc-server-current-nick "tester")
         erc-kill-channel-hook erc-kill-server-hook erc-kill-buffer-hook
         erc-timer-hook
         erc-channel-members-changed-hook)

    ;; Extension only activated once ISUPPORT token arrives.
    (should-not erc-v3--whox)
    (erc-parse-server-response
     erc-server-process
     ":irc.foonet.org 005 tester PREFIX=(qaohv)~&@%+ WHOX=390 :supported")
    (should (equal (erc-v3-tests--slots '(canon step))
                   '((multi-prefix resolved) (whox resolved))))
    (should erc-v3--whox)

    ;; Simulate outgoing request for members in #chan.
    (push (make-erc-v3--whox-request
           :token 1
           :target "#chan"
           :fields "cuhnflar"
           :handler #'erc-v3--whox-handle-channel-user)
          (erc-v3--whox-requests erc-v3--whox))
    (cl-incf (erc-v3--whox-token erc-v3--whox))

    (with-current-buffer (erc--open-target "#chan")
      (erc-v3-mode +1)
      (should erc-v3--whox)
      (should erc-v3--multi-prefix)
      (erc-update-current-channel-member "hugle" "hugle" t))

    ;; Simulate a single entry arriving for nick "hugle".
    (setq erc-active-buffer (get-buffer "#chan")) ; why?
    (erc-parse-server-response
     erc-server-process
     (concat ":irc.example.org 354 tester 1 #chan"
             " ~hugle fsf/person/hugle hugle H@+Br 90 fshugle"
             " :F. S. Hugle"))

    ;; Store successfully updated.
    (with-current-buffer "#chan"
      (let ((actual (gethash "hugle" erc-channel-users)))
        (should (equal (car actual)
                       `[erc-server-user "hugle" "fsf/person/hugle" "~hugle"
                                         "F. S. Hugle" nil "fshugle"
                                         ,(list (current-buffer))]))
        (should (eq (car actual) (erc-get-server-user "hugle")))
        (should (equal (cdr actual) [erc-channel-user 5 nil])))
      (when noninteractive (kill-buffer)))))

(erc-v3--define-capability test/c2sv :slots (abc xyz))

(ert-deftest erc-v3--cap-to-slot-values ()
  (let ((obj (erc-v3--test/c2sv :val "abc=1,xyz=2"))
        (fn (lambda (_ v) (string-to-number v))))
    (should (eq obj (erc-v3--cap-to-slot-values obj fn)))
    (should (= (erc-v3--test/c2sv-abc obj) 1))
    (should (= (erc-v3--test/c2sv-xyz obj) 2)))

  (let ((obj (erc-v3--test/c2sv :val "abc,xyz=42"))
        (fn (lambda (_ v) (if v (string-to-number v) t))))
    (should (eq obj (erc-v3--cap-to-slot-values obj fn)))
    (should (eq (erc-v3--test/c2sv-abc obj) t))
    (should (= (erc-v3--test/c2sv-xyz obj) 42))))

;;; erc-v3-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

