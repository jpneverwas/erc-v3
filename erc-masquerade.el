;;; erc-masquerade.el --- display-names, relaymsg, etc. -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2024 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; *Update 2024*: This is currently a placeholder for a demo module
;; whose purpose will be to extol the virtues of whatever next-gen API
;; emerges from bug#68861.  For a more practical, minimal "bridge bot"
;; module actually compatible with the current ERC release, see
;; <https://git.sr.ht/~technomancy/erc-bridge-nicks>.
;;
;; Some channels are "bridged" to communities on other protocols, like
;; Matrix and XMPP or commercial chat platforms.  Sometimes,
;; individual remote presences aren't "puppeted" on the local IRC
;; side.  Instead, a so-called "relay bot" preforms a degraded type of
;; forwarding by sending messages prefixed with the names of speakers.
;; This results in a frustrating user experience:
;;
;;   <relaybot> <alice@telegram> telegram is bad
;;   <relaybot> <bob@matrix.org> matrix is slwo
;;   <relaybot> <bob@matrix.org> matrix is slow
;;
;; In this example, only relaybot's nick is ever highlighted or
;; offered as a completion suggestion, and bob's last message is isn't
;; marked as being an edit.  This module addresses the first problem
;; to varying degrees but does so at the expense of making it
;; immediately obvious to users when they're dealing with a bot.
;;
;; To get started, set `erc-masquerade-bots' to something like
;;
;;   ((Libera.Chat ("#chan" "MyBot" "\\`<\\(.+\\)> " "")
;;                 ("#spam" "UrBot" "\\`<\\(.+\\):\\(.+\\)> " "\\2")))
;;
;; And add `masquerade' to `erc-modules' before connecting.

;;; Background:

;; Right now (2022), there are two draft specs from IRCv3 that touch
;; on various c2s issues involving relay bots and display-only names:
;;
;;   https://github.com/ircv3/ircv3-specifications/pull/417
;;   https://github.com/ircv3/ircv3-specifications/pull/452
;;
;; Word is that neither is poised to take off.  If that remains true,
;; something addressing similar problems will likely emerge.  In the
;; meantime, we can still try to improve the experience by minimizing
;; the distraction of untreated relay-bot messages while still clearly
;; indicating when spoofing is taking place.  To that end, this file
;; provides a module called `masquerade' that fake-implements
;; `display-name' for bots declared a priori.  This is preferable to
;; rewriting relayed messages as being native.

;;; Code:
(require 'erc-button)
(require 'erc-goodies)

(defgroup erc-masquerade nil
  "Display-names for \"bridged\" users from other platforms."
  :group 'erc
  :package-version '(ERC . "5.7"))

(define-obsolete-variable-alias 'erc-masquerade-relay-bots
  'erc-masquerade-bots "30.1")
(defcustom erc-masquerade-bots nil
  "Network nicks to treat as relay bots.
More precisely, an alist of (NETWORK CHANSPEC1 CHANSPEC2 ...) where
NETWORK is a symbol and CHANSPEC is a list containing (CHAN BOTNICK
SPEAKER REPLACER).  Here, the first three items in a CHANSPEC are regexp
patterns.  When matching, ERC surrounds the first two with
`string-start' and `string-end' zero-width assertions (\\\\=` and \\\\='
respectively).  ERC uses the entire matched substring, sans surrounding
whitespace and angle brackets as a unique identifier for the virtual
user, which may be relevant when bridging communities from many chat
platforms.  The first matched group becomes the display name.
Additional groups may exists and be referenced by REPLACER, which
specifies text to substitute for the entire matched portion.  It can
include specifiers known to `replace-match'.

REPLACER can also be a function that accepts two arguments: the
message body and the above mentioned SPEAKER regexp, which they may
choose to ignore.  It must either return nil, to signal that the message
should instead be processed normally, or a list of these items:

  1. a canonical key, ideally unique among virtual nicks in the channel
  2. a display name to be shown as the speaker in place of the robot's
     nick; typically identical to 2 or a truncated version
  3. a replacement for the matched portion of the message
  4. a replacement for the remainder of the message body
  5. an optional face for colorizing the speaker when `nicks' is enabled

This latter function form mainly exists for ERC itself to provide ready
made handlers for well known bots of a specific make, model, and
configuration.

If you tinker with this option during a live IRC session, cycle the
module's minor mode for the changes to take effect."
  :type (let ((pats `(choice :tag "Speaker display name pattern"
                             (regexp :tag "Full: <{Name}> {Msg}"
                                     ,(rx "<" (group (+? nonl)) "> "))
                             (regexp :tag "Partitioned: <{Name:Host}> {Msg}"
                                     ,(rx "<" (group (+? nonl)) ":"
                                          (group (+? nonl)) "> "))
                             (regexp :tag "Speaker pattern")))
              (reps '(choice :tag "Replacer"
                             (string :tag "Partitioned" "\\2 ")
                             (string :tag "User-provided string")
                             (function-item
                              :tag "Mattermost bot with platform prefix"
                              erc-masquerade-parse-fallback-mattermost)
                             (function :tag "User-provided function"))))
          `(alist :key-type (symbol :tag "Network")
                  :value-type (repeat (list (regexp :tag "Channel pattern")
                                            (regexp :tag "Bot nick pattern")
                                            ,pats ,reps)))))

(defcustom erc-masquerade-prefer-mirc-colors-for-nicks t
  "Whether to prefer faces derived from MIRC control-sequences.
This only matters when the `nicks' module is enabled.  Some bots enclose
the entire fallback prefix in control chars, such as
\"\\C-c02<{DisplayName}> \\C-o{MessageContent}\".  When enabled, this
options tells ERC to retain the interpreted face rather than let `nicks'
choose one.  See also `erc-controls-interpret'."
  :type 'boolean)

(defface erc-masquerade-alt-prefix-face
  '((t :inherit (erc-notice-face italic)))
  "Face for text that replaces the \"fallback\" prefix.
For example, if an original message \"<bot> <nick@host> msg\" becomes
\"<nick> host msg\", then use this face for the \"host \" portion."
  :group 'erc-faces)

(defvar-local erc-masquerade--match-replacer nil)
(defvar-local erc-masquerade--botnick-regexp nil)
(defvar-local erc-masquerade--speaker-regexp nil)
;; FIXME rename to `erc-masquerade--frag-to-nonirc'
(defvar-local erc-masquerade--partial-to-nonstandard nil)

(defvar-local erc-masquerade--display-names nil
  "Hash table mapping visible displayed name to full, given display name.
For example, if the original display name from the bot's message
was \"<joe:example.com>\", and we see the speaker as \"<joe>\",
then this has an item mapping \"joe\" to \"joe:example.com\".")

(defvar erc-masquerade--canonicalize-prefix #'erc-masquerade--strip-chars)
(defvar erc-masquerade--detect-edit-function nil)
(defvar erc-masquerade--expanded-syntax-table nil)
(defvar erc-masquerade--extra-word-chars ".")

(define-erc-module masquerade nil
  "Replace speakers with \"display names\".

This is a local module whose minor mode is non-nil in target buffers.
In-session changes to `erc-masquerade-bots' will not be detected.  If
reconnecting is impossible, try cycling the minor mode off and then on
again in the appropriate buffer."
  ((cond
    ((and erc--target (erc-masquerade--determine-channel-info))
     (add-hook 'erc-insert-pre-hook 'erc-masquerade--override-spkr-prop -50 t)
     (add-function :around (local 'erc--cmem-from-nick-function)
                   #'erc-masquerade--adorn '((depth . 30)))
     (when (erc--module-active-p 'fill)
       (add-function :after-while (local 'erc-fill--wrap-continued-predicate)
                     #'erc-masquerade--fill-wrap-on-continued-message-p
                     '((depth . -50))))
     (when (erc--module-active-p 'nicks)
       (add-function :filter-args (local 'erc-button--modify-nick-function)
                     #'erc-masquerade--massage-nick '((depth . -80))))
     (setq
      erc-masquerade--display-names (make-hash-table :test #'equal)
      erc-masquerade--partial-to-nonstandard (make-hash-table :test #'equal)))
    (t (erc-masquerade-mode -1))))
  ((remove-hook 'erc-insert-pre-hook 'erc-masquerade--override-spkr-prop t)
   (remove-function (local 'erc-fill--wrap-continued-predicate)
                    #'erc-masquerade--fill-wrap-on-continued-message-p)
   (remove-function (local 'erc--cmem-from-nick-function)
                    #'erc-masquerade--adorn)
   (remove-function (local 'erc-button--modify-nick-function)
                    #'erc-masquerade--massage-nick)
   (kill-local-variable 'erc-masquerade--botnick-regexp)
   (kill-local-variable 'erc-masquerade--speaker-regexp)
   (kill-local-variable 'erc-masquerade--match-replacer)
   (kill-local-variable 'erc-masquerade--partial-to-nonstandard)
   (kill-local-variable 'erc-masquerade--display-names))
  localp)

(defun erc-masquerade--strip-chars (text)
  "Helper for relay-bot functions to clean puppet names."
  (string-replace "\u200b" ""
                  (if erc-masquerade-prefer-mirc-colors-for-nicks
                      (let ((erc-interpret-mirc-color t))
                        (erc-controls-interpret text))
                    (erc-controls-strip text))))

(defun erc-masquerade--override-spkr-prop (string)
  "Replace real `erc--spkr' with bridged user.
Also swap out placeholder face property in prefix portion of STRING."
  (when-let* ((beg (text-property-not-all
                    0 (length string) 'erc-masquerade--pfx-face nil string))
              (end (next-single-property-change
                    beg 'erc-masquerade--pfx-face string))
              (val (get-text-property beg 'erc-masquerade--pfx-face string)))
    (remove-list-of-text-properties beg end
                                    '(erc-masquerade--pfx-face string) string)
    (put-text-property beg end 'font-lock-face val string))
  (when-let* ((bridged (erc--check-msg-prop 'erc--masquerade-key)))
    (puthash 'erc--spkr bridged erc--msg-props)
    (remhash 'erc--masquerade-key erc--msg-props)))

(define-obsolete-function-alias 'erc-masquerade-parse-fallback-host
  'erc-masquerade--preform-legacy-fallback "30.1")
(defun erc-masquerade--preform-legacy-fallback (msg &rest _)
  (erc-masquerade--match-nick-default
   msg (rx "<" (group (+? nonl)) ":" (group (+? nonl)) "> ") "\\2 "))

(defun erc-masquerade--canonicalize-prefix-mattermost (fallback)
  (let (user platform)
    (if-let* ((stripped (erc-masquerade--strip-chars fallback))
              (proto (string-search "] " stripped)))
        (setq user (substring stripped (+ proto 3) -2)
              platform (substring stripped 1 proto))
      (setq user (substring stripped 1 -2)))
    (when (and platform (string-suffix-p "-" platform))
      (setq platform (substring platform 0 -1)))
    (concat user (and platform ":") platform)))

(defun erc-masquerade-parse-fallback-mattermost (msg _)
  "Example prefix parser for Mattermost bot (as it behaved in 2022).
Process MSG, ignoring the `erc-masquerade-bot' option's REGEXP.  Only
handle bridges configured with `RemoteNickFormat' set to either
\"[{PROTOCOL}] <{NICK}> \" or \"<{NICK}> \".  Otherwise behave like
`erc-masquerade--match-nick-default' with the replacer parameter set to
sub-expression 2."
  (let ((erc-masquerade--canonicalize-prefix
         #'erc-masquerade--canonicalize-prefix-mattermost))
    (erc-masquerade--preform-legacy-fallback msg)))

(defun erc-masquerade--match-nick-default (msg regexp subst)
  "Return MSG parts partitioned via REGEXP after replacing with SUBST."
  (when-let* (((string-match regexp msg))
              (msg (funcall erc-masquerade--canonicalize-prefix msg))
              ((string-match regexp msg))
              (display (substring-no-properties (match-string 1 msg)))
              (trimmed (substring-no-properties msg (match-end 0)))
              (prefix (substring msg 0 (match-end 0)))
              (replaced (substring-no-properties
                         (replace-match subst nil nil prefix)))
              (canon (substring-no-properties
                      (string-trim prefix "[ <]+" "[ >]+"))))
    (let ((face (and-let* ((erc-masquerade-prefer-mirc-colors-for-nicks)
                           (val (get-text-property 0 'font-lock-face prefix))
                           (face (car-safe val))
                           ((symbolp face))
                           ((string-prefix-p "fg:erc-" (symbol-name face))))
                  face)))
      (list canon display replaced trimmed face))))

(defun erc-masquerade--determine-channel-info ()
  "Set local variables for function and bot-nick, returning the latter.
If not found, return nil.  For compatibility, allow the function
to be nil."
  (when-let* ((entries (alist-get (erc-network) erc-masquerade-bots))
              (entry (alist-get (erc-default-target) entries nil nil
                                (lambda (pattern target)
                                  (string-match (concat "\\`" pattern "\\'")
                                                target)))))
    (when (functionp (nth 1 entry))
      (setq entry (list (car entry) (rx unmatchable) (nth 1 entry)))
      (erc-button--display-error-notice-with-keys
       (erc-server-buffer)
       "The shape of `erc-masquerade-bots's expected value has changed."
       " Please take corrective action."))
    (pcase-exhaustive entry
      (`(,botpat ,spkrpat ,replacement)
       (when (and erc-masquerade-prefer-mirc-colors-for-nicks
                  (string-suffix-p " " spkrpat))
         (setq spkrpat (concat spkrpat (rx (? ?\C-o)))))
       (setq erc-masquerade--match-replacer replacement
             erc-masquerade--botnick-regexp (concat "\\`" botpat "\\'")
             erc-masquerade--speaker-regexp spkrpat)))))

(defun erc-masquerade--add-speaker-to-phantom-users (downcased name)
  "Add speaker to `erc-button--phantom-users' and return bot user.
Also map speaker to bot data in `erc-button--nicks-to-data'."
  (cl-assert erc-button-mode)
  (cl-assert erc-button--phantom-users-mode)
  (or (gethash downcased erc-button--phantom-cmems)
      (and-let* ((bot-nick (erc-extract-nick
                            (erc-response.sender erc--parsed-response)))
                 (bot-down (erc-downcase bot-nick))
                 (cmem (erc-button--find-data-channel-member bot-down))
                 (bot-data (gethash bot-down erc-button--nicks-to-data)))
        ;; Handle names containing nonstandard chars.
        (when-let* ((found (string-match
                            (concat "[" erc-masquerade--extra-word-chars "]")
                            name))
                    (partial (substring name 0 found)))
          (puthash (erc-downcase partial) cmem erc-button--phantom-cmems)
          (puthash partial name erc-masquerade--partial-to-nonstandard))
        (puthash downcased bot-data erc-button--nicks-to-data)
        (puthash downcased cmem erc-button--phantom-cmems))))

(defun erc-masquerade--adorn (orig parts)
  "Stylize message to impersonate real channel members.
Defer to `erc-masquerade--detect-edit-function' to handle edit
detection and styling."
  (if-let* ((botnick-downcased (erc--msg-parts-key parts))
            ((string-match erc-masquerade--botnick-regexp botnick-downcased))
            (msg (erc--msg-parts-msg parts))
            (pat erc-masquerade--speaker-regexp)
            (rep erc-masquerade--match-replacer)
            (result (if (functionp rep)
                        (funcall rep msg pat)
                      (erc-masquerade--match-nick-default msg pat rep))))
      (pcase-let* ((`(,canon ,display ,prefix ,emsg ,face) result)
                   ((cl-struct erc--msg-parts login host) parts)
                   ;; File phony cmem under key "<downcased> <display-name>".
                   (key (concat botnick-downcased " " canon))
                   (cmem (or (gethash key erc-channel-members)
                             (cons (make-erc--phantom-server-user
                                    :nickname display :host host :login login)
                                   (make-erc--phantom-channel-user)))))
        (unless emsg (setq emsg (erc--msg-parts-msg parts)))
        (setf (erc--msg-parts-key parts) key
              ;;
              (erc--msg-parts-msg parts)
              (concat (and prefix (not (string-empty-p prefix))
                           (propertize prefix
                                       'erc-masquerade--pfx-face
                                       'erc-masquerade-alt-prefix-face))
                      (or (and erc-masquerade--detect-edit-function
                               (funcall erc-masquerade--detect-edit-function
                                        canon emsg))
                          emsg)))
        (when erc-button--phantom-users-mode
          (erc-masquerade--add-speaker-to-phantom-users (erc-downcase display)
                                                        display))
        (puthash display canon erc-masquerade--display-names)
        (puthash key cmem erc-channel-members)
        (when face
          (push (cons 'erc--masquerade-face face) erc--msg-prop-overrides))
        (push (cons 'erc--masquerade-key key) erc--msg-prop-overrides)
        (push (cons 'erc--masquerade canon) erc--msg-prop-overrides)
        cmem)
    (funcall orig parts)))


;;;; `fill-wrap' integration

(defun erc-masquerade--fill-wrap-on-continued-message-p ()
  (when-let* ((beg (text-property-any (point-min) (point-max) 'font-lock-face
                                      'erc-masquerade-alt-prefix-face))
              (end (next-single-property-change beg 'face)))
    (goto-char end))
  t)

;;;; `nicks' integration

(defun erc-masquerade--bounds-of-word-at-point ()
  "Return more liberal bounds than allowed by `erc-button-syntax-table'."
  ;; FIXME move this into a public setter to allow user-defined bot
  ;; functions to alter the table.
  (unless erc-masquerade--expanded-syntax-table
    (let ((table (setq erc-masquerade--expanded-syntax-table
                       (copy-syntax-table erc-button-syntax-table))))
      (erc--doarray (c erc-masquerade--extra-word-chars)
        (modify-syntax-entry c "w" table))))
  (with-syntax-table erc-masquerade--expanded-syntax-table
    (erc-bounds-of-word-at-point)))

(defun erc-masquerade--resolve-partial (name nick-object)
  "Rewrite bounds of NICK-OBJECT if NAME resides in expanded word at point."
  (when-let* ((expanded (erc-masquerade--bounds-of-word-at-point))
              ((save-excursion (goto-char (car expanded))
                               (search-forward name (cdr expanded) 'noerror))))
    (setf (erc-button--nick-bounds nick-object)
          (cons (match-beginning 0) (match-end 0)))
    (cl-assert (equal name (match-string 0)))
    (let ((found (gethash name erc-masquerade--display-names)))
      (cl-assert found t)
      found)))

(declare-function erc-nicks--get-face "erc-nicks" (nick key))

;; FIXME don't highlight over `erc-masquerade-alt-prefix-face'.
(defun erc-masquerade--massage-nick (args)
  (cl-assert (erc-button--nick-user (car args)))
  (when-let* ((nick-object (car args))
              (erc--target)
              (server-user (erc-button--nick-user nick-object))
              (bounds (erc-button--nick-bounds nick-object)))
    ;; Set the button-nick object's `downcased' slot to the full
    ;; display name, so `erc-nicks--highlight' uses it for its "key".
    ;; Known speakers, like <bob>, have this in a `erc--masquerade'
    ;; msg prop.  Otherwise, look in `erc-masquerade--display-names'.
    (let ((masq (erc--check-msg-prop 'erc--masquerade))
          (alt-face (erc--check-msg-prop 'erc--masquerade-face)))
      (when-let*
          (((not (and masq (get-text-property (car bounds) 'erc--speaker))))
           (str (buffer-substring-no-properties (car bounds) (cdr bounds)))
           (tab erc-masquerade--partial-to-nonstandard)
           (seen (or (gethash str erc-masquerade--display-names)
                     (and-let* ((name (gethash str tab))
                                (alt (erc-masquerade--resolve-partial
                                      name nick-object)))
                       (setq str name)
                       alt))))
        (setq masq seen))
      (when masq
        (setf
         (erc-button--nick-downcased nick-object) (erc-downcase masq)
         (erc-button--nick-user nick-object) (copy-erc-server-user server-user)
         (erc-server-user-nickname (erc-button--nick-user nick-object)) masq)
        ;; Replace natural color with assigned `irccontrols' face.
        ;; FIXME provide internal API to accomplish this painlessly.
        (when alt-face
          (defvar erc-nicks-colors)
          (defvar erc-nicks--colors-pool)
          (defvar erc-nicks--colors-len)
          (erc-with-server-buffer
            (let ((erc-nicks-colors)
                  (erc-nicks--colors-pool (list (face-foreground alt-face)))
                  (erc-nicks--colors-len 1))
              (erc-nicks--get-face (erc-downcase masq) "key")))))))
  args)

(provide 'erc-masquerade)

;;; erc-masquerade.el ends here
