;;; erc-fugedit.el -- Fallback edit detection for ERC  -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is an experimental demo module not meant for inclusion in
;; Emacs.  It attempts to implement a heuristics-based fallback for
;; https://github.com/ircv3/ircv3-specifications/pull/425 but
;; currently fails in many respects.
;;
;; TODO add LLM backend.

;;; Code:

(require 'erc)
(require 'diff-mode)

(defgroup erc-fugedit nil
  "Failure-prone edit detection for ERC.
The \"fug\" in \"fugedit\" does not mean anything in particular,
but you may think of it as \"fugazi\" or \"fugly\"."
  :group 'erc
  :package-version '(ERC . "5.7"))

(defcustom erc-fugedit-edit-sensitivity-threshold '(15 . 0.4)
  "Max \"changedness\" to consider a message edited.
When nil, don't bother detecting edits.  Otherwise, this should
be a cons of (MIN-AVG-MSG-LENGTH . MIN-CHANGED-RATIO)."
  :type '(cons number number))

(defcustom erc-fugedit-message-filter-function #'cons
  "A filter for massaging messages to undergo fallback styling.
The function is called with two strings: OLD and NEW.  Returning
nil will abort processing, and the message will be printed
normally, with the relay bot shown as the sender.

Otherwise, the function should return a cons of (CHECKP . NEW),
where NEW is a string and CHECKP is a boolean.  If CHECKP is
non-nil, the message will undergo display-name styling and edit
detection, as usual.  Otherwise, NEW will automatically be
considered an edit.  In either case, NEW can be rewritten, if
necessary.  For example, if the relay bot appends \" [reply]\" to
replies and \" (edited)\" to edits, you'll want to drop those
and, in the case of the latter, ensure CHECKP is nil.

Note that this function may not be called if a custom parsing
function in `erc-fugedit-relay-bots' elects not to."
  :type '(choice function (function-item cons)))

(defcustom erc-fugedit-edit-style "(Edit)"
  "How to display edits.
When the value is `diff-color' or `diff-plain', use vc-style
diffing.  When it's a string, use that as the message prefix.
When nil, don't show edits."
  :type '(choice (const nil)
                 (const diff-plain)
                 (const diff-color)
                 (const "(Edit)")
                 string))

;; FIXME conditionally require'ing `diff-mode' means the customize
;; interface has no preview
;;
;; When fill is enabled, inserted newlines may wrap a diffed region
;; around to the next line, which is ugly when faces have a background
;; color defined.  Users may want to configure these to only use
;; foreground colors.
(defface erc-fugedit-diff-added-face
  '((default :inherit diff-added :extend nil))
  "Face for highlighting added regions in edited messages.")

(defface erc-fugedit-diff-removed-face
  '((default :inherit diff-removed :extend nil))
  "Face for highlighting removed regions in edited messages.")

(defvar-local erc-fugedit--edited-tag nil)
;; This is an alist of members like (NICK (LISP-TIME . MSG) ...)
(defvar-local erc-fugedit--edits nil)

(defvar erc-fugedit-parent-modes '(erc-masquerade-mode))

(defun erc-fugedit--depmod-p ()
  (seq-some (lambda (s) (and (boundp s) (symbol-value s)))
            erc-fugedit-parent-modes))

(define-erc-module fugedit nil
  "Detect edits and stylize messages as diffs."
  ((when erc--target
     (when (erc-fugedit--depmod-p)
       (erc-fugedit--setup +1))
     (dolist (mode-var erc-fugedit-parent-modes)
       (when-let* ((hook (intern-soft (concat (symbol-name mode-var)
                                              "-hook"))))
         (add-hook hook #'erc-fugedit--setup nil t)))))
  ((dolist (mode-var erc-fugedit-parent-modes)
     (when-let* ((hook (intern-soft (concat (symbol-name mode-var) "-hook"))))
       (remove-hook hook #'erc-fugedit--setup t)))
   (unless (erc-fugedit--depmod-p)
     (erc-fugedit--setup -1)))
  localp)

(cl-defmethod erc-fugedit--setup-parent ((_ (eql erc-masquerade-mode)) val)
  (if (natnump val)
      (add-function :override (local 'erc-masquerade--detect-edit-function)
                    #'erc-fugedit-treat-edited)
    (remove-function (local 'erc-masquerade--detect-edit-function)
                     #'erc-fugedit-treat-edited)))

(defun erc-fugedit--setup (&optional arg)
  (if (if arg (natnump arg) (erc-fugedit--depmod-p))
      (progn
        (dolist (mode-var erc-fugedit-parent-modes)
          (erc-fugedit--setup-parent mode-var +1))
        ;; ERC-Edge batch extension integration.
        (when (or (bound-and-true-p erc-v3--batch) (memq 'v3 erc-modules))
          (require 'erc-batch)
          (cl-assert (boundp 'erc-v3--batch-mode-hook))
          (add-hook 'erc-v3--batch-mode-hook #'erc-fugedit--set-batch-locals
                    0 t))
        (setq erc-fugedit--edits nil
              erc-fugedit--edited-tag nil))
    (dolist (mode-var erc-fugedit-parent-modes)
      (erc-fugedit--setup-parent mode-var -1))
    (remove-hook 'erc-v3--batch-mode-hook #'erc-fugedit--set-batch-locals t)
    (kill-local-variable 'erc-fugedit--edits)
    (kill-local-variable 'erc-fugedit--edited-tag)))

(defun erc-fugedit--set-batch-locals ()
  (defvar erc-span--target-locals)
  (if erc-fugedit-mode
      (cl-pushnew 'erc-fugedit--edits erc-span--target-locals)
    (setq erc-span--target-locals
          (remq 'erc-fugedit--edits erc-span--target-locals))))

;; https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm
(defun erc-fugedit--find-edit-distance (ss tt &optional raw)
  (let* ((m (length ss))
         (n (length tt))
         (d (apply #'vector (cl-loop for i to m
                                     for w = (make-vector (1+ n) 0)
                                     do (aset w 0 i)
                                     collect w)))
         (g (lambda (i j) (aref (aref d i) j))))

    (let ((row (aref d 0))
          (j 0))
      (while (<= (cl-incf j) n)
        (aset row j j)))

    (let ((j 0))
      (while (<= (cl-incf j) n)
        (let ((i 0)
              subst-offset
              v)
          (while (<= (cl-incf i) m)
            (setf subst-offset (if (= (aref ss (1- i)) (aref tt (1- j))) 0 1)
                  v (min (1+ (funcall g (1- i) j))
                         (1+ (funcall g i (1- j)))
                         (+ (funcall g (1- i) (1- j)) subst-offset))
                  (aref (aref d i) j) v)))))

    (if raw d (funcall g m n))))

;; Diffing procedure stolen from gitlab.com/opennota/wd GPLv3

(cl-defstruct erc-fugedit--wd-path pos comps)
(cl-defstruct erc-fugedit--wd-comp value status)

(defun erc-fugedit--wd-push (basepath value status)
  (let ((comps (erc-fugedit--wd-path-comps basepath)))
    (if-let* ((comp (car comps))
              ((eq (erc-fugedit--wd-comp-status comp) status)))
        (setf (erc-fugedit--wd-comp-value comp)
              (concat (erc-fugedit--wd-comp-value comp) value))
      (push (make-erc-fugedit--wd-comp :value value :status status) comps))
    (setf (erc-fugedit--wd-path-comps basepath) comps)))

(defun erc-fugedit--wd-extract-common (basepath new old diag)
  (let* ((newpos (erc-fugedit--wd-path-pos basepath))
         (oldpos (- newpos diag)))
    (while (and (< (1+ newpos) (length new))
                (< (1+ oldpos) (length old))
                (string= (aref new (1+ newpos))
                         (aref old (1+ oldpos))))
      (cl-incf newpos)
      (cl-incf oldpos)
      (erc-fugedit--wd-push basepath (aref new newpos) :txt))
    (setf (erc-fugedit--wd-path-pos basepath) newpos)
    oldpos))

(defun erc-fugedit--wd-clone (path)
  ;; Must create new objects because wd-push concatenates strings
  (make-erc-fugedit--wd-path
   :pos (erc-fugedit--wd-path-pos path)
   :comps (mapcar #'copy-erc-fugedit--wd-path
                  (erc-fugedit--wd-path-comps path))))

(defun erc-fugedit--wd (old new)
  (let* ((o-split (with-syntax-table text-mode-syntax-table
                    (cons (split-string new (rx word-boundary) 'omit-nulls)
                          (split-string old (rx word-boundary) 'omit-nulls))))
         (n-split (pop o-split))
         (o (apply #'vector (append o-split nil)))
         (n (apply #'vector (append n-split nil)))
         (maxlen (+ (length o) (length n)))
         (paths (make-hash-table :test #'eql))
         (zero-path (make-erc-fugedit--wd-path :pos -1))
         (oldpos (erc-fugedit--wd-extract-common zero-path n o 0))
         (editlen 0)
         rv)
    (puthash 0 zero-path paths)
    (if (and (>= (1+ (erc-fugedit--wd-path-pos zero-path)) (length n))
             (>= (1+ oldpos) (length o)))
        (erc-fugedit--wd-path-comps zero-path)
      (while (and (not rv) (<= (cl-incf editlen) maxlen))
        (let ((diag (- -2 editlen))
              addpath rempath canadd canrem basepath)
          (while (and (not rv) (<= (cl-incf diag 2) editlen))
            (setq addpath (gethash (1- diag) paths)
                  rempath (gethash (1+ diag) paths)
                  oldpos (if rempath (erc-fugedit--wd-path-pos rempath) 0))
            (cl-decf oldpos diag)
            (when addpath
              (remhash (1- diag) paths))
            (setq canadd (and addpath
                              (< (1+ (erc-fugedit--wd-path-pos addpath))
                                 (length n)))
                  canrem (and rempath (<= 0 oldpos) (< oldpos (length o))))
            (if (not (or canadd canrem))
                (remhash diag paths)
              (if (or (not canadd)
                      (and canrem (< (erc-fugedit--wd-path-pos addpath)
                                     (erc-fugedit--wd-path-pos rempath))))
                  (progn
                    (setq basepath (erc-fugedit--wd-clone rempath))
                    (erc-fugedit--wd-push basepath (aref o oldpos) :rem))
                (setq basepath (erc-fugedit--wd-clone addpath))
                (cl-incf (erc-fugedit--wd-path-pos basepath))
                (let ((v (aref n (erc-fugedit--wd-path-pos basepath))))
                  (erc-fugedit--wd-push basepath v :add)))
              (setq oldpos (erc-fugedit--wd-extract-common basepath
                                                              n o diag))
              (if (and (>= (1+ (erc-fugedit--wd-path-pos basepath))
                           (length n))
                       (>= (1+ oldpos) (length o)))
                  (setq rv (erc-fugedit--wd-path-comps basepath))
                (puthash diag basepath paths))))))
      (nreverse rv))))

;; For now, keep up to 2 messages for at most 5 minutes.
(defvar erc-fugedit-edit--retention-limit '(2 . 300))

(defun erc-fugedit--normalize-edit (old-msg new-msg &optional raw)
  (and-let* ((avg (/ (+ (length new-msg) (length old-msg)) 2.0))
             ((>= avg (car erc-fugedit-edit-sensitivity-threshold)))
             (ed (erc-fugedit--find-edit-distance old-msg new-msg)))
    (if raw
        (/ ed avg)
      (< (/ ed avg) (cdr erc-fugedit-edit-sensitivity-threshold)))))

(defun erc-fugedit--check-edits (nick now new-msg)
  (let* ((keep-quantity (car erc-fugedit-edit--retention-limit))
         (keep-duration (cdr erc-fugedit-edit--retention-limit))
         (this (unless (string-prefix-p "https://" new-msg)
                 (cons (time-add keep-duration now) new-msg)))
         out
         included
         found)
    (pcase-dolist (`(,who . ,items) (nreverse erc-fugedit--edits))
      (let* ((selfp (string= who nick))
             (i (if selfp 0 -1))
             old-msg
             old-ts
             cur)
        (when selfp (setq included t))
        (while (and items (setq old-msg (pop items)
                                old-ts (pop old-msg)))
          (when (and (< (cl-incf i) keep-quantity)
                     (time-less-p now old-ts))
            (push (cons old-ts old-msg) cur))
          (when-let*
              ((selfp)
               (this)
               ((not found))
               (filtered (funcall erc-fugedit-message-filter-function
                                  old-msg new-msg))
               ((progn
                  (setq new-msg (cdr filtered))
                  (or (not (car filtered))
                      (erc-fugedit--normalize-edit old-msg new-msg)))))
            (setq found (cons new-msg old-msg))))
        (setq cur (nreverse cur))
        (when (and selfp this)
          (push this cur))
        (when cur
          (push (cons who cur) out))))
    (unless (or included (not this))
      (push (cons nick (list this)) out))
    (setq erc-fugedit--edits out)
    found))

(defun erc-fugedit--decorate-diff (parts status plainp)
  (let ((value (apply #'concat (nreverse parts))))
    (if plainp
        (if (eq status :add)
            (concat "{+" value "+}")
          (concat "[-" value "-]"))
      (propertize value 'erc--face-temp
                  (if (eq status :add)
                      'erc-fugedit-diff-added-face
                    'erc-fugedit-diff-removed-face)
                  'rear-nonsticky t))))

;; This wants a plist of :text | :remove :add
(defun erc-fugedit--assemble-diff (parts &optional plainp)
  (let (out added removed)
    (while parts
      (pcase (pop parts)
        ((cl-struct erc-fugedit--wd-comp value (status :txt))
         (when removed
           (push (erc-fugedit--decorate-diff removed :rem plainp) out))
         (when added
           (push (erc-fugedit--decorate-diff added :add plainp) out))
         (setq removed nil added nil)
         (push value out))
        ((cl-struct erc-fugedit--wd-comp value status)
         (unless (string-empty-p value)
           (push value (if (eq status :add) added removed))))))
    (when removed
      (push (erc-fugedit--decorate-diff removed :rem plainp) out))
    (when added
      (push (erc-fugedit--decorate-diff added :add plainp) out))
    (apply #'concat (nreverse out))))

(defun erc-fugedit-treat-edited (canonical msg)
  "Stylize an edited MSG using sensible defaults.
Apply style given by `erc-fugedit-edit-style'.  CANONICAL is the
original fallback prefix stripped of any surrounding bracket
decorations color codes, and zero-space chars.  Note that this
permanently alters messages, which affects logs."
  (when-let* ((erc-fugedit-edit-style)
              (ts (cdr (assq 'time (erc-response.tags erc--parsed-response))))
              (ct (erc-compat--iso8601-to-time ts t))
              (found (erc-fugedit--check-edits canonical ct msg)))
    (setq msg (pop found))
    (pcase erc-fugedit-edit-style
      ((and (pred stringp) v)
       (unless (equal erc-fugedit--edited-tag v)
         (setq erc-fugedit--edited-tag
               (propertize v 'erc--face-temp
                           'erc-fugedit-alt-fallback-prefix-face)))
       (concat erc-fugedit--edited-tag " " msg))
      ('diff-color (erc-fugedit--assemble-diff (erc-fugedit--wd found msg)))
      ('diff-plain (erc-fugedit--assemble-diff (erc-fugedit--wd found msg) t))
      (_ msg))))


(provide 'erc-fugedit)

;;; erc-fugedit.el ends here
