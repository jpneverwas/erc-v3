#+TITLE: ERC Edge
This is an alpha-quality "future" version of ERC that exists for
integrations testing and "dog fooding" by ERC developers. It's built
as an installable =:core= [[https://elpa.gnu.org/devel/erc.html][ELPA package]] containing the =lisp/erc= subtree
from [[https://git.savannah.gnu.org/cgit/emacs.git][emacs.git]] along with some unofficial WIP patches (see below). Be
aware that it's often unstable, and many features are unfinished.
Using it as a daily driver is not recommended.

* Installation
1. Add ERC's "bugs" archive to =package-archives=, perhaps temporarily.
   #+BEGIN_SRC elisp
     (setopt package-archives
             (cons '("erc-bugs" . "https://emacs-erc.gitlab.io/bugs/archive/")
                   package-archives))
   #+END_SRC
2. Install the latest version of =erc-edge=. To do this reliably on any
   supported Emacs version, run =M-x list-packages RET=, search for and
   hit =RET= on the package name, then click =[Install]= in the resulting
   =help-mode= buffer. Alternative installation methods, such as via
   =package-vc-install= or =package-install-file= or straight.el, are not
   officially supported but have been reported to work.

* Patches
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0001-5.7-Make-reconnect-detection-more-readable-in-erc-op.patch][[5.7] Make reconnect detection more readable in erc-open]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0002-5.7-Make-ERC-a-special-mode.patch][[5.7] Make ERC a special mode]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0003-5.7-Split-ERC-module-documentation-into-subnodes.patch][[5.7] Split ERC module documentation into subnodes]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0004-5.7-Add-module-example-to-ERC-s-documentation.patch][[5.7] Add module example to ERC's documentation]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0005-5.7-Use-speaker-end-marker-in-ERC-insertion-hooks.patch][[5.7] Use speaker-end marker in ERC insertion hooks]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0006-5.7-Introduce-lower-level-erc-match-API.patch][[5.7] Introduce lower level erc-match API]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0007-5.7-Use-erc-match-type-API-for-erc-desktop-notificat.patch][[5.7] Use erc-match-type API for erc-desktop-notifications]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0008-5.7-Set-major-mode-before-updating-modules-in-erc-op.patch][[5.7] Set major-mode before updating modules in erc-open]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0009-5.7-Make-erc-settings-a-module.patch][[5.7] Make erc-settings a module]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0010-5.7-Persist-erc-ring-input-across-sessions.patch][[5.7] Persist erc-ring-input across sessions]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0011-5.7-Run-mode-hooks-in-ERC-query-buffers-on-reconnect.patch][[5.7] Run mode hooks in ERC query buffers on reconnect]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0012-5.x-Add-divider-variant-for-erc-keep-place-indicator.patch][[5.x] Add divider variant for erc-keep-place-indicator-style]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0013-5.x-Improve-internal-message-splicing-interface-in-E.patch][[5.x] Improve internal message splicing interface in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0014-5.x-Add-predicate-for-logical-ERC-module-activation.patch][[5.x] Add predicate for logical ERC module activation]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0015-5.x-Optionally-suppress-mode-line-updates-in-ERC.patch][[5.x] Optionally suppress mode-line updates in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0016-5.x-Replace-erc-cmem-from-nick-function-args-with-st.patch][[5.x] Replace erc--cmem-from-nick-function args with struct]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0017-5.x-Add-erc-echo-msg-property-for-self-messages.patch][[5.x] Add erc--echo msg property for self messages]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0018-5.x-Make-ERC-response-handling-more-flexible.patch][[5.x] Make ERC response handling more flexible]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0019-5.x-Add-macro-for-creating-lazy-struct-accessors-in-.patch][[5.x] Add macro for creating lazy struct accessors in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0020-5.x-Clearly-define-erc-server-user-info-field-as-an-.patch][[5.x] Clearly define erc-server-user info field as an alist]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0021-5.x-Set-stage-for-breaking-change-to-ERC-tags-format.patch][[5.x] Set stage for breaking change to ERC tags format]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0022-5.x-Add-account-slot-to-erc-server-user.patch][[5.x] Add account slot to erc-server-user]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0023-5.x-Make-updating-last-message-time-more-flexible-in.patch][[5.x] Make updating last-message-time more flexible in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0024-5.x-Use-bespoke-per-response-handler-data-classes-in.patch][[5.x] Use bespoke per-response-handler data classes in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0025-5.x-Make-erc-insert-line-overridable.patch][[5.x] Make erc-insert-line overridable]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0026-WIP-Retain-references-to-all-erc-data-prop-values.patch][[WIP] Retain references to all erc-data prop values]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0027-WIP-Make-erc-partition-prefixed-names-overridable.patch][[WIP] Make erc--partition-prefixed-names overridable]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0028-WIP-Add-IRCv3-building-blocks-to-ERC.patch][[WIP] Add IRCv3 building blocks to ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0029-WIP-Add-module-to-manage-historical-regions-in-ERC.patch][[WIP] Add module to manage historical regions in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0030-WIP-Support-IRCv3-batch-extension-in-ERC.patch][[WIP] Support IRCv3 batch extension in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0031-WIP-Support-IRCv3-multiline-extension-in-ERC.patch][[WIP] Support IRCv3 multiline extension in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0032-WIP-Support-IRCv3-labeled-response-extension-in-ERC.patch][[WIP] Support IRCv3 labeled-response extension in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0033-WIP-Support-IRCv3-chathistory-extension-in-ERC.patch][[WIP] Support IRCv3 chathistory extension in ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0034-POC-Add-a-couple-v3-demo-modules-for-ERC.patch][[POC] Add a couple v3 demo modules for ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0035-POC-Add-experimental-backing-store-for-ERC.patch][[POC] Add experimental backing store for ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0036-POC-Add-general-display-name-support-to-ERC.patch][[POC] Add general display-name support to ERC]]
- [[https://gitlab.com/emacs-erc/edge/-/raw/master/resources/patches/0037-POC-Warn-about-PoC-v3-features-moving-to-non-bug-pac.patch][[POC] Warn about PoC v3 features moving to non-bug package]]
