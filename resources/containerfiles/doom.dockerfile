FROM docker.io/library/debian:bookworm

ARG revision=master

RUN apt-get update && \
    apt-get install -y --no-install-recommends -o=Dpkg::Use-Pty=0 \
        xz-utils curl ca-certificates \
        libc-dev gcc g++ make autoconf automake libncurses-dev gnutls-dev \
        libxml2-dev libdbus-1-dev libacl1-dev acl git texinfo gdb  \
        zlib1g-dev libgccjit-12-dev \
        ripgrep fd-find && \
    cd /root && \
    curl -LO https://ftp.gnu.org/gnu/emacs/emacs-29.1.tar.xz && \
    tar -xf emacs-29.1.tar.xz && cd emacs-29.1 && \
    ./autogen.sh && ./configure && \
    make -j && make install && \
    emacs --version && \
    git clone https://github.com/hlissner/doom-emacs ~/.emacs.d && \
    git -C ~/.emacs.d checkout $revision && \
    YES=1 ~/.emacs.d/bin/doom install --force && \
    mkdir /project

CMD ["emacs"]

# vim:ts=4:sts=4:sw=4:et:ft=dockerfile


