;; -*- lexical-binding: t; -*-
(require 'cl-lib)
(require 'message)

(defun update-readme--collect (patchdir)
  "Return a list of all subject headers from patches in PATCHDIR."
  (let (out)
    (with-temp-buffer
      (message-mode)
      (dolist (file (directory-files patchdir nil (rx ".patch" eot)))
        (insert-file-contents (expand-file-name file patchdir))
        (save-restriction
          (message-narrow-to-headers)
          (search-forward-regexp (rx "Subject: [PATCH "
                                     (+ digit) "/" (+ digit) "] "))
          (let* ((beg (point))
                 (end (progn (message-next-header) (point)))
                 (string (buffer-substring-no-properties beg (1- end))))
            (push (cons file (replace-regexp-in-string "\n" "" string)) out)))
        (erase-buffer))
      (nreverse out))))

(defun update-readme ()
  (pcase-exhaustive command-line-args-left
    (`(,stub ,patchdir ,outfile ,url)
     (cl-assert (file-writable-p outfile))
     (with-temp-file outfile
       (insert-file-contents-literally stub)
       (goto-char (point-max))
       (pcase-dolist (`(,file . ,patch) (update-readme--collect patchdir))
         (insert (format "- [[%s][%s]]" (concat url file) patch) "\n"))))))
