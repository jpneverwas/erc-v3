From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sun, 18 Jul 2021 04:33:17 -0700
Subject: [PATCH 25/37] [5.x] Make erc-insert-line overridable

FIXME report change in insert-marker behavior in ERC-NEWS.

* lisp/erc/erc-goodies.el (erc-scrolltobottom-confirm): Also recenter
when inserting above `erc--insert-marker'.
* lisp/erc/erc.el (erc-insert-line): Make obsolete, move body to
new internal generic function `erc--insert-line'.
(erc--insert-line): Add new generic function to take the place of the
now deprecated `erc-insert-line'.
(erc--route-insertion): Suppress obsolete warning when calling
`erc-insert-line'.
(erc--send-input-lines): Convert to generic function to allow modules
control over fundamental insertion and sending operations, which is
necessary for next-generation features, like multiline messages.
---
 lisp/erc/erc-goodies.el |  2 +-
 lisp/erc/erc.el         | 15 ++++++++++++---
 2 files changed, 13 insertions(+), 4 deletions(-)

diff --git a/lisp/erc/erc-goodies.el b/lisp/erc/erc-goodies.el
index bd2d84dd406..72870fed846 100644
--- a/lisp/erc/erc-goodies.el
+++ b/lisp/erc/erc-goodies.el
@@ -204,7 +204,7 @@ erc--scrolltobottom-confirm
     (let ((resize-mini-windows nil))
       (save-restriction
         (widen)
-        (when (>= (window-point) erc-input-marker)
+        (when (>= (window-point) (or erc--insert-marker erc-input-marker))
           (save-excursion
             (goto-char (point-max))
             (recenter (+ (or scroll-to 0) (or erc-input-line-position -1)))
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index dda7b93fca5..122c49b505e 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -3576,8 +3576,16 @@ erc-insert-line
 STRING, which may make it appear incongruous in situ (unless
 preformatted or anticipated by third-party members of the various
 modification hooks)."
+  (declare (obsolete "use higher-level `erc-display-message'" "30.1"))
   (when string
     (with-current-buffer (or buffer (process-buffer erc-server-process))
+      (erc--insert-line string))))
+
+(cl-defmethod erc--insert-line (string)
+  "Perform work historically done by `erc-insert-line'.
+This is always called in the buffer where STRING is to be inserted."
+  (progn ; Preserve indentation until next major refactoring
+    (progn
       (let (insert-position)
         ;; Initialize ^ below to thwart rogue `erc-insert-pre-hook'
         ;; members that dare to modify the buffer's length.
@@ -3591,7 +3599,7 @@ erc-insert-line
                                        '(invisible intangible) string)))
           (erc-log (concat "erc-display-message: " string
                            (format "(%S)" string) " in buffer "
-                           (format "%s" buffer)))
+                           (format "%s" (current-buffer))))
           (setq erc-insert-this t)
           (erc--run-insert-hook 'erc-insert-pre-hook string)
           (setq insert-position (marker-position (or erc--insert-marker
@@ -3683,7 +3691,8 @@ erc--route-insertion
       (when (buffer-live-p buf)
         (when msg-props
           (setq erc--msg-props (copy-hash-table msg-props)))
-        (erc-insert-line string buf)
+        (with-suppressed-warnings ((obsolete erc-insert-line))
+          (erc-insert-line string buf))
         (setq seen t)))
     (unless (or seen (null buffer))
       (erc--route-insertion string nil))))
@@ -8466,7 +8475,7 @@ erc--run-send-hooks
     (user-error "Multiline command detected" ))
   lines-obj)
 
-(defun erc--send-input-lines (lines-obj)
+(cl-defmethod erc--send-input-lines (lines-obj)
   "Send lines in `erc--input-split-lines' object LINES-OBJ."
   (when (erc--input-split-sendp lines-obj)
     (let ((insertp (erc--input-split-insertp lines-obj))
-- 
2.48.1

