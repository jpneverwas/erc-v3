From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sat, 6 Jan 2024 12:57:27 -0800
Subject: [PATCH 14/37] [5.x] Add predicate for logical ERC module activation

* lisp/erc/erc.el (erc--module-active-p): New function, a utility for
simplifying the loading of module dependencies.
* test/lisp/erc/erc-tests.el (erc--module-active-p):
Add test.
---
 lisp/erc/erc-common.el     | 10 +++++++
 lisp/erc/erc.el            |  9 ++++++
 test/lisp/erc/erc-tests.el | 60 ++++++++++++++++++++++++++++++++++++++
 3 files changed, 79 insertions(+)

diff --git a/lisp/erc/erc-common.el b/lisp/erc/erc-common.el
index 776407b0e8d..46c19e78fca 100644
--- a/lisp/erc/erc-common.el
+++ b/lisp/erc/erc-common.el
@@ -644,6 +644,16 @@ erc-define-message-format-catalog
            (debug (symbolp [&rest [keywordp form]] &rest (symbolp . form))))
   `(erc--define-catalog ,language ,entries))
 
+(defun erc--modify-module-flag (variable module addp)
+  "Add symbol MODULE to list value of local VARIABLE if ADDP, else remove.
+Kill local binding if empty when removing.  Expect MODULE to use
+VARIABLE like a flag (boolean)."
+  (cl-assert (local-variable-if-set-p variable))
+  (if addp
+      (cl-pushnew module (symbol-value variable))
+    (unless (set variable (delq module (ensure-list (symbol-value variable))))
+      (kill-local-variable variable))))
+
 (define-inline erc--strpos (char string)
   "Return position of CHAR in STRING or nil if not found."
   (inline-quote (string-search (string ,char) ,string)))
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index d9a35d4990b..c10057bd2a4 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -2445,6 +2445,15 @@ erc--find-mode
          (fboundp mode)
          mode)))
 
+(defun erc--module-active-p (symbol &optional ensurep)
+  "Return non-nil if module SYMBOL has been loaded or will be.
+With ENSUREP, enable the module's minor mode before returning."
+  (let ((mode-var (intern-soft (concat "erc-" (symbol-name symbol) "-mode"))))
+    (and (or (and mode-var (boundp mode-var) (symbol-value mode-var))
+             (and (memq symbol erc-modules)
+                  (setq mode-var (erc--find-mode symbol))))
+         (or (not ensurep) (always (funcall mode-var +1))))))
+
 (defun erc--update-modules (modules)
   (let (local-modes)
     (dolist (module modules local-modes)
diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index 5ff2b5d8344..818bd9d5a83 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -3660,6 +3660,66 @@ erc--find-mode
                                  (string< (symbol-name a) (symbol-name b)))))))
    erc-tests--modules))
 
+(ert-deftest erc--module-active-p ()
+  (should-not (erc--module-active-p 'fake))
+  (should-not (erc--module-active-p 'fake :ensurep))
+
+  (should (featurep 'erc-networks))
+
+  (let (after-load-alist calls)
+
+    (cl-letf (((symbol-function 'erc-networks-mode)
+               (lambda (&rest r) (push r calls))))
+
+      (let (erc-networks-mode erc-modules)
+        (should-not (erc--module-active-p 'networks))
+        (should-not (erc--module-active-p 'networks :ensurep))
+        (should-not calls))
+
+      (let ((erc-networks-mode t)
+            erc-modules)
+        (should (erc--module-active-p 'networks))
+        (should-not calls)
+        (should (erc--module-active-p 'networks :ensurep))
+        (should (equal calls '((1))))
+        (setq calls nil))
+
+      (let ((erc-networks-mode nil)
+            (erc-modules '(networks)))
+        (should (erc--module-active-p 'networks))
+        (should-not calls)
+
+        (should (erc--module-active-p 'networks :ensurep))
+        (should (equal calls '((1))))
+        (setq calls nil)))))
+
+(defvar-local erc-tests--modify-module-flag nil)
+
+(ert-deftest erc--modify-module-flag ()
+  (should-not erc-tests--modify-module-flag)
+
+  ;; Subscribe module `foo' to flag `erc-tests--modify-module-flag'.
+  (erc--modify-module-flag 'erc-tests--modify-module-flag 'foo t)
+  (should (local-variable-p 'erc-tests--modify-module-flag))
+  (should (equal erc-tests--modify-module-flag '(foo)))
+
+  ;; Redundant subscriptions ignored (idempotent).
+  (erc--modify-module-flag 'erc-tests--modify-module-flag 'foo t)
+  (should (equal erc-tests--modify-module-flag '(foo)))
+
+  ;; Subscribe module `bar' to flag `erc-tests--modify-module-flag'.
+  (erc--modify-module-flag 'erc-tests--modify-module-flag 'bar t)
+  (should (equal erc-tests--modify-module-flag '(bar foo)))
+
+  ;; Removal works.
+  (erc--modify-module-flag 'erc-tests--modify-module-flag 'bar nil)
+  (should (equal erc-tests--modify-module-flag '(foo)))
+
+  ;; last removal unsets.
+  (erc--modify-module-flag 'erc-tests--modify-module-flag 'foo nil)
+  (should-not erc-tests--modify-module-flag)
+  (should-not (local-variable-p 'erc-tests--modify-module-flag)))
+
 (ert-deftest erc--essential-hook-ordering ()
   (erc-tests--assert-printed-in-subprocess
    '(progn
-- 
2.48.1

