From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Fri, 15 Jul 2022 05:02:35 -0700
Subject: [PATCH 21/37] [5.x] Set stage for breaking change to ERC tags format

* lisp/erc/erc-backend.el (erc--finish-parsing-server-response): Add
new generic function and default method.
(erc-parse-server-response): Convert to modular logic and delegate to
helpers.
(erc--parse-traditional-response): Perform actual parsing-related
business formerly handled by `erc-parse-server-response)
(erc--handle-parsed-tags): Add new generic function to handle a tag
regardless of the response command or numeric type.
(erc-handle-parsed-server-response): Call `erc--handle-parsed-tags'.
---
 lisp/erc/erc-backend.el                       | 88 +++++++++++++------
 lisp/erc/erc-compat.el                        | 31 +++++++
 .../erc/resources/erc-scenarios-common.el     |  1 +
 3 files changed, 91 insertions(+), 29 deletions(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index 51b1b3cc909..81474234281 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -1011,7 +1011,10 @@ erc-server-filter-function
                     (substring erc-server-filter-data
                                (match-end 0))))
             (erc-log-irc-protocol line nil)
-            (erc-parse-server-response process line)))))))
+            (save-match-data
+              (unless (string= line "")
+                (erc-compat--with-backtrace-buffer "*erc-process-error*"
+                  (erc-parse-server-response process line))))))))))
 
 (defun erc--server-reconnect-p (event)
   "Return non-nil when ERC should attempt to reconnect.
@@ -1503,22 +1506,38 @@ erc--parse-message-tags
         " releases.  See `erc-tags-format' for more info.")))
     (erc--parse-tags string)))
 
-(defun erc-parse-server-response (proc string)
+(cl-defgeneric erc--finish-parsing-server-response (process string message)
+  "Parse response from the server when appropriate.
+PROCESS is the server process.  STRING is the processed string.  MESSAGE
+is the `erc-response' object, initialized with the unprocessed string,
+as received from the server.")
+
+(cl-defmethod erc--finish-parsing-server-response (process string message)
+  "Parse response from server, decode message, and run handlers."
+  (erc--parse-traditional-response string message)
+  (erc-decode-parsed-server-response message)
+  (erc-handle-parsed-server-response process message)
+  message)
+
+(defun erc-parse-server-response (process string)
   "Parse and act upon a complete line from an IRC server.
-PROC is the process (connection) from which STRING was received.
-PROCs `process-buffer' is `current-buffer' when this function is called."
-  (unless (string= string "") ;; Ignore empty strings
-    (save-match-data
-      (let* ((tag-list (when (eq (aref string 0) ?@)
-                         (substring string 1
-                                    (string-search " " string))))
-             (msg (make-erc-response :unparsed string :tags
-                                     (when tag-list
-                                       (erc--parse-message-tags tag-list))))
-             (string (if tag-list
-                         (substring string (+ 1 (string-search " " string)))
-                       string))
-             (posn (if (eq (aref string 0) ?:)
+Return a new `erc-response' object.  The network PROCESS from which
+STRING is received is always `erc-server-process'."
+  (let ((tags-end (when (eq (aref string 0) ?@)
+                    (string-search " " string)))
+        (msg (make-erc-response :unparsed string)))
+    (when tags-end
+      (setf (erc-response.tags msg) (save-match-data
+                                      (erc--parse-message-tags
+                                       (substring string 1 tags-end)))
+            string (substring string (1+ tags-end))))
+    (erc--finish-parsing-server-response process string msg)))
+
+(defun erc--parse-traditional-response (string msg)
+  (if (not (string-empty-p (erc-response.command msg)))
+      msg ; this can only run once, otherwise mangles msg
+    (progn ; preserve indentation until next refactoring
+      (let* ((posn (if (eq (aref string 0) ?:)
                        (string-search " " string)
                      0)))
 
@@ -1543,19 +1562,24 @@ erc-parse-server-response
                   (substring string bposn eposn))
                 (erc-response.command-args msg)))
         (when posn
-      (let ((str (substring string (1+ posn))))
-        (push str (erc-response.command-args msg))))
-
-    (setf (erc-response.contents msg)
-          (car (erc-response.command-args msg)))
-
-    (setf (erc-response.command-args msg)
-          (nreverse (erc-response.command-args msg)))
-
-    (erc-decode-parsed-server-response msg)
-
-    (erc-handle-parsed-server-response proc msg)))))
-
+          (let ((str (substring string (1+ posn))))
+            (push str (erc-response.command-args msg))))
+
+        (setf (erc-response.contents msg)
+              (car (erc-response.command-args msg)))
+
+        (setf (erc-response.command-args msg)
+              (nreverse (erc-response.command-args msg)))
+        msg))))
+
+;; FIXME this function performs the work of decoding the `contents'
+;; slot twice because it's normally `eq' to the last member of
+;; `command-args'.  Also, `erc-coding-system-for-target' only needs to
+;; run once because all slots in an `erc-response' object share the
+;; same target and thus the same coding system.  For ERC's next major
+;; version, we should put off decoding until target discovery.  If
+;; necessary, we could use a heuristic like the one below to decode
+;; non-ascii target names for nonstandard IRC-like services.
 (defun erc-decode-parsed-server-response (parsed-response)
   "Decode a pre-parsed PARSED-RESPONSE before it can be handled.
 
@@ -1594,10 +1618,16 @@ erc-decode-parsed-server-response
            (erc-response.contents parsed-response)
            decode-target))))
 
+(cl-defgeneric erc--handle-parsed-tags (_parsed)
+  "Handle any tag for any type of response."
+  nil)
+
 (defun erc-handle-parsed-server-response (process parsed-response)
   "Handle a pre-parsed PARSED-RESPONSE from PROCESS.
 
 Hands off to helper functions via `erc-call-hooks'."
+  (when (erc-response.tags parsed-response)
+    (erc--handle-parsed-tags parsed-response))
   (if (member (erc-response.command parsed-response)
               erc-server-prevent-duplicates)
       (let ((m (erc-response.unparsed parsed-response)))
diff --git a/lisp/erc/erc-compat.el b/lisp/erc/erc-compat.el
index 7d5c87c4a58..6500dc171c5 100644
--- a/lisp/erc/erc-compat.el
+++ b/lisp/erc/erc-compat.el
@@ -441,6 +441,37 @@ erc-compat--defer-format-spec-in-buffer
                spec)))))
 
 
+;;;; Misc 30.1
+
+(defmacro erc-compat--with-backtrace-buffer (bufname &rest body)
+  "When BODY fails, print a backtrace in BUFNAME and re-signal."
+  (declare (indent 1))
+  `(,@(if (fboundp 'handler-bind)
+          `(handler-bind
+               ((error
+                 ;; FIXME find out how to allow signals to propagate
+                 ;; properly.  (Re-signaling `err' the normal way here
+                 ;; sometimes causes a segfault, hence `cl-assert'.)
+                 (lambda (err)
+                   (when debug-on-error
+                     (cl-assert (null (cdr err)) nil
+                                "FIXME kludge to trigger debugger"))
+                   (let ((standard-output
+                          (if noninteractive
+                              standard-output
+                            (get-buffer-create ,bufname))))
+                     (terpri)
+                     (let ((print-escape-newlines t))
+                       (prin1
+                        (list :err err
+                              :buffer (current-buffer)
+                              :time (format-time-string "%FT%T.%3NZ" nil t))))
+                     (terpri)
+                     (backtrace))))))
+        '(progn))
+    ,@body))
+
+
 ;;;; Misc 31.1
 
 (defun erc-compat--window-no-other-p (window)
diff --git a/test/lisp/erc/resources/erc-scenarios-common.el b/test/lisp/erc/resources/erc-scenarios-common.el
index 0c2a9bef74b..d9292aabb1d 100644
--- a/test/lisp/erc/resources/erc-scenarios-common.el
+++ b/test/lisp/erc/resources/erc-scenarios-common.el
@@ -195,6 +195,7 @@ erc-scenarios-common-with-cleanup
              (ert-info ("Running extra teardown")
                (funcall erc-scenarios-common-extra-teardown)))
 
+           (should-not (match-buffers (rx "*erc-" (+ (any "a-z-")) "error*")))
            (erc-buffer-do #'erc-scenarios-common--assert-date-stamps)
            (when (and (boundp 'erc-autojoin-mode)
                       (not (eq erc-autojoin-mode ,orig-autojoin-mode)))
-- 
2.48.1

