;;; erc-labeled-response.el --- IRCv3 labeled-response -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This extension provides general request-tracking.  At the moment,
;; this file's implementation merely adds labels to a few commands
;; whose responses might otherwise be ambiguous.  However, its main
;; concern is placeholders for `echo-timestamp'.

;; TODO
;;
;; - Add tests for `erc--ephemeral', asserting stamps, `fill-wrap'
;;   merging are skipped for echo placeholders.
;;
;; - Add tests ensuring `command-indicator' interop.  Specifically,
;;   assert `erc-noncommands-list' members are exempt.
;;
;; - Add tests ensuring labels sent, echo placeholders inserted for
;;   /ME, /SAY, /SV, etc.
;;
;; - Improve design and readability.
;;
;; - If the server-user entry for our own nick has an empty account
;;   field > 30 sec after logging in (when there aren't any active
;;   batches), send a WHO to the server, requesting our own account
;;   status (when WHOX is supported).
;;
;; - Make it easier for one-off commands to take advantage of this.
;;   For example, a URL like irc://link/#chan&insert=hello requires
;;   special handling to insert "hello" at the prompt after
;;   successfully joining #chan.

;;; Code:

(require 'erc-batch)
(require 'ring)

;; We might receive responses that didn't originate from us:
;;
;; > Bouncers might choose to restrict routing labeled responses to
;; > some or all of their clients.
;;
;; Therefore, these objects track which responses are for us.

(cl-defstruct erc-labeled-response--req
  "A `labeled-response' request."
  (label nil :type string)
  (type nil :type symbol)
  ;; A labeled-response job may print to different buffers.
  (markers nil :type (list-of markers))
  ;; Called by the various response handlers with two args: the owning
  ;; `erc-labeled-response--req' instance and the corresponding
  ;; `erc-response' object.  Depending on the context, the latter may
  ;; be nil, such as when the abort timer for echoed messages fires
  ;; because a request is being canceled.  Either way, the callback is
  ;; responsible for resolving the request and cleaning up its
  ;; resources, which, at a minimum, involves removing it from
  ;; `erc-v3--labeled-response' and setting all markers to nil.
  (callback nil :type function))

(defvar erc-labeled-response--device-id nil)
(defvar erc-labeled-response--counter 0)

(erc-v3--define-capability labeled-response
  :depends '(batch)
  :supports '(echo-message)
  :slots (( requests (make-hash-table :test #'equal) :type hash-table
            :documentation "Hashmap of label to `erc-labeled-response--req'.")
          ( echo-queue (make-ring 10) :type ring
            :documentation "Current input's echo-message labels.")
          ( last-self-msgid nil :type (or null string)
            :documentation "Message ID of last self-message."))
  (if erc-v3--labeled-response
      (progn (add-hook 'erc-insert-pre-hook
                       #'erc-labeled-response--on-insert-pre nil t)
             (when (erc-with-server-buffer erc-v3--echo-message)
               ;; Ensure these run before global members.
               (add-hook 'erc-send-modify-hook
                         #'erc-labeled-response--on-send-modify -10 t)
               (add-hook 'erc-pre-send-functions
                         #'erc-labeled-response--on-pre-send -10 t)
               ;; Override `echo-message' send-* variants.
               (add-function :around (local 'erc--send-message-nested-function)
                             #'erc-labeled-response--on-send-message-nested
                             '((depth . -50)))
               (add-function :override (local 'erc--send-action-function)
                             #'erc-labeled-response--send-action
                             '((depth . -50)))
               (add-function :around (local 'erc-send-input-line-function)
                             #'erc-labeled-response--send-input-line))
             (when (memq 'nicks erc-modules)
               (erc-labeled-response--on-nicks-mode)
               (add-hook 'erc-nicks-mode-hook
                         #'erc-labeled-response--on-nicks-mode nil t)))
    (remove-function (local 'erc--send-message-nested-function)
                     #'erc-labeled-response--on-send-message-nested)
    (remove-function (local 'erc-send-input-line-function)
                     #'erc-labeled-response--send-input-line)
    (remove-function (local 'erc--send-action-function)
                     #'erc-labeled-response--send-action)
    (remove-hook 'erc-nicks-mode-hook
                 'erc-labeled-response--on-nicks-mode t)
    (remove-hook 'erc-send-modify-hook
                 #'erc-labeled-response--on-send-modify t)
    (remove-hook 'erc-pre-send-functions
                 #'erc-labeled-response--on-pre-send t)
    (remove-hook 'erc-insert-pre-hook
                 #'erc-labeled-response--on-insert-pre t)))

(defun erc-labeled-response-generate-label ()
  "Issue a unique label as a string."
  (let ((s (format "%.10s.%d"
                   (or erc-labeled-response--device-id
                       (setq erc-labeled-response--device-id
                             (sxhash
                              (list (erc-networks--id-symbol erc-networks--id)
                                    (emacs-pid) (user-uid) (system-name)))))
                   (cl-incf erc-labeled-response--counter))))
    (if (> (string-bytes s) 64)
        (secure-hash 'md5 s)
      (erc-v3--message-tags-escape-tag-value s))))

(defun erc-labeled-response--common-cleanup (request)
  "Return non-nil after performing essential teardown actions for REQUEST.
Return nil if there was nothing to do."
  (when (gethash (erc-labeled-response--req-label request)
                 (erc-v3--labeled-response-requests erc-v3--labeled-response))
    (remhash (erc-labeled-response--req-label request)
             (erc-v3--labeled-response-requests erc-v3--labeled-response))
    (dolist (marker (erc-labeled-response--req-markers request))
      (set-marker marker nil))
    t))

(define-erc-response-handler (ACK)
  "A catch-all `labeled-response' handler." nil
  (when-let* ((label (alist-get 'label (erc-response.tags parsed)))
              (request (gethash label (erc-v3--labeled-response-requests
                                       erc-v3--labeled-response))))
    (funcall (erc-labeled-response--req-callback request) request parsed)))


;;;; Batch

(cl-defstruct (erc-labeled-response--batch (:include erc-batch--info))
  "Data for a single labeled-response batch response."
  ;; This is nil for "foreign responses" to requests from other
  ;; devices on the same network presence.
  (request nil :type erc-labeled-response--req)
  (parsed nil :type (or null erc-response)))

(cl-defmethod erc-batch--create-info ((_ (eql labeled-response))
                                      parsed ref rest)
  "Return a new `erc-labeled-response--batch' object for REF.
Do nothing when PARSED `erc-response' message is not ours.  Expect REST
to be empty."
  (cl-assert (not (erc-batch--get-info ref)) t)
  (cl-assert (null rest))
  (let* ((label (alist-get 'label (erc-response.tags parsed)))
         (request (gethash label (erc-v3--labeled-response-requests
                                  erc-v3--labeled-response)))
         (marker (and request
                      (car (erc-labeled-response--req-markers request))))
         name)
    (with-current-buffer (or (and marker (marker-buffer marker))
                             (erc-server-buffer))
      (setq name (or (erc-target) (erc-networks--id-string erc-networks--id)))
      (let ((info (make-erc-labeled-response--batch :type 'labeled-response
                                                    :ref ref
                                                    :name name
                                                    :parsed parsed
                                                    :request request)))
        (erc-batch--create-queue info)
        (when-let* ((request)
                    (old-cb (erc-labeled-response--req-callback request))
                    (new-cb (lambda (request parsed)
                              (erc-batch--common-teardown info)
                              (funcall old-cb request parsed)))
                    (symbol (make-symbol "erc-labeled-response--callback")))
          (fset symbol new-cb)
          (setf (erc-labeled-response--req-callback request) symbol))
        info))))

(defvar erc-labeled-response--display-nested nil)

(cl-defmethod erc--insert-line
  ((_ string) &context
   (erc-span--active-inserter erc-labeled-response--batch)
   (erc-labeled-response--display-nested null))
  (cl-assert erc-v3--batch)
  (let* ((info erc-span--active-inserter)
         (request (erc-labeled-response--batch-request info))
         (markers (erc-labeled-response--req-markers request))
         (set-active-p (and markers t)))
    (unless markers
      (push (copy-marker erc-insert-marker) markers))
    (dolist (marker markers)
      (with-current-buffer (marker-buffer marker)
        (set-marker-insertion-type marker t)
        (let ((erc-active-buffer
               (if set-active-p (current-buffer) erc-active-buffer))
              (erc--insert-marker marker))
          (unwind-protect (cl-call-next-method)
            (set-marker-insertion-type marker nil)))))))

(defun erc-labeled-response--send-action (target string force)
  (cl-assert erc-v3--echo-message)
  (let ((erc-insert-modify-hook (cons #'erc-labeled-response--on-send-modify
                                      erc-insert-modify-hook)))
    (erc--send-action-display string))
  (let ((erc-v3--unescaped-client-tags (erc-labeled-response--compose-tags)))
    (erc--send-action-perform-ctcp target string force)))

(cl-defmethod erc-span--inserter-insert-one
  ((info erc-labeled-response--batch))
  "Handle batch described by `erc-labeled-response--batch' INFO."
  (pcase-let* ((`(,tags . ,latest) (erc-batch--remove info))
               (erc-auto-query nil) ; don't create a query window
               (erc-batch--parsed-tags tags))
    (erc-parse-server-response erc-server-process latest)))

(cl-defmethod erc-span--inserter-finalize ((info erc-labeled-response--batch))
  (let ((request (erc-labeled-response--batch-request info)))
    (funcall (erc-labeled-response--req-callback request)
             request (erc-labeled-response--batch-parsed info))))

(cl-defmethod erc-batch--handle-enqueued ((info erc-labeled-response--batch))
  "Dispose of a foreign batch and run inserter or insert directly."
  ;; This doesn't run when a batch "ends early" (as termed in the docs)
  (if (erc-labeled-response--batch-request info)
      (erc-span--run-inserter (current-buffer) info)
    (erc-batch--handle-done info)))

(defun erc-labeled-response--register-request (&rest plist)
  "Create a label for client command (request).
Assume caller will bind the result to `erc-v3--unescaped-client-tags'
around a call to `erc--server-send'.  Pass PLIST to
`erc-labeled-response--req' constructor."
  (and-let* (((bound-and-true-p erc-v3--labeled-response))
             (label (erc-labeled-response-generate-label)))
    (puthash label (apply #'make-erc-labeled-response--req :label label plist)
             (erc-v3--labeled-response-requests erc-v3--labeled-response))
    `((label . ,label))))


;;;; WHOIS

;; According to the docs, we must cover this contingency:
;;
;; > the local server notices that the remote server is unavailable
;; > and sends a labeled ACK instead of a WHOIS response
;;
(defun erc-labeled-response--resolve-whois (request parsed)
  (erc-labeled-response--common-cleanup request)
  (when (and parsed (string= (erc-response.command parsed) "ACK"))
    (erc-display-error-notice
     parsed "Labeled WHOIS request mishandled.")))

;; FIXME explain purpose of using labeled response for WHOIS.
(cl-defmethod erc-cmd-WHOIS
  (_ &context (erc-v3--labeled-response erc-v3--labeled-response) &optional _)
  (let* ((label (erc-labeled-response-generate-label))
         (marker (copy-marker erc-insert-marker))
         (erc-v3--unescaped-client-tags `((label . ,label))))
    ;; Put marker at the end of the previous line.
    (cl-assert (= ?\n (char-before erc-insert-marker)))
    (puthash label (make-erc-labeled-response--req
                    :label label
                    :markers (list marker)
                    :callback #'erc-labeled-response--resolve-whois)
             (erc-v3--labeled-response-requests erc-v3--labeled-response))
    (cl-call-next-method)))

(defun erc-labeled-response--resolve-whox (token request parsed)
  "Handle a PARSED \"WHO\" `erc-response' for label REQUEST.
Remove TOKEN's request object from `erc-v3--whox-req-seen'."
  (when (and parsed (string= (erc-response.command parsed) "ACK"))
    (erc-display-error-notice
     parsed "Labeled WHO request on JOIN mishandled."))
  (when (and (erc-labeled-response--common-cleanup request)
             erc-span--active-inserter
             (eq t (erc-batch--info-state erc-span--active-inserter)))
    (erc-v3--whox-delete token)))

(cl-defmethod erc-v3--whox-on-join
  (&context (erc-v3--labeled-response erc-v3--labeled-response))
  (let* ((label (erc-labeled-response-generate-label))
         (erc-v3--unescaped-client-tags `((label . ,label)
                                          ,@erc-v3--unescaped-client-tags))
         (sym (make-symbol "erc-labeled-response--resolve-whox"))
         (req (make-erc-labeled-response--req :label label :callback sym))
         (token (progn (cl-call-next-method)
                       (erc-v3--whox-token erc-v3--whox))))
    (puthash label req (erc-v3--labeled-response-requests
                        erc-v3--labeled-response))
    (setf (symbol-function sym)
          (apply-partially #'erc-labeled-response--resolve-whox token))))

(defun erc-labeled-response--remove-echo (request)
  "Delete placeholder message for REQUEST."
  (when-let* ((marker (car (erc-labeled-response--req-markers request)))
              (buffer (marker-buffer marker))
              (inhibit-field-text-motion t))
    (with-current-buffer buffer
      (when-let* ((start (or (and (get-text-property marker 'label) marker)
                             (next-single-property-change marker 'label)))
                  ((equal (get-text-property start 'label)
                          (erc-labeled-response--req-label request))))
        ;; `start' may be less than the CAR of `bounds'.
        (with-silent-modifications
          (save-excursion
            (goto-char start)
            (let ((end (erc--get-inserted-msg-end (point))))
              (delete-region (1- (pos-bol)) end))))))
    t))

(defun erc-labeled-response--abort-on-timeout (buffer label)
  "Forget requiest for LABEL in BUFFER."
  (when (buffer-live-p buffer)
    (with-current-buffer buffer
      (when-let* ((ring (erc-v3--labeled-response-echo-queue
                         erc-v3--labeled-response))
                  (index (ring-member ring label))
                  (request (gethash label (erc-v3--labeled-response-requests
                                           erc-v3--labeled-response))))
        (ring-remove ring index)
        (funcall (erc-labeled-response--req-callback request)
                 request nil)))))

(defun erc-labeled-response--on-echo-response (request parsed)
  "Maybe remove placeholder when PARSED `erc-response' arrives for REQUEST."
  (pcase (and parsed (erc-response.command parsed))
    ("ACK" (erc-display-error-notice
            parsed "Labeled echo-message request mishandled."))
    ((or 'nil "PRIVMSG" "BATCH") (erc-labeled-response--remove-echo request)))
  (erc-labeled-response--common-cleanup request))

(defvar erc-labeled-response--echo-abort-timeout 1.0) ; for testing

(defun erc-labeled-response--prep-echo-request ()
  "Create a request for current input, subject to `echo-message'.
Start an abort timer to destroy the request if input should fail
the review process."
  (let* ((label (erc-labeled-response-generate-label))
         (marker (point-min-marker))
         (request (make-erc-labeled-response--req
                   :label label
                   :type 'echo-message
                   :markers (list marker)
                   :callback #'erc-labeled-response--on-echo-response)))
    (erc-with-server-buffer
      (ring-insert+extend (erc-v3--labeled-response-echo-queue
                           erc-v3--labeled-response)
                          label)
      (puthash label request (erc-v3--labeled-response-requests
                              erc-v3--labeled-response))
      (run-at-time erc-labeled-response--echo-abort-timeout
                   nil #'erc-labeled-response--abort-on-timeout
                   (current-buffer) label)
      label)))

(defun erc-labeled-response--on-insert-pre (_)
  "Handle a labeled response for STRING, a normal chat message.
Suppress STRING's insertion when we're the sender, except when
encountering a message that lacks a label but has a message ID
matching the last recorded.  In that case, interpret STRING as a
self-message (note to self) that should be inserted.  For now,
assume intervening messages cannot occur or that they won't have
a message ID."
  (when-let* (((erc--check-msg-prop 'erc--spkr))
              (parsed erc--parsed-response))
    (if-let* ((label (alist-get 'label (erc-response.tags parsed))))
        (let* ((request (gethash label (erc-v3--labeled-response-requests
                                        erc-v3--labeled-response)))
               (callback (erc-labeled-response--req-callback request)))
          (funcall callback request parsed)
          (when (erc-current-nick-p (car (erc-response.command-args parsed)))
            (setq erc-insert-this nil)))
      ;; For now, use crude heuristics to detect self-messages.
      (when-let* (((null erc-span--active-inserter)) ; skip when batching
                  ((erc-current-nick-p
                    (car (erc-response.command-args parsed))))
                  ((erc-current-nick-p
                    (car (erc-parse-user (erc-response.sender parsed)))))
                  (id (alist-get 'msgid (erc-response.tags parsed))))
        (if-let* ((inst erc-v3--labeled-response)
                  ((equal id (erc-v3--labeled-response-last-self-msgid inst))))
            (setq erc-insert-this nil)
          (setf (erc-v3--labeled-response-last-self-msgid inst) id))))))

;; Can't issue labels here because this runs on the entirety of the
;; input, which may span mulitple lines and thus be subject to
;; splitting.
(defun erc-labeled-response--on-pre-send (state)
  "Reverse decision to inhibit insertion by upstream input reviewers."
  (cl-assert erc-v3--echo-message)
  (unless (or (erc-input-insertp state)
              (erc--input-split-cmdp erc--current-line-input-split))
    (setf (erc-input-insertp state) t)))

(defun erc-labeled-response--on-send-modify ()
  "Propertize a placeholder message on insertion."
  (cl-assert erc-v3--echo-message)
  ;; Only label chat messages (vs. echoed slash commands, etc.).
  (when-let* (((erc--check-msg-prop 'erc--spkr))
              (label (erc-labeled-response--prep-echo-request)))
    (puthash 'label label erc--msg-props)
    (puthash 'erc--ephemeral 'labeled-response erc--msg-props)
    (put-text-property (point-min) (1- (point-max))
                       'font-lock-face 'erc-pending)))

(defun erc-labeled-response--on-send-message-nested (orig &rest args)
  "Restore `erc-send-message' insertion for `echo-message'.
Assume ORIG and ARGS conform to the
`erc--send-message-nested-function' interface."
  (cl-assert erc-v3--echo-message)
  (let ((erc-v3--echo-message-allow-noncommands-p t))
    (apply orig args)))

(defvar erc-labeled-response--inhibit-label nil
  "Non-nil to inhibit labeling an outgoing PRIVMSG.")

(defun erc-labeled-response--compose-tags ()
  "Add current label to alist suitable for `erc-v3--unescaped-client-tags'."
  (cl-assert erc-v3--labeled-response)
  (if (or erc-labeled-response--inhibit-label (not erc-v3--echo-message))
      erc-v3--unescaped-client-tags
    (if-let* ((ring (erc-v3--labeled-response-echo-queue
                     erc-v3--labeled-response))
              ((not (ring-empty-p ring)))
              (label (ring-remove ring)))
        `((label . ,label) ,@erc-v3--unescaped-client-tags)
      erc-v3--unescaped-client-tags)))

(defun erc-labeled-response--send-input-line (inner &rest args)
  "Call INNER with ARGS, conditionally injecting a label tag."
  ;; Gate on `erc-v3--labeled-response' to allow dependents (callers)
  ;; to bypass label injection.
  (let ((erc-v3--unescaped-client-tags
         (erc-labeled-response--compose-tags)))
    (apply inner args)))


;;;; Nicks integration

(defvar erc-nicks-mode)
(defvar erc-nicks-skip-faces)

(defun erc-labeled-response--on-nicks-mode ()
  "Tell the `nicks' module to avoid highlighting atop `erc-pending'."
  (when (local-variable-p 'erc-nicks-mode)
    (if erc-nicks-mode
        (setq-local erc-nicks-skip-faces
                    (cons 'erc-pending erc-nicks-skip-faces))
      (setq erc-nicks-skip-faces
            (remq 'erc-pending erc-nicks-skip-faces)))))


(provide 'erc-labeled-response)

;;; erc-labeled-response.el ends here
