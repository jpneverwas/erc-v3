;;; erc-names.el --- Tabular /NAMES for ERC -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is currently a throwaway module meant to demonstrate how
;; incremental user-state updates can enrich the user experience.  It
;; replaces the /NAMES command for refreshing a channel's manifest via
;; I/O with one that merely displays the current, rolling local state
;; in a `tabulated-list' buffer.
;;
;; TODO use vtable instead of tabulated-list if available
;; TODO delete or replace with something nice.

;;; Code:
(require 'erc-v3)
(require 'erc-button)
(require 'tabulated-list)
(require 'text-property-search)

(defcustom erc-names-columns '(15 15 25 20 5 5 20 12)
  "Columns for tabulated list of channel members."
  :group 'erc
  :type '(list integer integer integer integer
               integer integer integer integer))

(defvar erc-names--channel-buffer nil
  "The channel buffer associated with the tabulated list buffer.")

(defvar-local erc-names-buffer nil
  "The names buffer for the current channel, if any.")

(defun erc-names--string-less-p (n a b)
  (string< (aref (cadr a) n) (aref (cadr b) n)))

(defun erc-names--status-less-p (a b)
  (< (let ((s (aref (cadr a) 4)))
       (if (string= s "") 5 (seq-position "qaohv" (aref s 0))))
     (let ((s (aref (cadr b) 4)))
       (if (string= s "") 5 (seq-position "qaohv" (aref s 0))))))

(defun erc-names--perform-action ()
  (interactive)
  (when-let* ((entry (tabulated-list-get-entry))
              (name (aref entry 0)))
    (with-current-buffer erc-names--channel-buffer
      (unless (erc-nick-equal-p name (erc-current-nick))
        (erc-nick-popup name)))))

(defvar-keymap erc-names-major-mode-map
  :doc "Local keymap for `erc-names-major-mode-map' buffers."
  :parent tabulated-list-mode-map
  "RET" #'erc-names--perform-action)

(define-derived-mode erc-names-major-mode
  tabulated-list-mode "ERC Channel Names"
  "Major mode for listing channel members."
  :interactive nil
  (setq tabulated-list-format
        `[("Nick"
           ,(nth 0 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 0))
          ("Account"
           ,(nth 1 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 1))
          ("Userhost"
           ,(nth 2 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 2))
          ("Full Name"
           ,(nth 3 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 3))
          ("Status"
           ,(nth 4 erc-names-columns)
           erc-names--status-less-p)
          ("Away"
           ,(nth 5 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 5))
          ("Away Msg"
           ,(nth 6 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 6))
          ("Buffers"
           ,(nth 7 erc-names-columns)
           ,(apply-partially #'erc-names--string-less-p 7))])
  (setq tabulated-list-padding 2
        tabulated-list-sort-key (cons "Status" nil)
        revert-buffer-function #'erc-names-list-refresh)
  (tabulated-list-init-header))

(defun erc-names--create-entry (cusr)
  "Assemble a row object from `erc-channel-members' value CUSR."
  (let* ((user (car cusr))
         (awayp (erc--server-user-away-p user)))
    (vector
     ;; 0 Nick
     (erc-server-user-nickname user)
     ;; 1 Account
     (or (erc-server-user-account user) "*")
     ;; 2 Userhost
     (if-let* ((login (erc-server-user-login user))
               (host (erc-server-user-host user)))
         (concat login "@" host)
       "")
     ;; 3 Full Name
     (if-let* ((full-name (erc-server-user-full-name user))
               ((not (string-empty-p full-name))))
         full-name
       "")
     ;; 4 Status
     (or (erc-v3--format-prefix (cdr cusr) t) "")
     ;; 5 Away
     (if awayp "G" "H")
     ;; 6 Away Msg
     (or awayp "")
     ;; 7 Buffers
     (if-let* ((bufs (erc-server-user-buffers user)))
         (string-join (sort (delete (erc-target) (mapcar #'buffer-name bufs))
                            #'string<)
                      ",")
       ""))))

;; FIXME remove this or use button cache instead.  Users shouldn't
;; expect this to work when disconnected anyway.
(defun erc-names--refill ()
  (let ((table (make-hash-table :test #'equal)))
    (save-excursion
      (save-restriction
        (widen)
        (goto-char (point-min))
        (while (when-let*
                   ((found (text-property-search-forward 'erc-data))
                    (key (prop-match-value found))
                    (user (nth 1 key))
                    (beg (prop-match-beginning found))
                    (chl (or (nth 2 key) (make-erc-channel-user)))
                    (nick (erc-server-user-nickname user)))
                 (puthash nick (cons user chl) table)))))
    table))

(defun erc-names--refresh-contents (&optional _ignore-auto _no-confirm)
  (setq tabulated-list-entries
        (let ((id (tabulated-list-get-id))
              (entries ()))
          (with-current-buffer erc-names--channel-buffer
            (maphash (lambda (_ u)
                       (push (list id (erc-names--create-entry u)) entries))
                     (if (zerop (hash-table-count erc-channel-users))
                         (erc-names--refill)
                       erc-channel-users)))
          entries)))

(defun erc-names-list-refresh (&rest _)
  (erc-names--refresh-contents)
  (tabulated-list-print t))

(defun erc-names-list (channel)
  (interactive (list (erc-target)))
  (with-current-buffer (erc-get-buffer channel erc-server-process)
    (let ((channel-buffer (current-buffer))
          (buf (get-buffer-create (format "*%s/NAMES*" (buffer-name)))))
      (with-current-buffer (or (and (buffer-live-p erc-names-buffer)
                                    erc-names-buffer)
                               (setq erc-names-buffer buf))
        (erc-names-major-mode)
        (setq erc-names--channel-buffer channel-buffer)
        (erc-names--refresh-contents)
        (tabulated-list-print)
        (pop-to-buffer buf)))))

(define-erc-module names nil
  "Module providing /NAMES replacement for ERC."
  ((advice-add 'erc-cmd-NAMES :around #'erc-names--cmd))
  ((put 'erc-cmd-NAMES 'process-not-needed t)
   (advice-remove 'erc-cmd-NAMES #'erc-names-list)
   (put 'erc-cmd-NAMES 'process-not-needed nil)))

(defun erc-names--cmd (orig &rest args)
  "Call `erc-names-list' if `erc-names-mode' is enabled, else ORIG with ARGS."
  (if (and erc-v3-mode erc-names-mode)
      (always (erc-names-list (or (car args) (erc-target))))
    (funcall orig)))


;;;; Extension: no-implicit-names

;; XXX this currently does nothing.

;; TODO only enable when reconnecting within some maximum allowed time
;; frame, and persist phantom cmems or equivalent from button store.

;;;###autoload(put 'draft/no-implicit-names 'erc--feature 'erc-names)
;;;###autoload(put 'no-implicit-names 'erc--feature 'erc-names)
(erc-v3--define-capability no-implicit-names
  "Update server users from \"WHOX\" instead of \"NAMES\".
Print a one-line summary instead of listing all names."
  :aliases '(draft/no-implicit-names)
  (if erc-v3--no-implicit-names
      (if erc--target
          (progn
            (unless erc-names-mode
              (erc-names-mode +1))
            (add-hook 'erc-v3--whox-on-354-functions
                      #'erc-names--increment-tally 80 t))
        (add-hook 'erc-server-315-functions #'erc-names--bake-tally 80 t))
    (remove-hook 'erc-v3--whox-on-354-functions #'erc-names--increment-tally t)
    (remove-hook 'erc-server-315-functions #'erc-names--bake-tally t)
    (kill-local-variable 'erc-names--tally)))

(defvar erc-message-english-names-summary "Users on %c (%d)"
  "English `erc-format-message' template for key `names-summary'.")

(defvar-local erc-names--tally nil)

(defun erc-names--increment-tally ()
  (cl-assert erc-names--tally)
  (setcar erc-names--tally
          (number-to-string (hash-table-count erc-channel-members))))

(defun erc-names--bake-tally (_ parsed)
  (let ((target (nth 1 (erc-response.command-args parsed))))
    (erc-with-buffer (target)
      (erc-v3--bake-display-count (point-min) erc-insert-marker
                                  erc-names--tally))))

(cl-defmethod erc-v3--whox-on-join
  (&context (erc-v3--no-implicit-names erc-v3--no-implicit-names)
            (erc-v3--whox erc-v3--whox))
  "Insert an informative local notice."
  (cl-call-next-method)
  (setq erc-names--tally
        (list (number-to-string (hash-table-count erc-channel-members))))
  (erc-display-message nil 'notice (current-buffer)
                       'names-summary ?c (erc-target)
                       ?d (propertize "   " 'display erc-names--tally)))

(provide 'erc-names)

;;; erc-names.el ends here

;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
